local wezterm = require('wezterm')
local act = wezterm.action

local config = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

local function isarray(x)
  return type(x) == "table" and x[1] ~= nil
end

local getiter = function(x)
  if isarray(x) then
    return ipairs
  elseif type(x) == "table" then
    return pairs
  end
  error("expected table", 3)
end

local function merge(...)
  local rtn = {}
  for i = 1, select("#", ...) do
    local t = select(i, ...)
    local iter = getiter(t)
    for k, v in iter(t) do
      rtn[k] = v
    end
  end
  return rtn
end

local conf_opts = {
  -- scrolling
  --
  scrollback_lines = 50000,

  enable_scroll_bar = false,

  -- window things
  --
  window_background_opacity = 0.8,

  window_padding = {
    left = 0,
    right = 0,
    bottom = 0,
    top = 0
  },

  -- tab-bar
  --
  enable_tab_bar = true,

  hide_tab_bar_if_only_one_tab = true,

  -- cursor
  --
  --cursor_blink_rate = 0.5,

  -- font & text
  --
  font = wezterm.font("IBM Plex Mono"),

  font_size = 11.0,

  text_background_opacity = 0.8,

  -- keys
  --
  use_dead_keys = false, 

  -- colors
  --
  color_scheme = "nord",

  -- other
  -- 
  show_update_window = false,
  check_for_updates = false,
  warn_about_missing_glyphs = false,

  keys = {
    {
	  key = 'LeftArrow',
      mods = 'CTRL|SHIFT',
      action = act.ActivatePaneDirection 'Left',
    },
    {
      key = 'RightArrow',
      mods = 'CTRL|SHIFT',
      action = act.ActivatePaneDirection 'Right',
    },
    {
      key = 'DownArrow',
      mods = 'CTRL|SHIFT',
      action = act.ActivatePaneDirection 'Down',
    },
    { 
      key = 'UpArrow',
      mods = 'CTRL|SHIFT',
      action = act.ActivatePaneDirection 'Up',
    },
  },
}

local merged = merge(config, conf_opts)

return merged
