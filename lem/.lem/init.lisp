(in-package :lem-user)

(setf *scroll-recenter-p* nil)

;;; LSP configurations

(lem-lsp-mode/lsp-mode::define-language-spec
  (c-spec lem-c-mode:c-mode)
  :language-id "c"
  :root-uri-patterns '("compile_commands.json" "compile_flags.txt")
  :command '("clangd" "--header-insertion-decorators=0" "--clang-tidy")
  :readme-url "https://clangd.llvm.org/"
  :connection-mode :stdio)

(lem-lsp-mode/lsp-mode::define-language-spec
    (elixir-spec lem-elixir-mode:elixir-mode)
  :language-id "elixir"
  :root-uir-patterns '("mix.exs")
  :command '("nextls" "--stdio")
  :readme-url "https://www.elixir-tools.dev/docs/next-ls/quickstart/"
  :connection-mode :stdio)