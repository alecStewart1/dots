#!/usr/bin/env bash

# ====# Bashrc #====

# --- If running interactively, do nothing. ---
[[ $- != *i* ]] && return

# --- Options ---
# From bash-sensible, but I'm not sourcing some file I downloaded from the internet.
# I'm just going to copy it, *obviously*.

# No file overwrite
set -o noclobber

# Update window size
shopt -s checkwinsize

# Trim long paths
PROMPT_DIRTRIM=2

# Expand history with <space>
bind Space:magic-space

# Turn on recursive globbing (enables ** to recurse all directories)
shopt -s globstar 2>/dev/null

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

shopt -s dotglob

# Perform file completion in a case insensitive fashion
bind "set completion-ignore-case on"

# Treat hyphens and underscores as equivalent
bind "set completion-map-case on"

# Display matches for ambiguous patterns at first tab press
bind "set show-all-if-ambiguous on"

# Immediately add a trailing slash when autocompleting symlinks to directories
bind "set mark-symlinked-directories on"

## SANE HISTORY DEFAULTS ##

# Append to the history file, don't overwrite it
shopt -s histappend

# Save multi-line commands as one command
shopt -s cmdhist

# Record each line as it gets issued
PROMPT_COMMAND='history -a'

# Beeg history
HISTSIZE=500000
HISTFILESIZE=100000

# Avoid duplicate entries
HISTCONTROL="erasedups:ignoreboth"

# Don't record some commands
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history:clear"

# Use standard ISO 8601 timestamp
# %F equivalent to %Y-%m-%d
# %T equivalent to %H:%M:%S (24-hours format)
HISTTIMEFORMAT='%F %T '

## BETTER DIRECTORY NAVIGATION ##

# Prepend cd to directory names automatically
shopt -s autocd 2>/dev/null
# Correct spelling errors during tab-completion
shopt -s dirspell 2>/dev/null
# Correct spelling errors in arguments supplied to cd
shopt -s cdspell 2>/dev/null

# This defines where cd looks for targets
# Add the directories you want to have fast access to, separated by colon
# Ex: CDPATH=".:~:~/projects" will look for targets in the current working directory, in home and in the ~/projects folder
CDPATH=".:~:~/.config:~/Projects"

# This allows you to bookmark your favorite places across the file system
# Define a variable containing a path and you will be able to cd into it regardless of the directory you're in
shopt -s cdable_vars

# Set the editing mode to vi and bind some more keys for that editing mode.
set -o vi

# --- Functions ---

is_exec() {
	if type "$1" >/dev/null 2>&1; then
		true
	else
		false
	fi
}

passgen() {
	if [[ $1 -gt 0 && $1 -ge 12 ]]; then
		# Generate a strong-ish password
		# Uses /dev/urandom so it's not /really/ random
		# From http://www.bashoneliners.com/oneliners/286/
		tr -dc 'a-zA-Z0-9~!@#$%^&*_()+}{?></\";.,[]=-' </dev/urandom | fold -w "$1" | head -n 1
	else
		echo "Password size is too small to actually be secure"
	fi
}

e() {
	if [[ $INSIDE_EMACS == "vterm" ]]; then
		printf '\e]51;E%s\e' "find-file $1"
	else
		emacsclient -c -a ' ' "$1" &
	fi
}

delnks() {
	if [[ -e $1 ]] && [[ -d $1 ]]; then
		if is_exec fd; then
			fd -L . "$1" --max-depth 1 --type l --exec rm -rf {}
		else
			find -O3 -L "$1" -maxdepth 1 -type l -delete
		fi
	fi
}

if is_exec fzf; then
	fs() {
		local session
		session=$(tmux list-sessions -F "#{session_name}" |
			fzf --query="$1" --select-1 --exit-0) &&
			tmux switch-client -t "$session"
	}
	fkill() {
		local pid
		if [ "$UID" != "0" ]; then
			pid=$(ps -f -u "$UID" | sed 1d | fzf -m | cut -d' ' -f2)
		else
			pid=$(ps -ef | sed 1d | fzf -m | cut -d' ' -f2)
		fi
		if [ "x$pid" != "x" ]; then
			printf "%s" $pid | xargs kill -${1:-9}
		fi
	}
	kf() {
		local path=$1
		local glob=$2
		local glob_opt

		if [[ -n $glob ]]; then
			glob_opt=--glob $glob
		fi
		if [[ -n $path ]] && [[ -d $path ]]; then
			kak "$(rg --file "$glob_opt" "$path" | fzf)"
		elif [[ ! -d $path ]]; then
			echo "Directory does not exist or a file was given!"
		else
			echo "Error occurred! Did you give a path?"
		fi
	}
fi

# --- Aliases for Bash ---

# --- the ls horde ---
alias ls="ls --color=auto"
alias la="ls -laAFh"
alias ll="ls -l"
alias lsl="ls -lhFA | less"
alias lr="ls -tRFh"
alias lt="ls -ltFh"
alias lu="ls -aFu"
alias lscrt="ls -Fcrt"
alias lscart="ls -lFcart"

# --- Make some basic commands a bit better ---
alias mkdir="mkdir -p"
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"

if [[ -x /usr/bin/xbps-install ]]; then
	alias xbpsi="sudo xbps-install -Sv"
	alias xbpsu="sudo xbps-install -Svu"
	alias xbpsre="sudo xbps-install -Svf"
	alias xbpsrm="sudo xbps-remove -FR"
	alias xbpss="sudo xbps-query -Rs"
fi

if [[ -x /usr/bin/pacman ]]; then
	alias paci="sudo pacman -Svv"
	alias pacu="sudo pacman -Syyvvu"
	alias pacuu="sudo pacman -Syyvvuu"
	alias pacs="pacman -Ss"
	if is_exec rg; then
		alias pacg="pacman -Ssq | sort -d | uniq -u | rg"
	else
		alias pacg="pacman -Ssq | sort -d | uniq -u | grep"
	fi
	alias pacf="pacman -Si"
	alias pacr="sudo pacman -R"
	alias pacrs="sudo pacman -Rs"
	alias pacrn="sudo pacman -Rns"
	alias pacro="sudo pacman -Rns '$(pacman -Qtdq)'"
fi

# --- Emacs ---
if is_exec emacs; then
	alias em="emacs"
	alias emq="emacs -Q"
	alias emk="emacsclient -e \"(kill-emacs)\""
	alias emr="emacsclient -e \"(kill-emacs)\"; emacs --daemon"
	alias emc="emacsclient -nc -a 'emacs'"
	alias emt="emacsclient -nw -a 'emacs'"
fi

# --- Kakoune ---
if is_exec kak; then
	alias k="kak"
	alias kc="kf ~/.config/kak/ '!plugins/'"
fi

# --- Some fun ones ---
alias logzip="find -O3 . -type f -mtime +1 -name \"*.log\" -exec zip -m {}.zip {} \; >/dev/null"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"

# --- PATH ---
export PATH=/usr/bin:/usr/sbin:/usr/local/bin:"$HOME/.local/bin":"$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu":"$HOME/.cargo/bin":"$HOME/.nimble/bin":"$HOME/.luarocks/bin":"$NPM_PACKAGES/bin"

# --- Prompt ---

PS1="\[\033[32m\]( \[\033[35m\]\u\[\033[36m\]@\[\033[35m\]\h \[\033[33m\]\w \[\033[32m\])\[\033[34m\] ~> \[\033[37m\]"

# --- FZF ---
[ -f "$HOME/.fzf.bash" ] && source "$HOME/.fzf.bash"
