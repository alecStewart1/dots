#!/usr/bin/env bash

# ====# .bash_profile #====

export LANG=en_US.UTF-8

export TERM="xterm-256color"

# --- Comply with XDG ---

export XDG_CONFIG_HOME="$HOME/.config"

export XDG_CACHE_HOME="$HOME/.cache"

export XDG_DATA_HOME="$HOME/.local/share"

export XDG_RUNTIME_DIR="$HOME/.runtime"

# --- Other things ---

export INPUTRC="$XDG_CONFIG_HOME/inputrc"
#export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export LESSHISTFILE="-"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"

# Better man pages
LESS_TERMCAP_md=$(
    tput bold
    tput setaf 4
)
LESS_TERMCAP_me=$(tput sgr0)
LESS_TERMCAP_mb=$(tput blink)
LESS_TERMCAP_us=$(tput setaf 2)
LESS_TERMCAP_ue=$(tput sgr0)
LESS_TERMCAP_so=$(tput smso)
LESS_TERMCAP_se=$(tput rmso)
export PAGER="${commands[less]:-$PAGER}"

# --- Programming stuff ---
# Nim
# For when I install Nim with choosenim
if type choosenim >/dev/null 2>&1; then
    export CHOOSENIM_CHOOSE_VERSION=#head
fi

# .NET Core things
if type dotnet >/dev/null 2>&1; then
    export DOTNET_CLI_TELEMETRY_OPTOUT=1
    export CLR_OPENSSL_VERSION_OVERRIDE=47
    export DOTNET_ROOT="$XDG_DATA_HOME/dotnet"
fi

# Java
if type java >/dev/null 2>&1; then
    export JAVA_HOME="/usr/lib/jvm/default"
fi

# Dumb NPM stuffs
if type node >/dev/null 2>&1; then
    export NPM_PACKAGES="${XDG_DATA_HOME}/npm-packages"
    export NODE_PATH="$NPM_PACKAGES/lib/node_modules"
    export NPM_CONFIG_PREFIX="$NPM_PACKAGES/npm-global"
    export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
    export MANPATH="$MANPATH:$NPM_PACKAGES/share/man"
fi

# TeX
if type tex >/dev/null 2>&1; then
    export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive*/texmf-config"
    export TEXMFVAR="$XDG_CONFIG_HOME/texlive*/texmf-var"
    export TEXMFHOME="$XDG_DATA_HOME/texmf"
fi

# --- Chromium's configuration dir ---
if type chromium >/dev/null 2>&1; then
    export CHROMIUM_CONFIG_HOME="$XDG_CONFIG_HOME/chromium"
fi

# --- Ripgrep configuration file ---
if type rg >/dev/null 2>&1; then
    export RIPGREP_CONFIG_PATH "$XDG_CONFIG_HOME/ripgreprc"
fi

# --- FZF ---

if type fzf >/dev/null 2>&1; then
    # Make it a /tiny/ bit faster
    if type rg >/dev/null 2>&1; then
        export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'
        _fzf_compgen_path() {
            rg --files --no-ignore-vcs --hidden . "$1"
        }

        # Use rg to generate the list for directory completion
        _fzf_compgen_dir() {
            rg --files --no-ignore-vcs --hidden . "$1"
        }
    fi
    # Make it look nicer
    export FZF_DEFAULT_OPTS='--height 30% --reverse --ansi'
    [[ -f "$HOME/.fzf.bash" ]] && source "$HOME/.fzf.bash"
fi

# --- Wine env variables ---
if type wine >/dev/null 2>&1; then
    export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
fi

# --- Source our bashrc ---

source "$HOME/.bashrc"

