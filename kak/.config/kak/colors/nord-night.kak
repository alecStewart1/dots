# === Nord Night Themes ===
#

# --- Color Palette ---
#

declare-option str nord0 "rgb:252933"
declare-option str nord1 "rgb:3b4252"
declare-option str nord2 "rgb:434c5e"
declare-option str nord3 "rgb:4c566a"
declare-option str nord3_bright "rgb:616e88"
declare-option str nord4 "rgb:c0c5cf"
declare-option str nord5 "rgb:d8dee9"
declare-option str nord6 "rgb:eceff4"
declare-option str nord7 "rgb:8fbcbb"
declare-option str nord8 "rgb:88c0d0"
declare-option str nord9 "rgb:81a1c1"
declare-option str nord10 "rgb:5e81ac"
declare-option str nord11 "rgb:bf616a"
declare-option str nord12 "rgb:d08770"
declare-option str nord13 "rgb:ebcb8b"
declare-option str nord14 "rgb:a3be8c"
declare-option str nord15 "rgb:b48ead"

# --- For code ---
#
  
set-face global variable "%opt{nord4}"
set-face global constant "%opt{nord13}+b"
set-face global function "%opt{nord8}"
set-face global argument "%opt{nord15}"
set-face global type "%opt{nord7}"
set-face global value "%opt{nord8}"
set-face global string "%opt{nord14}"
set-face global builtin "%opt{nord7}"
set-face global keyword "%opt{nord11}"
set-face global operator "%opt{nord9}"
set-face global module "%opt{nord4}"
set-face global class "%opt{nord7}"
set-face global meta "%opt{nord9}"
set-face global comment "%opt{nord3_bright}+i"
set-face global comma "%opt{nord3}"
set-face global bracket "%opt{nord6}"

# --- For markup ---
#

set-face global title "%opt{nord8}+b"
set-face global header "%opt{nord8}"
set-face global bold "%opt{nord2}+b"
set-face global italic "%opt{nord2}+i"
set-face global mono "%opt{nord5}"
set-face global block "%opt{nord1}"
set-face global link "%opt{nord7}"
set-face global list "%opt{nord9}"
set-face global bullet "%opt{nord10}"

# --- For builtin faces ---
#
set-face global Default "%opt{nord4},%opt{nord0}"
set-face global PrimarySelection "default,%opt{nord3}"
set-face global SecondarySelection "default,%opt{nord2}"
set-face global PrimaryCursour "%opt{nord4},default"
set-face global SecondaryCursor "%opt{nord5},default"
set-face global PrimaryCursorEol "%opt{nord0},%opt{nord4}"
set-face global SecondaryCursorEol "%opt{nord0},%opt{nord5}"
set-face global LineNumbers "%opt{nord4},%opt{nord0}"
set-face global LineNumberCursor "default,%opt{nord1}"
set-face global MenuForeground "default,%opt{nord1}+i"
set-face global MenuBackground "default,%opt{nord1}"
set-face global MenuInfo "default,%opt{nord1}"
set-face global Information "%opt{nord8},default"
set-face global Error "%opt{nord11},%opt{nord0}"
set-face global StatusLine "%opt{nord4},%opt{nord1}"
set-face global StatusLineMode "%opt{nord6},%opt{nord1}"
set-face global StatusLineInfo "%opt{nord7},%opt{nord1}"
set-face global StatusLineValue "%opt{nord8},%opt{nord1}"
set-face global StatusCursor "%opt{nord1},%opt{nord3}"
set-face global Prompt "%opt{nord4},%opt{nord2}"
set-face global MatchingChar "%opt{nord7},%opt{nord0}+b"
set-face global Whitespace "%opt{nord4},%opt{nord0}"
set-face global WrapMarker Whitespace
set-face global BufferPadding "%opt{nord5},%opt{nord0}"
