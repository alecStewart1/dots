# ====# Plugins #====
#


# --- Installation of plugin manager ---
#

evaluate-commands %sh{
  plugins="$kak_config/bundle"
  mkdir -p "$plugins"
  [ ! -e "$plugins/kak-bundle" ] && \
    git clone -q https://codeberg.org/jdugan6240/kak-bundle "$plugins/kak-bundle"
  printf "%s\n" "source '$plugins/kak-bundle/rc/kak-bundle.kak'"
}
bundle-noload kak-bundle https://codeberg.org/jdugan6240/kak-bundle

# --- Enchancements ---
#

bundle kakoune-state-save https://gitlab.com/Screwtapello/kakoune-state-save %{
    hook global KakBegin .* %{
        state-save-reg-load colon
        state-save-reg-load pipe
        state-save-reg-load slash
    }

    hook global KakEnd .* %{
        state-save-reg-save colon
        state-save-reg-save pipe
        state-save-reg-save slash
    }

    hook global FocusOut .* %{ state-save-reg-save dquote }
    hook global FocusIn  .* %{ state-save-reg-load dquote }
} 

bundle smarttab.kak https://github.com/andreyorst/smarttab.kak

# --- Moving ---
#

bundle kakoune-vertical-selection https://github.com/occivink/kakoune-vertical-selection %{
    declare-user-mode vertical

    map global user 'v' ': enter-user-mode vertical<ret>' -docstring "vertical selection"

    map global vertical 'j' ': vertical-selection-down<ret>'        -docstring "down"
    map global vertical 'k' ': vertical-selection-up<ret>'          -docstring "up"
    map global vertical 'V' ': vertical-selection-up-and-down<ret>' -docstring "up and down"
}

bundle kak-harpoon https://github.com/raiguard/kak-harpoon

bundle hop.kak https://git.sr.ht/~hadronized/hop.kak %{
    declare-option str hop_kak_keyset 'qwertyuiop[]asdfghjkl;zxcvbnm,.'

    evaluate-commands %sh{ hop-kak --init }

    map global normal <a-%> ':execute-keys gtGbx<ret>'

    define-command hop-kak %{
        eval -no-hooks -- %sh{ hop-kak --keyset "$kak_opt_hop_kak_keyset" --sels "$kak_selections_desc" }
    }

    define-command hop-kak-words %{
      evaluate-commands -draft %{
        execute-keys 'gtGbxs\w+<ret>'
        evaluate-commands -no-hooks -client "%val{client}" -- %sh{ hop-kak --keyset "$kak_opt_hop_kak_keyset" --sels "$kak_selections_desc" }
      }
    }
    
    declare-user-mode hop

    map global user 'g' ': enter-user-mode hop<ret>' -docstring "hop.kak"

    map global hop 'h' ': hop-kak<ret>'       -docstring "Hop to thing"
    map global hop 'w' ': hop-kak-words<ret>' -docstring "Hop to word"
}
bundle-install-hook hop.kak %{
    RUSTFLAGS="-C target-cpu=native -C opt-level=3 -C debug-info=0 -C lto=thin -C link-arg=-Wl,-z,pack-relative-relocs -C link-arg=-Wl,-z,now -C link-arg=-Wl,-z,relro"
    cargo build --release --locked
    cargo install --locked --force --path .
    cargo clean
}
bundle-cleaner hop.kak %{
    cargo clean
    rm -rf ~/.cargo/bin/hop-kak
}


# --- Editing ---
#

bundle kakoune-text-objects https://github.com/delapouite/kakoune-text-objects
bundle kakoune-sudo-write https://github.com/occivink/kakoune-sudo-write

bundle kakoune-table https://gitlab.com/listentolist/kakoune-table %{
    map global user t ": evaluate-commands -draft table-align<ret>" -docstring "align table"
    map global user t ": table-enable<ret>" -docstring "enable table mode"
    map global user T ": table-disable<ret>" -docstring "disable table mode"
    map global user t ": table-toggle<ret>" -docstring "toggle table mode"
    map global user t ": enter-user-mode table<ret>" -docstring "table"
    map global user T ": enter-user-mode -lock table<ret>" -docstring "table (lock)"
}

bundle kakoune-inc-dec https://gitlab.com/Screwtapello/kakoune-inc-dec %{
    map -docstring "decrement selection" global normal <c-c> \
    ': inc-dec-modify-numbers + %val{count}<ret>'

    map -docstring "increment selection" global normal <c-x> \
    ': inc-dec-modify-numbers - %val{count}<ret>'
} 


# --- Coding ---
#

bundle kak-tree-sitter "cargo install kak-tree-sitter; cargo install ktsctl; ktsctl sync -a" %{
  hook global WinSetOption filetype=(c|cpp|objc|rust|python|nim|elixir|lua|latex|javascript|typescript) %{
     eval %sh{ kak-tree-sitter -dks --init "$kak_session" --with-highlighting --with-text-objects -vvv }
  }
}
bundle-cleaner kak-tree-sitter %{
    cargo uninstall kak-tree-sitter
    cargo uninstall ktsctl
}

bundle kakoune-lsp https://github.com/kakoune-lsp/kakoune-lsp %{
    #set-option global lsp_cmd "kak-lsp -vvv -c $HOME/.config/kak-lsp/kak-lsp.toml -s %val{session} --log $HOME/.config/kak-lsp/kak-lsp.log"
    set-option global lsp_completion_trigger "execute-keys 'h<a-h><a-k>\S[^\s,=;*(){}\[\]]\z<ret>'"
    set-option global lsp_diagnostic_line_error_sign "!"
    set-option global lsp_diagnostic_line_warning_sign "?"
   
    hook global WinSetOption filetype=(c|cpp|objc|rust|python|nim|elixir|lua|latex|javascript|typescript) %{

        echo -debug "Enabling LSP for filtetype %opt{filetype}"
        lsp-enable-window

        lsp-auto-hover-enable
        lsp-auto-hover-insert-mode-disable
        lsp-auto-signature-help-enable

        lsp-inlay-diagnostics-enable window
        hook window -group inlay-diagnostics ModeChange (push|pop):.*:insert %{
            lsp-inlay-diagnostics-disable window
        }
        hook window -group inlay-diagnostics ModeChange (push|pop):insert:.* %{
            lsp-inlay-diagnostics-enable window
        }

        set-option global lsp_auto_highlight_references true
        set-option global lsp_hover_anchor true

        # Semantic highlighting
        hook -once global WinSetOption filetype=(c|cpp|objc|rust|lua|elixir|javascript|typescript) %{

            hook window -group semantic-tokens BufReload .* lsp-semantic-tokens
            hook window -group semantic-tokens NormalIdle .* lsp-semantic-tokens
            hook window -group semantic-tokens InsertIdle .* lsp-semantic-tokens

            hook -once -always window WinSetOption filetype=.* %{
                 remove-hooks window semantic-tokens
            }
        }

        # Other things

        #hook window BufWritePre .* lsp-formatting-sync # this breaks things

        hook window BufWritePost .* lsp-diagnostics
        hook -always global KakEnd .* lsp-exit
        hook global WinSetOption filetype=rust %{
            set-option window lsp_server_configuration rust.clippy_preference="on"
        }
    }
}
bundle-install-hook kakoune-lsp %{
    RUSTFLAGS="-C target-cpu=native -C opt-level=3 -C debug-info=0 -C lto=thin -C link-arg=-Wl,-z,pack-relative-relocs -C link-arg=-Wl,-z,now -C link-arg=-Wl,-z,relro"
    cargo build --release --locked
    cargo install --locked --force --path .
    cargo clean
    mkdir -p ~/.config/kak-lsp
    cp -n kak-lsp.toml ~/.dotfiles/kak-lsp/.config/kak-lsp/
}
