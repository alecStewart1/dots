hook global BufCreate (.*/)?(\.kakrc) %{
    set-option buffer filetype kak
    set-option buffer tabstop 4
    set-option buffer indentwidth 4
    set-option buffer softtabstop 1
}
