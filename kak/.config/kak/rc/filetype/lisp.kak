hook global WinSetOption filetype=(lisp|clojure) %{
	set-option window tabstop 2
	set-option window indentwidth 2
	rainbow-enable-window
}


