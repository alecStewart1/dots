# ====# Basic settings #====
#


# --- Kakoune specific settings ---
#

set global indentwidth 2
set global tabstop 2

# ~~~ UI ~~~
#

set-option -add global ui_options terminal_set_title=no terminal_synchronized=yes terminal_change_colors=true terminal_assistant=cat terminal_enable_mouse=no
set-option -add global ui_options ncurses_set_title=false ncurses_wheel_done_button=0 ncurses_change_colors=true

# TODO make the modeline prettier
set-option global modelinefmt '{{mode_info}} / %val{bufname} [ %opt{filetype} ] / %val{cursor_line}:%val{cursor_char_column} {{context_info}} / %val{client}'

hook global BufOpenFile .* %{ modeline-parse }

colorscheme nord-night

hook global WinCreate .* %{ 
  try %{
    add-highlighter window/numbers number-lines -relative -hlcursor -separator '  '
    add-highlighter window/search dynregex '%reg{/}' 0:search
    add-highlighter window/wrap wrap -word -indent
    add-highlighter global/match_brackets show-matching
    add-highlighter global/ranges show_matching_range
    add-highlighter window/todo  regex \b(TODO|FIXME|HACK|PROJ|NOTE|IDEA)\b 0:default+rb
  }
}

set-option global matching_pairs ( ) [ ] { }

# ~~~ UX (I guess) ~~~
#

set-option global autoreload  yes

hook global BufCreate .* %{ try %{ editorconfig-load } }
hook global BufOpenFile .* %{ try %{ editorconfig-load } }

set-option global scrolloff   2,2

set-option global autoinfo command|onkey

set-option global disabled_hooks '(?!auto)(?!detect)\K(.+)-(trim-indent|insert|indent)'

set-option global writemethod replace

# ~~~ Completion ~~~
#

# This seems a little too eager
# set-option global completers filename line=buffer word=all

# ~~~ Commands ~~~
#

set-option global grepcmd 'rg -0 . --hidden --follow --no-heading --column --color=auto --smart-case'
set-option global makecmd 'make -j4'

