# ====# Keys #====
#


# --- Remappings ---
#

map global normal /       '/(?i)'
map global normal <a-/>   '<a-/>(?i)'
map global normal ?       '?(?i)'
map global normal <a-?>   '<a-?>(?i)'
map global normal -docstring 'save selection to register' z Z
map global normal -docstring 'restore selection(s) from register' Z z


# --- Cutting, Yanking, Pasting ---
#
# This is the most universal and sane option.
# So why I don't use a separate user-mode for this?
# Because neither does Vim or Emacs.
# Get over it, nerd.

# set-option global cb_copy_cmd sh %{
#    test "$(uname)" = "Darwin"
# }

# set-option global cb_paste_cmd sh %{

# }

map global normal d      '<a-|>xsel -i -b<ret>d'
map global normal D      '<a-l><a-|>xsel -i -b<ret>d'
map global normal c      '<a-|>xsel -i -b<ret>c'

map global normal y '<a-|>xsel -i -b<ret>'
map global normal Y 'X<a-|>xsel -i -b<ret><a-;>;' # Yea this is ugly. Sue me.

map global normal p '!xsel -o -b<ret>'
map global normal P '<a-!>xsel -o -b<ret>'


# ~~~ Quit ~~~
#

declare-user-mode quitin

map global user 'q' ': enter-user-mode quitin<ret>' -docstring "quiting, saving, and such"

map global quitin 'q' ': quit!<ret>'           -docstring "try to quit Kakoune"
map global quitin 'w' ': write!<ret>'          -docstring "write"
map global quitin 'W' ': write-quit!<ret>'     -docstring "write and quit"
map global quitin 'a' ': write-all-quit!<ret>' -docstring "write and quit everything"

# ~~~ Buffers ~~~
#

declare-user-mode buffers

map global user 'b' ': enter-user-mode buffers<ret>' -docstring "buffers"

map global buffers 'd' ': delete-buffer!<ret>'           -docstring "delete current buffer"
map global buffers 'n' ': buffer-next<ret>'              -docstring "next buffer"
map global buffers 'j' ': buffer-next<ret>'              -docstring "next buffer"
map global buffers 'p' ': buffer-previous<ret>'          -docstring "prev buffer"
map global buffers 'k' ': buffer-previous<ret>'          -docstring "prev buffer"
map global buffers 'f' ': format-buffer<ret>'            -docstring "format buffer"
map global buffers 'l' ': lint-buffer<ret>'              -docstring "lint buffer"


# ~~~ "Windows" ~~~
#

# declare-user-mode windows

# map global user 'w' ': enter-user-mode windows<ret>' -docstring "windowing"

# --- Normal Keys ---
#


# --- Insert Keys ---
#

# ~~~ Some keys like Emacs has ~~~
# ~~~ someone is gonna be mad at me for these ~~~

map global insert <c-p> <up>
map global insert <c-n> <down>

map global insert <c-a> <home>
map global insert <c-e> <end>

map global insert <c-f> '<a-;>l'
map global insert <c-b> '<a-;>h'
map global insert <a-f> '<a-;>w'
map global insert <a-b> '<a-;>b'

 
# ~~~ jk for escpae ~~~
#

hook global InsertChar k %{
    try %{
        execute-keys -draft 'hH<a-k>jk<ret>d'
        execute-keys <esc>
    }
}

# ~~~ tabs to spaces ~~~
#

hook global InsertChar \t %{ 
    try %{
      execute-keys -draft "h<a-h><a-k>\A\h+\z<ret><a-;>;%opt{indentwidth}@"
    }
}

hook global InsertDelete ' ' %{ 
    try %{
      execute-keys -draft 'h<a-h><a-k>\A\h+\z<ret>i<space><esc><lt>'
    }
}

# ~~~ Use previous line indentation ~~~
#

define-command -params 1 \
-docstring %{lineindent <offset>: indent the current selection using a remote line as reference
The offset argument is an integer that indicates the number of the reference line, but can be prefixed with the following:
  - '-': indent with the previous non empty line, e.g. -2 for second line above the current selection
  - '+': indent with the next non empty line, e.g. +3 for the third line beneath the current selection} \
    lineindent %{ evaluate-commands %sh{
    pattern_align=''
    n=$(expr "$1" : '[+-]*\([0-9]*\)')
    case "$1" in
        -[0-9]*) pattern_align="${n}<a-/>^\\h*[^\\h\\n]+<ret>";;
        +[0-9]*) pattern_align="gl ${n}/^\\h*[^\\h\\n]+<ret>";;
        [0-9]*) pattern_align="${n}g";;
        *) exit;;
    esac

    printf %s\\n "eval -draft %{
        try %{ exec \"<a-s><a-K>^$<ret>giZ)<space>${pattern_align}gi<a-z>a<a-&>\" }
    }"
} }

#hook global InsertChar "\n" %{
#    try %{
#        execute-keys -draft "k<a-x><a-k>[{(]\h*$<ret> j<a-gt>"
#    }
#}
# ~~~ Tab for complete ~~~
#

hook global InsertCompletionShow .* %{
    try %{
        # this command temporarily removes cursors preceded by whitespace;
        # if there are no cursors left, it raises an error, does not
        # continue to execute the mapping commands, and the error is eaten
        # by the `try` command so no warning appears.
        execute-keys -draft 'h<a-K>\h<ret>'
        map window insert <tab> <c-n>
        map window insert <s-tab> <c-p>
        hook -once -always window InsertCompletionHide .* %{
            unmap window insert <tab> <c-n>
            unmap window insert <s-tab> <c-p>
        }
    }
}
