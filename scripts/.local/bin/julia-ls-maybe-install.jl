# From:
# https://codeberg.org/uncomfyhalomacro/erudite-macs/src/branch/main/scripts/julia-ls-update-or-install.jl

import Pkg

Pkg.add(url="https://github.com/julia-vscode/LanguageServer.jl", rev="master")
Pkg.add("PackageCompiler")
Pkg.update()

using PackageCompiler

create_sysimage(:LanguageServer, sysimage_path=dirname(Pkg.Types.Context().env.project_file) * "/languageserver.so")
