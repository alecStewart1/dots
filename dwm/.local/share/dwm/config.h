/* See LICENSE file for copyright and license details. */

/* Constants */

#define TERMINAL "wezterm"
#define TERMINAL_CLS_INST "org.wezfurlong.wezterm"
#define BROWSER "librewolf"
#define STATUSBAR "dwmblocks"

/* alt-tab configuration */
static const unsigned int tabModKey             = 0x40; /* if this key is hold the alt-tab functionality stays acitve. This key must be the same as key that is used to active functin altTabStart `*/
static const unsigned int tabCycleKey           = 0x17; /* if this key is hit the alt-tab program moves one position forward in clients stack. This key must be the same as key that is used to active functin altTabStart */
static const unsigned int tabPosY               = 1;    /* tab position on Y axis, 0 = bottom, 1 = center, 2 = top */
static const unsigned int tabPosX               = 1;    /* tab position on X axis, 0 = left, 1 = center, 2 = right */
static const unsigned int maxWTab               = 600;  /* tab menu width */
static const unsigned int maxHTab               = 200;  /* tab menu height */

/* appearance */
static unsigned int borderpx  = 1;        /* border pixel of windows */
static unsigned int snap      = 32;       /* snap pixel */
static int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static unsigned int gappih    = 20;       /* horiz inner gap between windows */
static unsigned int gappiv    = 10;       /* vert inner gap between windows */
static unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static       int smartgaps    = 0;        /* 1 means no outer gap when there is only one window */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]    = { "IBM Plex Mono:size=11" };
static char normbgcolor[]     = "#222222";
static char normbordercolor[] = "#444444";
static char normfgcolor[]     = "#bbbbbb";
static char selfgcolor[]      = "#eeeeee";
static char selbordercolor[]  = "#005577";
static char selbgcolor[]      = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
        /* xprop(1):
         *      WM_CLASS(STRING) = instance, class
         *      WM_NAME(STRING) = title
         */
        /* class              instance                  title       tags mask   isfloating  isfullscreen  isticky  isterminal  noswallow   monitor */
        { TERMINAL_CLS_INST,  TERMINAL_CLS_INST,        NULL,       1,          0,          0,            0,       1,          0,          -1 },
        { "librewolf",        "Navigator",              NULL,       1 << 1,     0,          1,            0,       0,          0,          -1 },
        { "Emacs",            "emacs",                  NULL,       1 << 2,     0,          0,            0,       0,          0,          -1 },
        { "Steam",            "Steam",                  "Steam",    1 << 3,     1,          1,            0,       0,          0,          -1 },
        { "Lutris",           "lutris",                 NULL,       1 << 3,     0,          1,            0,       0,          0,          -1 },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
        /* symbol     arrange function */
        { "[]=",      tile },    /* first entry is default */
        { "[M]",      monocle },
        { "[@]",      spiral },
        { "[\\]",     dwindle },
        { "H[]",      deck },
        { "TTT",      bstack },
        { "===",      bstackhoriz },
        { "HHH",      grid },
        { "###",      nrowgrid },
        { "---",      horizgrid },
        { ":::",      gaplessgrid },
        { "|M|",      centeredmaster },
        { ">M>",      centeredfloatingmaster },
        { "><>",      NULL },    /* no layout function means floating behavior */
        { NULL,       NULL },
};

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
                { "color0",             STRING,  &normbordercolor },
                { "color8",             STRING,  &selbordercolor },
                { "color0",             STRING,  &normbgcolor },
                { "color4",             STRING,  &normfgcolor },
                { "color0",             STRING,  &selfgcolor },
                { "color4",             STRING,  &selbgcolor },
                { "borderpx",           INTEGER, &borderpx },
                { "snap",               INTEGER, &snap },
                { "showbar",            INTEGER, &showbar },
                { "topbar",             INTEGER, &topbar },
                { "nmaster",            INTEGER, &nmaster },
                { "resizehints",        INTEGER, &resizehints },
                { "mfact",              FLOAT,   &mfact },
                { "gappih",             INTEGER, &gappih },
                { "gappiv",             INTEGER, &gappiv },
                { "gappoh",             INTEGER, &gappoh },
                { "gappov",             INTEGER, &gappov },
                { "swallowfloating",    INTEGER, &swallowfloating },
                { "smartgaps",          INTEGER, &smartgaps },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
        { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
        { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
        { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
        { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
        { MOD, XK_j,     ACTION##stack, {.i = INC(+1) } }, \
        { MOD, XK_k,     ACTION##stack, {.i = INC(-1) } }, \
        { MOD, XK_v,     ACTION##stack, {.i = 0 } }, \
        /* { MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \ */
        /* { MOD, XK_a,     ACTION##stack, {.i = 1 } }, \ */
        /* { MOD, XK_z,     ACTION##stack, {.i = 2 } }, \ */
        /* { MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }


/* commands */
static const char *launchercmd[] = { "rofi-launcher", NULL };
static const char *termcmd[]  = { TERMINAL, NULL };
static const char *editorcmd[] = { "emacsclient", "-nc", "-a", "emacs", NULL };

#include <X11/XF86keysym.h>
#include "shift-tools.c"

static const Key keys[] = {
        /* modifier                     key        function        argument */
        { MODKEY,                       XK_d,      spawn,          {.v = launchercmd } },
        { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
        { MODKEY,                       XK_w,      spawn,          {.v = (const char*[]){ "firejail", BROWSER, NULL }} },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = (const char*[]){ "iwgtk", NULL }} },
        { 0, XF86XK_AudioMute,          spawn,     SHCMD("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle; kill -44 $(pidof dwmblocks)") },
        { 0, XF86XK_AudioRaiseVolume,   spawn,     SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 3%+; kill -44 $(pidof dwmblocks)") },
        { 0, XF86XK_AudioLowerVolume,   spawn,     SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 3%-; kill -44 $(pidof dwmblocks)") },
        { 0, XF86XK_AudioMicMute,       spawn,     SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
        { 0, XF86XK_WWW,                spawn,     {.v = (const char*[]){ "firejail", BROWSER, NULL } } },
        { 0, XF86XK_DOS,                spawn,     {.v = termcmd } },
        { MODKEY,                       XK_b,      togglebar,      {0} },
        STACKKEYS(MODKEY,                          focus)
        STACKKEYS(MODKEY|ShiftMask,                push)
        { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
        { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
        { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
        { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
        { MODKEY,                       XK_Return, zoom,           {0} },
        { MODKEY|Mod1Mask,              XK_0,      togglegaps,     {0} },
        { MODKEY|Mod1Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
        { MODKEY,                       XK_q,      view,           {0} },
        { MODKEY|ShiftMask,             XK_g,      killclient,     {0} },
        { MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[1]} }, /* bstack */
        { MODKEY,                       XK_y,      setlayout,      {.v = &layouts[2]} }, /* spiral */
        { MODKEY|ShiftMask,             XK_y,      setlayout,      {.v = &layouts[3]} }, /* dwindle */
        { MODKEY,                       XK_u,      setlayout,      {.v = &layouts[4]} }, /* deck */
        { MODKEY|ShiftMask,             XK_u,      setlayout,      {.v = &layouts[5]} }, /* monocle */
        { MODKEY,                       XK_c,      setlayout,      {.v = &layouts[6]} }, /* centeredmaster */
        { MODKEY|ShiftMask,             XK_c,      setlayout,      {.v = &layouts[7]} }, /* centeredfloatingmaster */
        { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
        { MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
        { MODKEY,                       XK_s,      togglesticky,   {0} },
        { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
        { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
        { MODKEY,                       XK_o, shiftviewclients,    { .i = +1 } },
        { MODKEY|ShiftMask,             XK_o,   shiftview,         { .i = +1 } },
        { MODKEY|ShiftMask,             XK_i,   shiftview,         { .i = -1 } },
        { MODKEY,                       XK_i, shiftviewclients,    { .i = -1 } },
        { MODKEY|ShiftMask,             XK_h,      shiftboth,      { .i = -1 }  },
        { MODKEY|ControlMask,           XK_h,      shiftswaptags,  { .i = -1 }  },
        { MODKEY|ControlMask,           XK_l,      shiftswaptags,  { .i = +1 }  },
        { MODKEY|ShiftMask,             XK_l,      shiftboth,      { .i = +1 }  },
        { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
        { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
        { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
        { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
        { Mod1Mask,                     XK_Tab,    altTabStart,    {0} },
        { MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },
        TAGKEYS(                        XK_1,                      0)
        TAGKEYS(                        XK_2,                      1)
        TAGKEYS(                        XK_3,                      2)
        TAGKEYS(                        XK_4,                      3)
        TAGKEYS(                        XK_5,                      4)
        TAGKEYS(                        XK_6,                      5)
        TAGKEYS(                        XK_7,                      6)
        TAGKEYS(                        XK_8,                      7)
        TAGKEYS(                        XK_9,                      8)
        { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
        { MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} },
        /* Comment these out for now */
        /* { MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } }, */
        /* { MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } }, */
        /* { MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } }, */
        /* { MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } }, */
        /* { MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } }, */
        /* { MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } }, */
        /* { MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } }, */
        /* { MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } }, */
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
        /* click                event mask      button          function        argument */
        { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
        { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
        { ClkWinTitle,          0,              Button2,        zoom,           {0} },
        { ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
        { ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
        { ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
        { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
        { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
        { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
        { ClkTagBar,            0,              Button1,        view,           {0} },
        { ClkTagBar,            0,              Button3,        toggleview,     {0} },
        { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
        { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

