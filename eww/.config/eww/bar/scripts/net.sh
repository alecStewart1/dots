#!/usr/bin/env bash

export GREP_CMD

if command -v rg &>/dev/null; then
    GREP_CMD="$(which rg)"
else
    GREP_CMD="$(which grep)"
fi

TEST="$(ip link | $GREP_CMD 'state UP' | cut -f2 -d' ')"
ESSID="$(iwgetid -r)"

net_stat() {
    if (ping -4 -c 1 1.1.1.1) &>/dev/null; then
	if [[ -n "$TEST" ]]; then
	    echo "Online"
	    echo "some icon"
	else
	    echo "$ESSID"
	    echo "some other icon"
	fi
    else
	echo "Offline"
	echo "another icon"
    fi
}

if [[ $1 == '--stat' ]]; then
    net_stat | head -n1
elif [[ $1 == '--icon' ]]; then
    net_stat | tail -n1
fi

