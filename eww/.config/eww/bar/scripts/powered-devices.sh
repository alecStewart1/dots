#!/usr/bin/env bash

export GREP_CMD
export power

if command -v rg &>/dev/null; then
    GREP_CMD="$(which rg)"
else
    GREP_CMD="$(which grep)"
fi

device_info() {
    "$(upower --dump | $GREP_CMD "$1" --after-context=8)"
}

device() {
    local name
    local state

    name="$(device_info "$1" | $GREP_CMD model | awk '{for(i=2;i<=NF;i++) printf $i" "; print " "}')"
    state="$(device_info "$1" | $GREP_CMD state | cut -f20 -d' ')"

    if [[ "$state" == discharging ]]; then
        power="$(device_info "$1" | $GREP_CMD percentage | cut -f15 -d' ')"
    fi

    if [[ -z "$power" ]]; then
        echo "$name"
        echo "$power"
    else
        echo "$name"
    fi
}

if [[ "$1" == mouse ]]; then
    "$(device mouse)"
elif [[ "$1" == keyboard ]]; then
    "$(device keyboard)"
fi
