# Remove broken symlinks
function delnks --description "Remove broken symlinks in a given directory."
	if test -e "$argv[1]" && test -d "$argv[1]"
		if type fd >/dev/null 2>&1
			fd -L . "$argv[1]" --max-depth 1 --type l --exec rm -rf {}
		else
			find -O3 -L "$argv[1]" -maxdepth 1 -type l -delete
		end
	end
end
