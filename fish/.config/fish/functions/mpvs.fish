#!/usr/bin/env fish

function mpvs --wraps mpv -d 'Stream a video in MPV'
    mpv '$argv[1]' --ytdl-format="bestvideo+bestaudio/best"
end
