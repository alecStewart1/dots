#!/usr/bin/env fish

function vf -a location -d "Edit a file with Kakoune that you selected with FZF."
    set -l location "$argv[1]"
    set -l preview_cmd

    if is-exec bat
        set preview_cmd "bat"
    else
        set preview_cmd "cat"
    end

    nvim (rg --files --ignore --glob !plugins/ "$location" | fzf --preview="$preview_cmd {}" --preview-window=right:70%:wrap)
end
