#!/bin/fish

function tes3cmd-clean -a esp-file -d "Clean ESP file with ease."
    if test -x "$HOME/bin/tes3cmd"
        set -l ret_dir (cwd)
        mv "$ret_dir/$esp-file" "$STEAMDIR/steamapps/common/Morrowind/"
        cd "$STEAMDIR/steamapps/common/Morrowind/"
        te3cmd clean "$esp-file"
        mv "Clean_$esp-file" "$ret_dir"
    else
        printf "te3cmd not found!"
    end
end
