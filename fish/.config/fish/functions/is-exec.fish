#!/usr/bin/env fish

function is-exec -d "Determine if argument passed is an executable"
    set -l result 1
    if test (count "$argv") -gt 1
        return $result
    else
        set -l dir (type -p "$argv[1]")
        set result (type -q "$dir"; and test -x "$dir")
    end
    return $result
end
