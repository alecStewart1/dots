#!/usr/bin/env fish

function ytdl-mpl --description "Download music playlist from YouTube"
    if count $argv > 1 && test -n "$argv[2]"
        yt-dlp --youtube-skip-dash-manifest -x --audio-format best --audio-quality 0 --reject-title $argv[2] --xattrs $argv[1]
    else
        yt-dlp --youtube-skip-dash-manifest -x --audio-format best --audio-quality 0 --xattrs $argv[1]
    end
end
