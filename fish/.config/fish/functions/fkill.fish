# Kill processes with FZF

function fkill -d "Kill a process with FZF"
    set -l pid
    if test -n "$UID" && test "$UID" != "0"
        set pid (ps -f -u "$UID" | sed 1d | fzf -m | cut -d' ' -f2 )
    else
        set pid (ps -ef | sed 1d | fzf -m | cut -d' ' -f2 )
    end
    if test -n "$pid"
        echo "$pid" | xargs kill -{$argv[1]:-9}
    end
end
