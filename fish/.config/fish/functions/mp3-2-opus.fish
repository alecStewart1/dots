#!/usr/bin/env fish

function mp3-2-opus -w ffmpeg -d "Use ffmpeg to convert MP3s to OPUS"
    # Last argument is the file

    set -l mp3FileOrDir $argv[-1]

    # options

    set -l options (fish_opt -s m -l metadata)
    set options $options (string join '' (fish_opt -s b -l bitrate -r) '_validate_int --min 64 --max 384')
    set options $options (string join '' (fish_opt -s s -l samplingrate -r) '_validate_int --min 8 --max 48')
    set options $opionts (string join '' (fish_opt -s c -l compression -r) '_validate_int --min 0 --max 10')
    set options $options (fish_opt -s a -l audiobook)
    set options $options (fish_opt -s m -l muisc)
    set options $options (fish_opt -s v -l verbose)
    argparse $options -- $argv[1..-2]

    # Possible extra options

    set -l ffmpegExtraOpts

    if set -q _flag_metadata
        set -a ffmpegExtraOpts -map_metatdata 0
    end

    if set -q _flag_bitrate
        set -a ffmpegExtraOpts -b:a (string join '' "$_flag_bitrate" "K")
    else if not set -q _flag_bitrate
        set -a ffmpegExtraOpts -b:a 64K
    end

    if set-q _flag_samplingrate
        set -a ffmpegExtraOpts -ar (string join '' "$_flag_samplingrate" "k")
    else
        set -a ffmpegExtraOpts -ar 8k
    end

    if set -q _flag_compression
        set -a ffmpegExtraOpts -compression_level $_flag_compression
    else
        set -a ffmpegExtraOpts -compression_level 10
    end

    if set -q _flag_audiobook and not set -q _flag_muisc
        set -a ffmpegExtraOpts -application voip
    else if set -q _flag_muisc and not set -q _flag_audiobook
        set -a ffmpegExtraOpts -application audio
    else if set -q _flag_musc and set -q _flag_audiobook
        echo "Cannot set -application to voip *and* audio." and return
    end

    if not set -q _flag_verbose
        set -a ffmpegExtraOpts -nostdin
    end

    # Let ’em rip

    if test -d $mp3FileOrDir
        set -l returnDir (cwd)

        cd "$mp3FileOrDir"

        set -l mp3files (fd -t f ".mp3")

        if test -n $mp3files
            for f in $mp3files
                set -l fName (basename "$f" | string split -r -m1 .)[1]
                echo "$f => $fName.opus"
                ffmpeg -i "$f" -c:a libopus -strict -2 -vbr on $ffmpegExtraOpts -y "fName".opus
            end
            echo "MP3 files in $mp3FileOrDir converted to OPUS files."
        else
            echo "No MP3 files in directory."
        end
        cd "$returnDir"
    else
        set -l opusFilename (basename "$mp3FileOrDir" | string split -r -m1 .)[1]
        echo "$mp3FileOrDir => $opusFilename.opus"
        ffmpeg -i "$mp3FileOrDir" -c:a libopus -strict -2 -vbr on $ffmpegExtraOpts -y "$opusFilename".opus
    end
end
