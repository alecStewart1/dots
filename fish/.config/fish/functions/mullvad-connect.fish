function mullvad-connect -d "Connect to a Mullvad Wireguard server."
    set -l wg_result (sudo wg)
    set -l has_connection (count $wg_result)

    if test "$has_connection" -gt 0
        set -l current_connection (sudo wg | head -n 1 | cut -d' ' -f2)
        sudo wg-quick down "$current_connection"
        sudo wg-quick up (sudo fd -t f . /etc/wireguard | path basename | cut -d'.' -f1 | fzf)
    else
        sudo wg-quick up (sudo fd -t f . /etc/wireguard | path basename | cut -d'.' -f1 | fzf)
    end
end
