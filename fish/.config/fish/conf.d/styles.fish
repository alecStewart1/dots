#!/usr/bin/env fish

# === Some stuff related to how fish shell looks ===

set -e fish_greeting
function fish_greeting
end

set -e fish_title
function fish_title
    true
end

# gets rid of mode prompt
function fish_mode_prompt
end

# === Monokai Classic ===
#
# set_color black 48483E
# set_color brblack 76715E

# set_color white ACADA1
# set_color brwhite CFD0C2

# set_color blue 55BCCE
# set_color brblue 66D9EE

# set_color red DC2566
# set_color brred FA2772

# set_color green 8FC029
# set_color brgreen A7E22E

# set_color yellow D4C96E
# set_color bryellow E7DB75

# set_color magenta 9358FE
# set_color brmagenta AE82FF

# set_color cyan 56b7a5
# set_color brcyan 66EFD5

# set fish_color_normal white
# set fish_color_command green --bold
# set fish_color_quote brgreen
# set fish_color_redirection bryellow
# set fish_color_end blue --bold
# set fish_color_error brred --bold
# set fish_color_param cyan
# set fish_color_comment brblack --italic
# set fish_color_match brgreen
# set fish_color_selection magenta
# set fish_color_search_match green --bold --underline
# set fish_color_operator yellow --bold
# set fish_color_escape brblue --bold
# set fish_color_cwd brmagenta --bold
# set fish_color_autosuggestion brblack
# set fish_color_cancel brblue --italic
#
# =======================

# === Palenight ===
#
# set_color black 292D3E
# set_color brblack 1E212E

# set_color white BFC7D5
# set_color brwhite BCBCBC

# set_color blue 55BCCE
# set_color brblue 66D9EE

# set_color red FF5370
# set_color brred FF2750

# set_color green C3E88D
# set_color brgreen AFFF00

# set_color yellow FFCB6B
# set_color bryellow FFD700

# set_color magenta C792EA
# set_color brmagenta D787D7

# set_color cyan 89DDFF
# set_color brcyan 5FD7FF

# set fish_color_normal white
# set fish_color_command green --bold
# set fish_color_quote brgreen
# set fish_color_redirection bryellow
# set fish_color_end blue --bold
# set fish_color_error brred --bold
# set fish_color_param cyan
# set fish_color_comment brblack --italic
# set fish_color_match brgreen
# set fish_color_selection brblue
# set fish_color_search_match green --bold --underline
# set fish_color_operator yellow --bold
# set fish_color_escape brcyan  --bold
# set fish_color_cwd brmagenta --bold
# set fish_color_autosuggestion brblack
# set fish_color_cancel brblue --italic
#
#  =================

# === Moonlight ===
#
#set_color black 212337
#set_color brblack 2F334D
#
#set_color white C8D3F5
#set_color brwhite B4C2F0
#
#set_color blue 82AAFF
#set_color brblue 4976EB
#
#set_color red FF5370
#set_color brred FF757F
#
#set_color green C3E88D
#set_color brgreen AFFF00
#
#set_color yellow FFCB6B
#set_color bryellow FFD700
#
#set_color magenta BAACFF
#set_color brmagenta C099FF
#
#set_color cyan B4F9F8
#set_color brcyan 86E1FC
#
#set fish_color_normal white
#set fish_color_command green --bold
#set fish_color_quote brgreen
#set fish_color_redirection bryellow
#set fish_color_end blue --bold
#set fish_color_error brred --bold
#set fish_color_param cyan
#set fish_color_comment brblack --italic
#set fish_color_match brgreen
#set fish_color_selection brblue
#set fish_color_search_match green --bold --underline
#set fish_color_operator yellow --bold
#set fish_color_escape brcyan  --bold
#set fish_color_cwd brmagenta --bold
#set fish_color_autosuggestion brblack
#set fish_color_cancel brblue --italic
#
# =================

# === TRON Legacy ===
#
#set_color black 000000
#set_color brblack 17181b
#
#set_color white bbccdd
#set_color brwhite a5bad0
#
#set_color blue 5ec4ff
#set_color brblue a0dcfc
#
#set_color red b62d66
#set_color brred ff7dbb
#
#set_color green 4bb5be
#set_color brgreen abdcdf
#
#set_color yellow deb45b
#set_color bryellow f9cc6e
#
#set_color magenta b62d66
#set_color brmagenta e8337e
#
#set_color cyan 4bb5be
#set_color brcyan 80dfe7
#
#set fish_color_normal white
#set fish_color_command green --bold
#set fish_color_quote brgreen
#set fish_color_redirection bryellow
#set fish_color_end blue --bold
#set fish_color_error red --bold
#set fish_color_param cyan
#set fish_color_comment brwhite --italic
#set fish_color_match brgreen
#set fish_color_selection brblue
#set fish_color_search_match green --bold --underline
#set fish_color_operator bryellow --bold
#set fish_color_escape brcyan --bold
#set fish_color_cwd brmagenta --bold
#set fish_color_autosuggestion black
#set fish_color_cancel brblue --italic

#
# ===================

# === Gruvbox ===
#
# set_color black 282828
# set_color brblack 928374

# set_color white a89984
# set_color brwhite ebdbb2

# set_color blue 458588
# set_color brblue 83a598

# set_color red cc241d
# set_color brred fb4934

# set_color green 98971a
# set_color brgreen b8bb26

# set_color yellow d79921
# set_color bryellow afbd2f

# set_color magenta b16286
# set_color brmagenta d3869b

# set_color cyan 689d6a
# set_color brcyan 8ec07c

# set fish_color_normal white
# set fish_color_command green --bold
# set fish_color_quote brgreen
# set fish_color_redirection bryellow
# set fish_color_end blue --bold
# set fish_color_error red --bold
# set fish_color_param cyan
# set fish_color_comment brwhite --italic
# set fish_color_match brgreen
# set fish_color_selection brblue
# set fish_color_search_match green --bold --underline
# set fish_color_operator bryellow --bold
# set fish_color_escape brcyan --bold
# set fish_color_cwd brmagenta --bold
# set fish_color_autosuggestion black
# set fish_color_cancel brblue --italic
#
# ===============

# === Red Planet ===
#
# set_color black 222222
# set_color brblack 202020

# set_color white c2b790
# set_color brwhite cbbf98 

# set_color blue 69819e
# set_color brblue 60827e

# set_color red 8c3432
# set_color brred b55242

# set_color green 728271
# set_color brgreen 869985 

# set_color yellow e8bf6a 
# set_color bryellow ebeb91

# set_color magenta 896492
# set_color brmagenta de4974

# set_color cyan 5b8390 
# set_color brcyan 38add8 

# set fish_color_normal white
# set fish_color_command green --bold
# set fish_color_quote brgreen
# set fish_color_redirection bryellow
# set fish_color_end blue --bold
# set fish_color_error red --bold
# set fish_color_param cyan
# set fish_color_comment brwhite --italic
# set fish_color_match brgreen
# set fish_color_selection brblue
# set fish_color_search_match green --bold --underline
# set fish_color_operator bryellow --bold
# set fish_color_escape brcyan --bold
# set fish_color_cwd brmagenta --bold
# set fish_color_autosuggestion black
# set fish_color_cancel brblue --italic
# ==================

# === Deep ===
#
# set_color black 171b2d
# set_color brblack 282f50

# set_color white 444960
# set_color brwhite 8d95bb

# set_color blue 4b6e96
# set_color brblue 6493c8

# set_color red a54242
# set_color brred cc6666

# set_color green 409458
# set_color brgreen 5fbe7a

# set_color yellow bdb034
# set_color bryellow c5ba4b

# set_color magenta 714f8f
# set_color brmagenta 9468bb

# set_color cyan 569283
# set_color brcyan 77c6b3

# set fish_color_normal 0a0f14
# set fish_color_command brblue --bold
# set fish_color_quote white
# set fish_color_redirection bryellow
# set fish_color_end blue --bold
# set fish_color_error red --bold
# set fish_color_param brcyan
# set fish_color_comment brwhite --italic
# set fish_color_match brgreen
# set fish_color_selection brblue
# set fish_color_search_match cyan --bold --underline
# set fish_color_operator yellow --bold
# set fish_color_escape brcyan --bold
# set fish_color_cwd brmagenta --bold
# set fish_color_autosuggestion brblack
# set fish_color_cancel cyan --italic
# ============

# === FlatWhite ===

# set_color black 605a52
# set_color brblack 93836c

# set_color white f1ece4
# set_color brwhite f7f3ee

# set_color blue 75a3ff
# set_color brblue dde4f2

# set_color red f00000
# set_color brred f6cfcb

# set_color green 84bd00
# set_color brgreen e2e9c1

# set_color yellow f08c00
# set_color bryellow f7e0c3

# set_color magenta ce5cff
# set_color brmagenta f1ddf1

# set_color cyan 00bda4
# set_color brcyan d2ebe3

# set fish_color_normal black
# set fish_color_command green --bold
# set fish_color_quote e4ddd2
# set fish_color_redirection cyan
# set fish_color_end blue --bold
# set fish_color_error red --bold
# set fish_color_param brblack
# set fish_color_comment b9a992 --italic
# set fish_color_match brgreen
# set fish_color_selection dcd3c6
# set fish_color_search_match brblue --bold --underline
# set fish_color_operator yellow --bold
# set fish_color_escape brcyan --bold
# set fish_color_cwd brmagenta --bold
# set fish_color_autosuggestion brblack
# set fish_color_cancel cyan --italic

# =================

# === Nord ===

set_color black 2e3440
set_color brblack 4c566a

set_color white d8dee9
set_color white eceff4

set_color blue 5e81ac
set_color brblue 81a1c1

set_color red bf616a
set_color brred d08770

set_color green a3be8c
set_color brgreen 8fbcbb

set_color yellow ebcb8b

set_color magenta b48ead

set_color cyan 88c0d0

set fish_color_normal white
set fish_color_command green --bold
set fish_color_quote e4ddd2
set fish_color_redirection cyan
set fish_color_end blue --bold
set fish_color_error red --bold
set fish_color_param brwhite
set fish_color_comment b9a992 --italic
set fish_color_match brgreen
set fish_color_selection brblack
set fish_color_search_match brblue --bold --underline
set fish_color_operator yellow --bold
set fish_color_escape brcyan --bold
set fish_color_cwd brmagenta --bold
set fish_color_autosuggestion e5e9f0
set fish_color_cancel cyan --italic

# ============

# === Prompt 1 ===

# function fish_prompt
#     set_color -o brcyan
#     printf "( "
#     set_color -o bryellow
#     printf "%s" "$USER"
#     set_color -o brmagenta
#     printf "@"
#     set_color -o brgreen
#     printf "%s " "$hostname"
#     set_color -o brmagenta
#     printf " %s" "$PWD"
#     set_color -o brcyan
#     printf " )"
#     echo (set_color -o brblue) "~> "
# end

# ================

# === Prompt 2 ===

function fish_prompt
    set_color -o brmagenta
    printf " %s " "$USER"
    set_color -o brblack
    printf "%s" "|"
    set_color -o brgreen
    printf " %s " "$hostname"
    set_color -o brblack
    printf "%s" "|"
    set_color -o brcyan
    printf " %s " "$PWD"
    set_color -o brwhite
    printf " %s " "=>"
	set_color normal
end

# ================
