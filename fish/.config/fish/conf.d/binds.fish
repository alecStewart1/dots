#!/usr/bin/env fish

# TODO
# My keys
function fish_user_key_bindings
    fish_default_key_bindings
    if type fzf >/dev/null 2>&1
	source /usr/share/fzf/key-bindings.fish
        fzf_key_bindings
    end
end

set fish_cursor_default block
set fish_cursor_insert line
set fish_cursor_replace_one underscore
set fish_cursor_visual block

bind \c\x7F backward-kill-word
bind \ce end-of-line
bind \ca beginning-of-line
