#!/usr/bin/env fish

# === Aliases ===

alias la="ls -laAFh"
alias ll="ls -l"
alias lsl="ls -lhFA | less"
alias lr="ls -tRFh"
alias lt="ls -ltFh"
alias lu="ls -aFu"
alias lscrt="ls -Fcrt"
alias lscart="ls -lFcart"

alias ..="cd .."
alias mkdir="mkdir -p"
alias rm="rm -i"
alias cp="cp -i"
alias mv="mv -i"

if type -q emerge && type -q eix
    alias eupdate="sudo eix-sync && sudo emerge -auNDv @world && sudo emerge -acv"
end

if type -q emacs
    alias em="emacs"
    alias emq="emacs -Q"
    alias emk="emacsclient -e \"(kill-emacs)\""
    alias emr="emacsclient -e \"(kill-emacs)\"; emacs --daemon"
    alias emc="emacsclient -nc -a 'emacs'"
    alias emt="emacsclient -nw -a 'emacs'"
end

if type -q nvim
    alias v="nvim"
    alias vc="vf ~/.config/nvim"
end

if type -q kak && type -q kcr
	alias k="kcr edit"
	alias K="kcr-fzf-shell"
	alias KK="kcr --working-directory ."
	alias ks="kcr shell --session"
	alias kl="kcr list"
	alias ka="kcr attach"
	alias ks="kcr create"
	alias kc="kf ~/.config/kak"
end

alias logzip="find -O3 . -type f -mtime +1 -name \"*.log\" -exec zip -m {}.zip {} \; >/dev/null"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"

if type -q gopass
	alias pass="gopass"
end
