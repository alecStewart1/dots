#!/usr/bin/env fish

# === Abbreviations ===

function last_history_items; echo $history[1]; end
abbr -a !! --position anywhere --function last_history_item

abbr h "history"
abbr hgrep "history | grep -i"
abbr hs "history search"

abbr ping2 "ping -c 2"
abbr ping3 "ping -c 3"
abbr ping4 "ping -c 4"

if test -x /usr/bin/xbps-install
    abbr xbpsi "sudo xbps-install -Sv"
    abbr xbpsu "sudo xbps-install -Svu"
    abbr xbpsir "sudo xbps-install -Svf"
    abbr xbpsr "sudo xbps-remove -FR"
    abbr xbpq "sudo xbps-query -Rs"
end

if test -x /usr/bin/pacman
    abbr paci "sudo pacman -Svv"
    abbr pacu "sudo pacman -Syyvvu"
    abbr pacuu "sudo pacman -Syyvvuu"
    abbr pacs "pacman -Ss"
    if test rg >/dev/null 2>&1
        abbr pacg "pacman -Ssq | sort -d | uniq -u | rg"
    else
        abbr pacg "pacman -Ssq | sort -d | uniq -u | grep"
    end
    abbr pacf "pacman -Si"
    abbr pacr "sudo pacman -R"
    abbr pacrs "sudo pacman -Rs"
    abbr pacrn "sudo pacman -Rns"
    abbr pacro "sudo pacman -Rns (pacman -Qtdq)"
end

if test -x /usr/bin/emerge
    abbr emgi "sudo emerge --ask --verbose"
    abbr emgd "sudo emerge --ask --depclean --verbose"
    abbr emgc "sudo emerge --ask --update --deep --changed-use --verbose @world"
    abbr emgn "sudo emerge --ask --update --deep --newuse --verbose @world"
    abbr emgp "sudo emerge --pretend --verbose" 
    abbr emgf "emerge --info"
    abbr emgo "sudo emerge --oneshot --verbose"
end
