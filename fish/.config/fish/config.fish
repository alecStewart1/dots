#!/usr/bin/env fish

# === Check if interactive ===

if status --is-interactive
    return
end

# === Starting PATH variable ===

fish_add_path -U -p /usr/bin /usr/local/bin "$HOME/bin" "$HOME/.local/bin"

# === Exports ===

# ---  The most important one from Gentoo ---

source /etc/profile.env

# --- For all exports ---

set -x LANG en_US.UTF-8
set -x EDITOR "emacsclient -nc -a emacs"
set -x VISUAL "emacsclient -nc -a emacs"
set -x BROWSER "firejail --noprofile librefox-bin"
set -x TERM "xterm-256color"

# ~~~ Use. XDG. Plz! ~~~

set -x XDG_CONFIG_HOME "$HOME/.config"
set -x XDG_CACHE_HOME "$HOME/.cache"
set -x XDG_DATA_HOME "$HOME/.local/share"
set -x XDG_RUNTIME_DIR "$HOME/.runtime"

set -x INPUTRC "$XDG_CONFIG_HOME/inputrc"
set -x LESSHISTFILE ""
set -x GNUPGHOME "$XDG_DATA_HOME/gnupg"
set -x PASSWORD_STORE_DIR "$XDG_DATA_HOME/password-store"
set -x CHROMIUM_CONFIG_HOME "$XDG_CONFIG_HOME/chromium"

# ~~~ Steam ~~~

if is-exec steam
    set -x STEAM_EXEC (which steam)
    set -x STEAMDIR "$XDG_DATA_HOME/Steam"
    set -x STEAM_GAMES "$STEAMDIR/steamapps/common"
end

# ~~~ Emacs things ~~~

set -x LSP_USE_PLISTS true

# ~~~ helix ~~~

if is-exec hx
    set -x HELIX_RUNTIME /usr/share/helix/runtime
end

# ~~~ (h)ledger ~~~

if is-exec hledger
    set -x FINDIR "$HOME/Documents/Ledger"
    set -x LEDGER_FILE "$FINDIR/2024/index.hledger"
end

# ~~~ For when using Wayland ~~~

set -x NEOFETCH (which neofetch)
set -x CURRENT_WM ("$NEOFETCH" wm | cut -d' ' -f2)

switch "$CURRENT_WM"
    case sway river hyprland	
        set -x GDK_BACKEND wayland
    	set -x MOZ_ENABLE_WAYLAND 1
    case Xfwm4 bspwm
        set -x GDK_BACKEND x11
	    set -x MOZ_ENABLE_WAYLAND 0
end

# ~~~ .NET Core ~~~

if is-exec dotnet
    set -x DOTNET_CLI_TELEMETRY_OPTOUT 1
end

# ~~~ Java Stuff ~~~

if is-exec java
    set -x JAVA_HOME "/usr/lib/jvm/default"
    fish_add_path -U -a "$JAVA_HOME/bin" 
end

# ~~~ Rust ~~~

if is-exec rustup
    fish_add_path -U -a "$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu"
    fish_add_path -U -a "$HOME/.cargo/bin" 
end

# ~~~ Erlang ~~~

if is-exec rebar3
    fish_add_path -U -a "$HOME/.cache/rebar3/bin" 
end

# ~~~ Nim ~~~

if is-exec nim
    fish_add_path -U -a "$HOME/.nimble/bin" 
end

# ~~~ Lua ~~~

if is-exec luarocks
    fish_add_path -U -a "$HOME/.luarocks/bin" 
end

# ~~~ Go ~~~

if is-exec go
    set -x GOPATH "$HOME/go"
    fish_add_path -U -a "$GOPATH/bin" 
end

# ~~~ Ruby ~~~

if is-exec rbenv
    status --is-interactive; and rbenv init - | source
end

if is-exec ruby and is-exec gem
    set -x GEM_HOME "$XDG_DATA_HOME/gems"
    set -l RUBY_MAIN_VERSION (ruby --version | cut -d' ' -f2 | cut -d'.' -f1)
    if test "$RUBY_MAIN_VERSION" -eq 3
        set -x RUBYOPT '--jit'
    end
	fish_add_path -U -a "$GEM_HOME/bin"
end

# ~~~ Perl 5 ~~~

if is-exec perl and is-exec perlthanks
    set -x PERL5LIB "$XDG_DATA_HOME/perl5/lib/perl5"
end

# ~~~ Dumb NPM stuffs ~~~

if is-exec node
    set -x NPM_PACKAGES "$XDG_DATA_HOME/npm-packages"
    set -x NODE_PATH "$NPM_PACKAGES/lib/node_modules"
    set -x NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME/npm/npmrc"
	set -x PNPM_HOME "$XDG_DATA_HOME/pnpm"        
    fish_add_path -U -a "$NPM_PACKAGES/bin"
end

# ~~~ TeX ~~~

if is-exec tex
    set -x TEXMFCONFIG "$XDG_CONFIG_HOME/texlive*/texmf-config"
    set -x TEXMFVAR "$XDG_CONFIG_HOME/texlive*/texmf-var"
    set -x TEXMFHOME "$XDG_DATA_HOME/texmf"
end

# ~~~ ccache ~~~

if is-exec ccache
    set -x USE_CCACHE 1
    set -x CCACHE_EXEC /usr/bin/ccache
end

if is-exec openmw-launcher
    set -x OSG_VERTEX_BUFFER_HINT VERTEX_BUFFER_OBJECT
end

# ~~~ get pkconfig to stop acting weird ~~~

set -x PKG_CONFIG_PATH /usr/lib/pkgconfig

# ~~~ Ripgrep configuration file ~~~

if is-exec rg
    set -x RIPGREP_CONFIG_PATH "$XDG_CONFIG_HOME/ripgreprc"
end

# ~~~ FZF ~~~

if is-exec fzf
    if is-exec rg
        set -x FZF_DEFAULT_COMMAND 'rg --sort-files --files --no-heading --no-ignore-vcs --color=auto --hidden --smart-case'
    end
    set -x FZF_DEFAULT_OPTS '--multi --height=27.5% --layout=reverse --ansi'
    set -x FZF_CTRL_T_COMMAND $FZF_DEFAULT_COMMAND
end

# === Global Path ===

set -gx PATH $PATH

