# My Julia startup file

try
    using OhMyREPL
catch e
    @warn "Error initializing OhMyREPL"
end

try
    using CodeTracking
catch e
    @warn "Error initializing CodeTracking"
end

try
    using Revise
catch e
    @warn "Error initializing Revise"
end

try
    using Infiltrator
catch e
    @warn "Error initializing Infiltrator"
end
