#!/usr/bin/env sh

export PATH=/bin:/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin:/home/alec/.bin:/home/alec/.local/bin:/home/alec/.cargo/bin:/usr/lib/jvm/default/bin:/home/alec/.nimble/bin::/bin:/home/alec/.local/share/npm-packages/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl
