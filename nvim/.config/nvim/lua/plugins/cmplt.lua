return {
  {
    "hrsh7th/nvim-cmp",
    enabled = false,
  },
  {
    "Saghen/blink.cmp",
    opts = {
      keymap = { preset = "super-tab" },
    },
  },
}
