return {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "stylua",
        "selene",
        "shellcheck",
        "shellharden",
        "shfmt",
        "black",
        "ruff",
        "eslint_d",
        "typstfmt",
        "codespell",
      },
    },
  },
  {
    "williamboman/mason-lspconfig",
    opts = {
      ensure_installed = {
        "lua_ls",
        "fennel_ls",
        "r_language_server",
        "julials",
        "nextls",
        "basedpyright",
        "html",
        "cssls",
        "css_variables",
        "svelte",
        "emmet_language_server",
        "volar",
        "tinymist",
      },
    },
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = { "saghen/blink.cmp" },
    opts = {
      servers = {
        lua_ls = {
          single_file_support = true,
          settings = {
            Lua = {
              workspace = { checkThirdParty = false },
              codeLens = { enable = true },
              completion = {
                callSnippet = "Replace",
              },
              diagnostics = { globals = { "vim" } },
              doc = { privateName = { "^_" } },
              hint = {
                enable = true,
                paramType = true,
                paramName = "Disable",
                semicolon = "Disable",
                arrayIndex = "Disable",
                setType = false,
              },
              type = {
                castNumberToInteger = true,
              },
              format = {
                enabled = false,
                defaultConfig = {
                  indent_style = "space",
                  indent_size = 2,
                  continuation_indent_size = "2",
                },
              },
              telemetry = { enable = false },
            },
          },
        },
        fennel_ls = {},
        julials = {
          single_file_support = true,
          julia_env_path = "~/.julia/environments/nvim-lspconfig/",
          cmd = {
            "julia",
            "--project=@nvim-lspconfig",
            "--startup-file=no",
            "--history-file=no",
            "--color=no",
            "-J ~/.julia/evironment/nvim-lspconfig/languageserver.so",
            "~/.local/bin/julia-ls.jl",
          },
        },
        r_language_server = {},
        nextls = {},
        basedpyright = {},
        html = {},
        cssls = {},
        css_variables = {},
        svelte = {},
        volar = {
          filetypes = { "typescript", "javascript", "javascriptreact", "typescriptreact", "vue", "json" },
        },
        tinymist = {},
      },
    },
  },
}
