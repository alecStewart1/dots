return {
  "nvim-neorg/neorg",
  lazy = false,
  version = "*",
  dependencies = { "nvim-treesitter/nvim-treesitter" },
  opts = {
    load = {
      ["core.defaults"] = {},
      ["core.concealer"] = {},
      ["core.dirman"] = {
        config = {
          workspaces = {
            notes = "~/Documents/Neorg/Notes",
          },
          default_workspace = "notes",
        },
      },
    },
  },
}
