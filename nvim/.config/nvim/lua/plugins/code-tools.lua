return {
  {
    "mfussenegger/nvim-lint",
    opts = {
      linters_by_ft = {
        --lua = { "selene" },
        fennel = { "fennel" },
        fish = { "fish" },
        sh = { "shellcheck" },
        elixir = { "credo" },
        python = { "bandit", "mypy", "ruff" },
      },
      --      linters = {
      --        selene = {
      --          condition = function(ctx)
      --            return vim.fn.executable("selene") == 1
      --              and vim.fs.find({ "selene.toml" }, { path = ctx.filename, upward = true })[1]
      --          end,
      --        },
      --      },
    },
  },
  {
    "stevearc/conform.nvim",
    opts = {
      formatters_by_ft = {
        lua = { "stylua" },
        fennel = { "fnlfmt" },
        fish = { "fish_indent" },
        sh = { "shfmt" },
        python = { "black", "ruff_format", "ruff_organize_imports" },
        elixir = { "mix" },
        javascript = { "eslint_d" },
        typescript = { "eslint_d" },
        typst = { "typstfmt" },
        ["_"] = { "trim_whitespace" },
      },
    },
  },
}
