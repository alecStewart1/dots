-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local vopt = vim.opt
local vfn = vim.fn

-- Faster redraw
--

--vopt["lazyredraw"] = true
vopt["updatetime"] = 200
vopt["timeoutlen"] = 400
vopt["redrawtime"] = 1500
vopt["ttimeoutlen"] = 10

if not vfn.has("gui_running") then
  vopt["t_Co"] = 256
end
