;;; early-init.el --- Early initialization for Emacs -*- lexical-binding: t no-byte-compile: t no-native-compile: t no-update-autoloads: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;  I took a lot of this from Doom Emacs.
;;
;;; Code:

(setq gc-cons-threshold most-positive-fixnum
			gc-cons-percentage 0.5
      load-prefer-newer noninteractive
      native-comp-jit-compilation nil
      native-comp-jit-speed 2
      native-comp-async-report-warnings-errors 'silent
      package-enable-at-startup nil
      package-native-compile t
      mode-line-format nil
      ;; we have to set these before use-package in Emacs 29+
      use-package-compute-statistics init-file-debug
      use-package-verbose init-file-debug
      use-package-minimum-reported-time (if init-file-debug 0 0.1)
      use-package-always-ensure t
      use-package-always-defer t
      use-package-expand-minimally (not noninteractive)
      use-package-enable-imenu-support t)

(unless (or (daemonp) noninteractive)
  (let ((old-value (default-toplevel-value 'file-name-handler-alist)))
    ;; `file-name-handler-alist' is consulted on each `require', `load' and
    ;; various path/io functions. You get a minor speed up by unsetting this.
    ;; Some warning, however: this could cause problems on builds of Emacs where
    ;; its site lisp files aren't byte-compiled and we're forced to load the
    ;; *.el.gz files (e.g. on Alpine).
    (setq file-name-handler-alist
          ;; HACK: If the bundled elisp for this Emacs install isn't
          ;;   byte-compiled (but is compressed), then leave the gzip file
          ;;   handler there so Emacs won't forget how to read read them.
          ;;
          ;;   calc-loaddefs.el is our heuristic for this because it is built-in
          ;;   to all supported versions of Emacs, and calc.el explicitly loads
          ;;   it uncompiled. This ensures that the only other, possible
          ;;   fallback would be calc-loaddefs.el.gz.
          (if (eval-when-compile
                (locate-file-internal "calc-loaddefs.el" load-path))
              nil
            (list (rassq 'jka-compr-handler old-value))))
    ;; Make sure the new value survives any current let-binding.
    (set-default-toplevel-value 'file-name-handler-alist file-name-handler-alist)
    ;; Remember it so it can be reset where needed.
    (put 'file-name-handler-alist 'initial-value old-value)
    ;; ...but restore `file-name-handler-alist' later, because it is needed for
    ;; handling encrypted or compressed files, among other things.
    (defun reset-file-handler-alist ()
      (setq file-name-handler-alist
            ;; Merge instead of overwrite because there may have bene changes to
            ;; `file-name-handler-alist' since startup we want to preserve.
            (delete-dups (append file-name-handler-alist old-value))))
    (add-hook 'emacs-startup-hook #'reset-file-handler-alist 101))

  ;; Site files tend to use `load-file', which emits "Loading X..." messages in
  ;; the echo area, which in turn triggers a redisplay. Redisplays can have a
  ;; substantial effect on startup times and in this case happens so early that
  ;; Emacs may flash white while starting up.
  (define-advice load-file (:override (file) silence)
    (load file nil 'nomessage))

  ;; Undo our `load-file' advice above, to limit the scope of any edge cases it
  ;; may introduce down the road.
  (define-advice startup--load-user-init-file (:before (&rest _) undo-silence)
    (advice-remove #'load-file #'load-file@silence))

  (or (let (file-name-handler-alist)
        (setq user-emacs-directory (file-name-directory load-file-name))
        (setq user-init-file (expand-file-name "early-init" user-emacs-directory))
        (setq load-prefer-newer t)
        (setq gc-cons-threshold (* 16 1024 1024))))

  (setq frame-inhibit-implied-resize t)

  (advice-add #'display-startup-echo-area-message :override #'ignore)
  (advice-add #'display-startup-screen :override #'ignore)

  (unless initial-window-system
    ;; PERF: Inexplicably, `tty-run-terminal-initialization' can sometimes
    ;;   take 2-3s when starting up Emacs in the terminal. Whatever slows it
    ;;   down at startup doesn't appear to affect it if it's called a little
    ;;   later in the startup process, so that's what I do.
    ;; REVIEW: This optimization is not well understood. Investigate it!
    (define-advice tty-run-terminal-initialization (:override (&rest _) defer)
      (advice-remove #'tty-run-terminal-initialization #'tty-run-terminal-initialization@defer)
      (add-hook 'window-setup-hook
                  (apply-partially #'tty-run-terminal-initialization (selected-fame) nil t))))

  (push '(menu-bar-lines . 0) default-frame-alist)
  (push '(tool-bar-lines . 0) default-frame-alist)
  (push '(horizontal-scroll-bars . nil) default-frame-alist)
  (push '(bottom-divider-width . 1) default-frame-alist)
  (push '(vertical-scroll-bars . nil) default-frame-alist)
  (push '(undecorated-rounded . t) default-frame-alist)

  (set-language-environment "UTF-8")

  (setq default-input-method nil)

  (unless (eq system-type 'darwin)
    (setq command-line-ns-option-alist nil))
  (unless (memq initial-window-system '(x pgtk))
    (setq command-line-x-option-alist nil)))

;;; early-init.el ends here
