;;; modal.el --- Experimenting with different modal editing modes -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;  I’ve heard good things about Meow and Devil, so I thought I’d give them it a shot.
;;
;;; Code:

;;; Packages
;;;

;;;; Meow
;;;;
;; TODO this still needs a bit of work, or rather getting used to.

(use-package meow
  :demand t
  :preface
  (defvar meow--kbd-redo "C-M-_"
    "KBD macro for command ‘undo-redo’.")

  (defun meow-redo ()
    "Cancel the current selection then redo."
    (interactive)
    (when (region-active-p)
      (meow--cancel-selection))
    (meow--execute-kbd-macro meow--kbd-redo))

  (defun meow-redo-in-selection ()
    "Cancel redo in current region."
    (interactive)
    (when (region-active-p)
      (meow--execute-kbd- meow--kbd-redo)))

  ;; This is merely from:
  ;; https://github.com/meow-edit/meow/blob/master/KEYBINDING_QWERTY.org
  (defun meow:setup ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
    (meow-motion-overwrite-define-key
     '("j" . meow-next)
     '("k" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
      ;; SPC j/k will run the original command in MOTION state.
     '("j" . "H-j")
     '("k" . "H-k")
      ;; Use SPC (0-9) for digit arguments.
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument)
      ;; Other leader keys
     '("/" . meow-keypad-describe-key)
     '("?" . meow-cheatsheet)
     '("D" . backward-kill-word))
    (meow-normal-define-key
     ;; Meow keys
     '("0" . meow-expand-0)
     '("9" . meow-expand-9)
     '("8" . meow-expand-8)
     '("7" . meow-expand-7)
     '("6" . meow-expand-6)
     '("5" . meow-expand-5)
     '("4" . meow-expand-4)
     '("3" . meow-expand-3)
     '("2" . meow-expand-2)
     '("1" . meow-expand-1)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("d" . meow-delete)
     '("D" . meow-backward-delete)
     '("e" . meow-next-word)
     '("E" . meow-next-symbol)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-left)
     '("H" . meow-left-expand)
     '("i" . meow-insert)
     '("I" . meow-open-above)
     '("j" . meow-next)
     '("J" . meow-next-expand)
     '("k" . meow-prev)
     '("K" . meow-prev-expand)
     '("l" . meow-right)
     '("L" . meow-right-expand)
     '("m" . meow-join)
     '("n" . meow-search)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("Q" . meow-goto-line)
     '("r" . meow-replace)
     '("R" . meow-swap-grab)
     '("s" . meow-kill)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-redo) ;; TODO figure out where to bind ‘meow-undo/redo-in-selection’
     '("v" . meow-visit)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("x" . meow-line)
     '("X" . meow-goto-line)
     '("y" . meow-save)
     '("Y" . meow-sync-grab)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore))
    ;; Other state stuff
    (setq meow-cursor-type-paren 'hollow))
  :custom
  (meow-use-clipboard t) ;; not recommended but I don’t know why I wouldn’t want this
  (meow-goto-line-function #'consult-goto-line)
  :config
  (defvar meow:paren-keymap (make-keymap)
    "A keymap used in defining the paren state for Puni in Meow.")

  (meow-define-state paren
    "meow state for interacting with puni"
    :lighter " [P]"
    :keymap meow:paren-keymap)

  (defvar meow:goto-keymap (make-keymap)
    "A keymap used in defining the goto state in Meow.")

  (meow-define-state goto
    "meow goto state for moving around buffers or windows."
    :lighter " [G]"
    :keymap meow:goto-keymap)
  
  (meow:setup)

  (meow-define-keys 'paren
                    '("<escape>" . meow-normal-mode)
                    '("a" . puni-beginning-of-sexp)
                    '("e" . puni-end-of-sexp)
                    '("l" . puni-forward-sexp)
                    '("h" . puni-backward-sexp)
                    '("k" . puni-up-list)
                    '("n" . puni-slurp-forward)
                    '("c" . puni-slurp-backward)
                    '("b" . puni-barf-forward)
                    '("v" . puni-barf-backward)
                    '("s" . puni-splice)
                    '("p" . puni-split)
                    '("r" . puni-raise)
                    '("u" . meow-undo)
                    '("U" . meow-redo))

  ;; TODO do I really need this?
  ;; (meow-define-keys 'goto
  ;;   '("<escacpe>" . meow-normal-mode)
  ;;   '("h" . move-beginning-of-line)
  ;;   '("l" . move-end-of-line)
  ;;   '(""))
  
  (meow-global-mode +1))

(use-package meow-tree-sitter
  :if (>= emacs-major-version 29)
  :after meow
  :config
  (meow-tree-sitter-register-defaults))

;;;; Devil (technically not modal editing)
;;;;

(use-package devil
  :defer t
  ;;:hook (after-init . global-devil-mode)
  ;;:general (:keymaps 'global-map
  ;;          "C-," #'global-devil-mode)
  :config
  (devil-set-key (kbd ","))
  (assoc-delete-all "%k SPC" devil-special-keys))

(provide 'modal)
;;; modal.el ends here
