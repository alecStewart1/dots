;;; programming.el --- Configurations for programming -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;  Packages that I use for programming, as well as some code
;;  I use to format said code.
;;
;;  TODO
;;  - add in Polymode?
;;  - fix formatter stuff
;;;
;;; Code:

(require 'lib)
(require 'snippets)
(require 'cl-macs)
(require 'cl-lib)
(require 'cl-seq)
(require 'subr-x)
(require 'pcase)
(require 'mode-local)
(require 'autoinsert)
;;(require 'ht)
(require 'f)
(require 'dash)
(require 'general)

;;; Packages
;;;

;;;; Utilities
;;;;


;;;;; Format things with Apheleia
;;;;;

(use-package apheleia
  :ensure (:wait t)
  :demand t
  :config
  (apheleia-global-mode +1))

;;;;; Compiling things
;;;;;

(use-package compile
  :ensure nil
  :hook (compilation-filter . compile:colorize-compilation-buffer)
  :preface
  (defun compile:colorize-compilation-buffer ()
    "ANSI coloring in the compilation buffers."
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  :custom
  (compilation-always-kill 1)
  (compilation-ask-about-save nil)
  (compilation-scroll-output 'first-error))

;;;;; Flymake
;;;;;

(use-package flymake
  :ensure nil
  ;; Flymake has this funny thing where it doesn’t
  ;; trigger proper when you hook it into ‘prog-mode’.
  ;; I imagine that’s due to flymake not knowing how to add a checker for the
  ;; generic prog-mode.
  :hook ((prog-mode
          elisp-mode
          emacs-lisp-mode
          awk-mode
          awk-ts-mode
          bash-ts-mode
          c-mode
          c-ts-mode
          c++-mode
          c++-ts-mode
          elixir-mode
          elixir-ts-mode
          lua-mode
          lua-ts-mode
          nxml-mode
          org-mode
          python-mode
          python-ts-mode
          ruby-mode
          ruby-ts-mode
          sh-mode
          lsp-mode) . flymake-mode)
  :custom
  (flymake-suppress-zero-counters t)
  (flymake-show-diagnostics-at-end-of-line emacs30-p)
  :config
  (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake))

(use-package flymake-collection
  :ensure (:wait t)
  :demand t
  :config
  (flymake-collection-hook-setup)
  (require 'flymake-collection-define)
  (push '(prog-mode
          (flymake-collection-codespell
           :predicate (lambda () (executable-find "codespell"))))
        flymake-collection-hook-config)

  (setf (alist-get 'org-mode flymake-collection-hook-config)
        '((flymake-collection-vale
           :predicate (lambda () (executable-find "vale")))
          (flymake-collection-proselint :disabled t))))

;;;;; For when we use flycheck (I don’t)
;;;;;

(use-package consult-flycheck
  :when (and
         (elpaca-installed-p 'flycheck)
         (bound-and-true-p flycheck-mode))
  :after (consult flycheck))


;;;;; Uh...this thing
;;;;;

(use-package goto-addr
  :ensure nil
  :hook (prog-mode . goto-address-mode))

;;;;; Get brief documentation in the message area
;;;;;

(use-package eldoc
  :ensure nil
  :diminish
  :init
  (unless emacs28-p
    (setq-default eldoc-documentation-format-function #'eldoc-documentation-format-concat-hr))
  :custom
  (eldoc-documentation-strategy 'eldoc-documentation-compose-eagerly)
  (eldoc-idle-delay 0.1))

;;;;; Configuration files
;;;;;

(use-package conf-mode
  :ensure nil
  :mode "\\.cfg\\’")

(use-package yaml-mode
  :mode "\\.yml\\’"
  :mode "\\.yaml\\’"
  :defines yaml-mode-hook
  :hook (yaml-mode . lsp)
  :config
  (general-setq yaml-ts-mode-hook yaml-mode-hook))

(use-package yuck-mode
  :mode "\\.yuck")

;;;;; CSV
;;;;;

(use-package csv-mode
  :mode "\\.csv\\’")

;;;;; Gentoo specific stuff
;;;;;

(use-package ebuild-mode
  :if (and gentoo-p (f-dir-p "/usr/share/emacs/site-lisp/"))
  :load-path "/usr/share/emacs/site-lisp/ebuild-mode/"
  :mode "\\.ebuild\\'")

(use-package company-ebuild
  :if (and gentoo-p (f-dir-p "/usr/share/emacs/site-lisp/"))
  :load-path "/usr/share/emacs/site-lisp/company-ebuild/"
  :preface
  (defalias 'cape:ebuild-capf
    (cape-capf-buster (cape-company-to-capf #'company-ebuild)))
  :config
  (setq-mode-local ebuild-mode
                   completion-at-point-functions (list #'cape:ebuild-capf
                                                       #'cape-file)))

;;;;; RMSBolt, deeply inspect you code
;;;;;

(use-package rmsbolt
  :after (:any c-mode c++-mode objc-mode emacs-lisp-mode common-lisp-mode python-mode java-mode))

;;;;; Treesit
;;;;;

(use-package treesit
  :ensure nil
  :custom
  ;; Some stuff taken from here: https://robbmann.io/posts/emacs-treesit-auto/
  (treesit-extra-load-path `("/usr/lib64/" ,(concat my-etc-dir "tree-sitter/")))
  (treesit-language-source-alist
   '((awk .             ("https://github.com/Beaglefoot/tree-sitter-awk"))
     (bash .            ("https://github.com/tree-sitter/tree-sitter-bash"))
     (bibtex .          ("https://github.com/latex-lsp/tree-sitter-bibtex"))
     (c .               ("https://github.com/tree-sitter/tree-sitter-c"))
     (c++ .             ("https://github.com/tree-sitter/tree-sitter-cpp"))
     (commonlisp .      ("https://github.com/tree-sitter-grammars/tree-sitter-commonlisp"))
     (csharp .          ("https://github.com/tree-sitter/tree-sitter-c-sharp"))
     (clojure .         ("https://github.com/sogaiu/tree-sitter-clojure"))
     (css .             ("https://github.com/tree-sitter/tree-sitter-css"))
     (csv .             ("https://github.com/tree-sitter-grammars/tree-sitter-csv"))
     (diff .            ("https://github.com/the-mikedavis/tree-sitter-diff"))
     (dockerfile.       ("https://github.com/camdencheek/tree-sitter-dockerfile"))
     (eex .             ("https://github.com/connorlay/tree-sitter-eex"))
     (elixir .          ("https://github.com/elixir-lang/tree-sitter-elixir"))
     (erlang .          ("https://github.com/WhatsApp/tree-sitter-erlang"))
     (fennel .          ("https://github.com/alexmozaidze/tree-sitter-fennel"))
     (fish .            ("https://github.com/ram02z/tree-sitter-fish"))
     (go  .             ("https://github.com/tree-sitter/tree-sitter-go"))
     (heex .            ("https://github.com/phoenixframework/tree-sitter-heex"))
     (html .            ("https://github.com/tree-sitter/tree-sitter-html"))
     (java .            ("https://github.com/tree-sitter/tree-sitter-java"))
     (javascript .      ("https://github.com/tree-sitter/tree-sitter-javascript"))
     (jq .              ("https://github.com/nverno/tree-sitter-jq"))
     (jsdoc .           ("https://github.com/tree-sitter/tree-sitter-jsdoc"))
     (json .            ("https://github.com/tree-sitter/tree-sitter-json"))
     (julia .           ("https://github.com/tree-sitter/tree-sitter-julia"))
     (just  .           ("https://github.com/IndianBoy42/tree-sitter-just"))
     (kotlin .          ("https://github.com/fwcd/tree-sitter-kotlin"))
     (latex .           ("https://github.com/latex-lsp/tree-sitter-latex"))
     (lua .             ("https://github.com/tree-sitter-grammars/tree-sitter-lua"))
     (luadoc .          ("https://github.com/tree-sitter-grammars/tree-sitter-luadoc"))
     (luap .            ("https://github.com/tree-sitter-grammars/tree-sitter-luap"))
     (ledger .          ("https://github.com/cbarrete/tree-sitter-ledger"))
     (makefile .        ("https://github.com/tree-sitter-grammars/tree-sitter-make"))
     (markdown .        ("https://github.com/tree-sitter-grammars/tree-sitter-markdown" "split_parser" "tree-sitter-markdown/src"))
     (markdown-inline . ("https://github.com/tree-sitter-grammars/tree-sitter-markdown" "split_parser" "tree-sitter-markdown-inline/src"))
     (org .             ("https://github.com/milisims/tree-sitter-org"))
     (perl .            ("https://github.com/tree-sitter-perl/tree-sitter-perl"))
     (php .             ("https://github.com/tree-sitter/tree-sitter-php"))
     (prolog .          ("https://codeberg.org/foxy/tree-sitter-prolog"))
     (python .          ("https://github.com/tree-sitter/tree-sitter-python"))
     (r .               ("https://github.com/r-lib/tree-sitter-r"))
     (racket .          ("https://github.com/6cdh/tree-sitter-racket"))
     (ruby .            ("https://github.com/tree-sitter/tree-sitter-ruby"))
     (rust .            ("https://github.com/tree-sitter/tree-sitter-rust"))
     (scss .            ("https://github.com/serenadeai/tree-sitter-scss"))
     (sql .             ("https://github.com/m-novikov/tree-sitter-sql"))
     (surface .         ("https://github.com/connorlay/tree-sitter-surface"))
     (svelte .          ("https://github.com/tree-sitter-grammars/tree-sitter-svelte"))
     (toml .            ("https://github.com/tree-sitter/tree-sitter-toml"))
     (tsx .             ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src"))
     (typescript .      ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src"))
     (typst .           ("https://github.com/uben0/tree-sitter-typst"))
     (vue .             ("https://github.com/tree-sitter-grammars/tree-sitter-vue"))
     (xml .             ("https://github.com/tree-sitter-grammars/tree-sitter-xml"))
     (yaml .            ("https://github.com/tree-sitter-grammars/tree-sitter-yaml"))
     (yuck .            ("https://github.com/Philipp-M/tree-sitter-yuck"))))
  ;;(treesit-font-lock-settings t)
  (treesit-font-lock-level 4)
  (treesit-simple-indent t)
  (treesit-defun-type-regexp t))

(use-package treesit-auto
  :demand t
  :after treesit
  :custom
  (treesit-auto-install t)
  (treesit-auto-langs
   '(awk
     bash
     bibtex
     c
     c-sharp
     clojure
     cmake
     commonlisp
     cpp
     css
     ;; dart
     dockerfile
     eex
     elixir
     go
     gomod
     heex
     html
     java
     javascript
     json
     julia
     kotlin
     ledger
     latex
     lua
     make
     markdown
     nu
     ;; proto
     python
     r
     ruby
     rust
     sql
     toml
     tsx
     typescript
     typst
     ;; verilog
     ;; vhdl
     ;; wat
     ;; wast
     yaml))
  (treesit-auto-fallback-alist
   '((toml-ts-mode . conf-toml-mode)
     (typescript-ts-mode . nil)
     (tsx-ts-mode . nil)))
  :config
  (defconst fish-ts-auto-recipe
    (make-treesit-auto-recipe
     :lang 'fish
     :ts-mode 'fish-ts-mode
     :remap 'fish-mode
     :url "https://github.com/ram02z/tree-sitter-fish"
     :revision "master"
     :source-dir "src"
     :ext "\\.fish\\'")
    "‘treesit-auto’ recipe for fish-shell scripts")

  (add-to-list 'treesit-auto-recipe-list fish-ts-auto-recipe)

  (defconst ledger-ts-auto-recipe
    (make-treesit-auto-recipe
     :lang 'ledger
     :ts-mode 'ledger-ts-mode
     :remap 'ledger-mode
     :url "https://github.com/cbarrete/tree-sitter-ledger"
     :ext "\\.\\(dat\\|hledger\\|journal\\)\\'")
    "‘treesit-auto’ recipe for Ledger.’")

  (add-to-list 'treesit-auto-recipe-list ledger-ts-auto-recipe)

  (defconst sql-ts-auto-config
    (make-treesit-auto-recipe
     :lang 'sql
     :ts-mode 'sql-ts-mode
     :remap 'sql-mode
     :requires nil
     :url "https://github.com/m-novikov/tree-sitter-sql"
     :revision "master"
     :source-dir "src"
     :ext "\\.sql\\'")
    "‘treesit-auto’ recipe for SQL.")

  (add-to-list 'treesit-auto-recipe-list sql-ts-auto-config)

  (defconst fennel-ts-auto-config
    (make-treesit-auto-recipe
     :lang 'fennel
     :ts-mode 'fennel-ts-mode
     :remap 'fennel-mode
     :requires nil
     :url "https://github.com/TravonteD/tree-sitter-fennel"
     :revision "master"
     :source-dir "src"
     :ext "\\.fnl\\'")
    "‘treesit-auto’ recipe for Fennel.")

  (add-to-list 'treesit-auto-recipe-list fennel-ts-auto-config)

  ;; Just in case some things aren’t quite right.
  ;; (defconst julia-ts-0.23-auto-config
  ;;   (make-treesit-auto-recipe
  ;;    :lang 'julia
  ;;    :ts-mode 'julia-ts-mode
  ;;    :remap 'julia-mode
  ;;    :requires nil
  ;;    :url "https://github.com/tree-sitter/tree-sitter-julia"
  ;;    :revision "v0.23.0"
  ;;    :source-dir "src"
  ;;    :ext "\\.jl\\'")
  ;;   "‘treesit-auto’ recipe for a specific version of the Julia treesitter grammer.")

  ;; (add-to-list 'make-treesit-auto-recipe julia-ts-0.23-auto-config)

  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode t))

;;;;; Projects
;;;;;

;;;;;; Project.el
;;;;;;

;; TODO
;; - Figure out if we can move away from projection.
;; - Figure out how to customize explicit "actions" for a project
;; - Figure out how to find a project’s root on a remote filesystem.
;;
;; Some configurations taken from woolsweater’s config:
;; https://gitlab.com/woolsweater/dotemacs.d/-/blob/main/modules/conjecture.el
;; other bits taken from:
;; https://github.com/angrybacon/dotemacs/blob/master/dotemacs.org#projectel

(use-package project
  :ensure nil
  :preface

  ;; (defun project::project-root-find-expr (files dir)
  ;;   "TODO"
  ;;   (if (listp files)
  ;;       `(cl-find-if (lambda (f) (locate-dominating-file ,dir f)) ,files)
  ;;     `(expand-file-name (locate-dominating-file ,dir ,files))))

  ;; (defmacro def-project-type (type &keys files)
  ;;   "Define a new project.el project TYPE, using FILES to denote the project root."
  ;;   (declare (debug t)
  ;;            (indent defun))
  ;;   (let* ((sym-str (symbol-name `,type))
  ;;          (type-sym (intern sym-str))
  ;;          (try-fn-name (intern (concat "project-try-" sym-str "-fn")))
  ;;          (dir-sym (gensym "dir-")))
  ;;     (macroexp-progn
  ;;      `((defun ,try-fn-name (,dir-sym)
  ;;          (let ((root ,(project::project-root-find-expr files dir-sym)))
  ;;            (if (file-exists-p root)
  ;;                (cons ',type-sym root))))
  ;;        (add-hook 'project-find-functions #',try-fn-name)
  ;;        (defmethod project-root ((project (head ,type-sym)))
  ;;          (when-let ((root (caddr project)))
  ;;            (file-name-as-directory (file-name-directory root))))))))

  (cl-defmethod project-root ((project (head vc)))
    "Only return the PROJECT root when it is not nil."
    (when-let* ((root (caddr project)))
      (file-name-as-directory (file-name-directory root))))

  (cl-defmethod project-name ((project (head vc)))
    "Return the name of PROJECT if specified."
    (when-let* ((root (caddr project)))
      (file-name-nondirectory
       (directory-file-name
        (file-name-directory root)))))

  ;; (cl-defmethod project-files :around (project &optional dirs)
  ;;   (if-let ((fd (executable-find "fd")))
  ;;       (let* ((search-dirs (string-join (or dirs (list (project-root project))) " "))
  ;;              (command (format "%s --type f --print0 ’.*’ %s" fd search-dirs)))
  ;;         (split-string (shell-command-to-string command) "\0" t))
  ;;     (cl-call-next-method project dirs)))

  (defun project:search ()
    "Find files in ‘project-root’ with ripgrep/grep."
    (interactive)
    (let ((proot (project-root (project-current))))
      (if (executable-find "rg")
          (consult-ripgrep proot)
        (consult-grep proot))))

  (defun project:save (&rest _)
    "Save file-visiting buffers under the current project root."
    (interactive)
    (save-some-buffers t #'save-some-buffers-root))

  ;; (defun switch-to-prev-buffer-in-project ()
  ;;   ""
  ;;   (let* ((window (window-normalize-window (selected-window) t))
  ;;          ))

  (defun project:eshell ()
    "Run an eshell instance in the current project directory."
    (interactive)
    (let* ((default-directory (project-root (project-current)))
           (eshell-buffer-name (project-prefixed-buffer-name "eshell"))
           (eshell-buffer (get-buffer eshell-buffer-name)))
      (if (and eshell-buffer (not current-prefix-arg))
          (pop-to-buffer eshell-buffer '((display-buffer-below-selected . ((window-height . 14)
                                                                           (window-min-height . 8)))))
        (eshell t))))

  (defun project:vterm ()
    "TODO"
    (interactive)
    (let* ((default-directory (project-root (project-current)))
           (vterm-buffer-name (project-prefixed-buffer-name "vterm"))
           (vterm-buffer (get-buffer vterm-buffer-name)))
      (if (and vterm-buffer (not current-prefix-arg))
          (pop-to-buffer vterm-buffer '((display-buffer-below-selected . ((window-height . 14)
                                                                          (window-min-height . 8)))))
        (vterm t))))
  :custom
  (project-vc-extra-root-markers
   '(".project"))
  (project-vc-ignores
   '("*/.git/*"
     "*/node_modules/*"
     "*/deps/*"
     "*/build/**"
     "*/_build/*"
     "*/__pycache__/*"
     "DS_Store"
     "*/.vscode/*"
     "*/.elixir_ls/*"
     "*.zip"
     "*.o"
     "*.obj"
     "*.bin"
     "*.pyc"
     "*.jar"))
  (project-list-file (concat my-cache-dir "projects.eld"))
  (project-switch-commands
   '((project-dired "Open project root in dired" "D")
     (project-find-file "Find file in project" "f")
     (project:search "Search in project" "G")
     (project-query-replace-regexp "Query and replace in project" "r")
     (magit-project-status "Git status of project" "i")
     (project-shell "Open shell for project" "s")
     (project:eshell "Open eshell for project" "e")
     (project:save "Save all files in project" "C-s")
     (project-forget-project "Forget the current project" "t")
     (project-forget-zombie-projects "Forget zombie projects" "T")))
  (define-key project-prefix-map (kbd "G") #'project:search)
  (define-key project-prefix-map (kbd "i") #'magit-project-status)
  (define-key project-prefix-map (kbd "e") #'project:eshell)
  (define-key project-prefix-map (kbd "C-s") #'project:save)
  (define-key project-prefix-map (kbd "t") #'project-forget-project)
  (define-key project-prefix-map (kbd "T") #'project-forget-zombie-projects))

;;;;;; Projection
;;;;;;

(use-package projection
  :requires project
  :hook (elpaca-after-init . global-projection-hook-mode)
  :config
  (defvar projection-project-type-emacs-eask
    (projection-type
     :name 'emacs-eask
     :predicate "Eask"
     :build "eask compile"
     :run "eask emacs"
     :test "eask test"
     :test-prefix "test-"
     :test-suffix "-test")
    "‘projection’ project type for working with Eask.")

  (add-to-list 'projection-project-types projection-project-type-emacs-eask 'append)

  (defvar projection-project-type-nuxt-npm
    (projection-type
     :name 'nuxt-npm
     :predicate '("nuxt.config.ts" "package-lock.json")
     :build "npm run build"
     :run "npm run dev"
     :test "npm run test")
    "‘projection’ project type for working with NPM and Nuxt.js")

  
  (add-to-list 'projection-project-types projection-project-type-nuxt-npm 'append)

  (defvar projection-project-type-nuxt-pnpm
    (projection-type
     :name 'nuxt-pnpm
     :predicate '("nuxt.config.ts" "pnpm-lock.yaml")
     :build "pnpm run build"
     :run "pnpm run dev"
     :test "pnpm run test")
    "‘projection’ project type for working with pnpm and Nuxt.js")

  (add-to-list 'projection-project-types projection-project-type-nuxt-pnpm 'append)

  (define-key 'global-map (kbd "C-x P") #'projection-map))

(use-package projection-multi
  :after projection
  :general
  (:keymaps 'projection-map
   "RET" #'projection-multi-compile))

(use-package projection-multi-embark
  :after embark
  :after projection-multi
  :demand t
  :config (projection-multi-embark-setup-command-map))

;;;; Coding and programming modes
;;;;

;;;;; Eglot
;;;;;

;; (use-package eglot
;;   :ensure nil
;;   :demand t
;;   :preface
;;   (defvar lsp:defer-shutdown 3)
;;   :custom
;;   (eglot-sync-connect 1)
;;   (eglot-connect-timeout 10)
;;   (eglot-autoshutdown t)
;;   (eglot-send-changes-idle-time 0.5)
;;   :config
;;   (defadvice! lsp:defer-server-shutdown (fn &optional server)
;;     "From Doom Emacs:
;; Defer server shutdown for a few seconds.
;; This gives the user a chance to open other project files before the server is
;; auto-killed (which is a potentially expensive process). It also prevents the
;; server getting expensively restarted when reverting buffers."
;;     :around #'eglot--managed-mode
;;     (letf! (defun eglot-shutdown (server)
;;              (if (or (null lsp:defer-shutdown)
;;                      (eq lsp:defer-shutdown 0))
;;                  (funcall eglot-shutdown server)
;;                (run-at-time
;;                 (if (numberp lsp:defer-shutdown) lsp:defer-shutdown 3)
;;                 nil (lambda (server)
;;                       (unless (eglot--managed-buffers server)
;;                         (funcall eglot-shutdown server)))
;;                 server)))
;;       (funcall fn server)))

;;   (add-to-list 'eglot-server-programs `(elixir-mode ,(expand-file-name "~/Projects/elixir-ls/release/language_server.sh"))))

;;;;; LSP and DAP
;;;;;

(use-package lsp-mode
  :ensure (:wait t)
  :commands (lsp
             lsp-format-buffer
             lsp-organize-imports
             lsp-install-server)
  :preface
  (defvar lsp:defer-shutdown 3)

  (defun lsp:orderless-dispatch-flex-1st (_pattern index _total)
    (and (eq index 0) 'orderless-flex))

  (defun lsp:setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless partial-completion)))

  (defalias 'cape:lsp-busted-cape (cape-capf-buster #'lsp-completion-at-point))

  (defalias 'cape:lsp-extra-capes (cape-capf-super
                                   #'cape-abbrev
                                   #'cape-dabbrev
                                   #'cape-keyword))
  (defvar cape:lsp-cape (list
                         #'cape:lsp-busted-cape
                         (cape-capf-buster #'cape:lsp-extra-capes)
                         #'cape-file))

  (defun lsp-booster:advice-json-parse (old-fn &rest args)
    "Try to parse bytecode instead of json."
    (or
     (when (equal (following-char) ?#)
       (let ((bytecode (read (current-buffer))))
         (when (byte-code-function-p bytecode)
           (funcall bytecode))))
     (apply old-fn args)))

  (defun lsp-booster:advice-final-command (old-fn cmd &optional test?)
    "Prepend emacs-lsp-booster command to lsp CMD."
    (let ((orig-result (funcall old-fn cmd test?)))
      (if (and (not test?)                             ; for check lsp-server-present?
               (not (file-remote-p default-directory)) ; see lsp-resolve-final-command, it would add extra shell wrapper
               lsp-use-plists
               (not (functionp 'json-rpc-connection))  ; native json-rpc
               (executable-find "emacs-lsp-booster"))
          (progn
            (when-let* ((command-from-exec-path (executable-find (car orig-result))))  ; resolve command from exec-path (in case not found in $PATH)
              (setcar orig-result command-from-exec-path))
            (message "Using emacs-lsp-booster for %s!" orig-result)
            (cons "emacs-lsp-booster" orig-result))
        orig-result)))
  :init
  (setenv "LSP_USE_PLISTS" "true")
  (general-setq lsp-session-file (concat my-etc-dir "lsp-session")
                lsp-server-install-dir (concat my-etc-dir "lsp/")
                lsp-use-plists t)

  (add-hook 'orderless-style-dispatchers #'lsp:orderless-dispatch-flex-1st
            nil
            'local)

  (general-add-hook 'lsp-completion-mode #'lsp:setup-completion t)
  
  ;; Make ‘lsp-completion-at-point’ noninterruptible and nonexclusive
  (advice-add #'lsp-completion-at-point :around #'cape-wrap-noninterruptible)
  (advice-add #'lsp-completion-at-point :around #'cape-wrap-nonexclusive)
  :custom
  (lsp-enable-suggest-server-download t)
  (lsp-idle-delay 0.25)
  (lsp-completion-provider :none) ; we use Corfu instead
  (lsp-diagnostics-provider :flymake)
  (lsp-enable-folding nil)
  (lsp-enable-text-document-color nil)
  (lsp-enable-on-type-formatting t)
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-keymap-prefix nil)
  (lsp-enable-snippet nil)
  (lsp-keep-workspace-alive nil)
  (lsp-disabled-clients
   (list 'semgrep-ls 'emmy-lua 'lsp-lua-lsp 'lua-roblox-language-server 'pyls))
  :config
  (advice-add (if (progn (require 'json)
                         (fboundp 'json-parse-buffer))
                  #'json-parse-buffer
                #'json-read)
      :around
    #'lsp-booster:advice-json-parse)

  (advice-add #'lsp-resolve-final-command :around #'lsp-booster:advice-final-command)

  ;; You _have_ to explicitly set this here. ‘lsp-mode’
  ;; sets this somewhere to ‘’(lsp-completion-at-point tags-completion-at-point-function)’ if
  ;; you don’t.
  (setq completion-at-point-functions cape:lsp-cape)

  (defadvice! lsp:respect-user-defined-checkers (orig-fn &rest args)
    :around #'lsp-diagnostics-flycheck-enable
    (if flycheck-checker
        (let ((old-checker flycheck-checker))
          (apply orig-fn args)
          (setq-local flycheck-checker old-checker))
      (apply orig-fn args)))

  (defvar lsp:deferred-shutdown-timer nil)
  (defadvice! lsp:defer-server-shutdown (orig-fn &optional restart)
    "Defer server shutdown for a few seconds.
  This gives the user a chance to open other project files before the server is
  auto-killed (which is a potentially expensive process). It also prevents the
  server getting expensively restarted when reverting buffers."
    :around #'lsp--shutdown-workspace
    (if (or lsp-keep-workspace-alive
            restart
            (null lsp:defer-shutdown)
            (= lsp:defer-shutdown 0))
        (funcall orig-fn restart)
      (when (timerp lsp:deferred-shutdown-timer)
        (cancel-timer lsp:deferred-shutdown-timer))
      (setq lsp:deferred-shutdown-timer
            (run-at-time
             (if (numberp lsp:defer-shutdown) lsp:defer-shutdown 3)
             nil (lambda (workspace)
                   (with-lsp-workspace workspace
                     (unless (lsp--workspace-buffers workspace)
                       (let ((lsp-restart 'ignore))
                         (funcall orig-fn)))))
             lsp--cur-workspace))))

  (add-hook! 'lsp-mode-hook
    (defun lsp:display-guessed-project-root ()
      "Log what LSP things is the root of the current project."
      ;; Makes it easier to detect root resolution issues.
      (when-let* ((path (buffer-file-name (buffer-base-buffer))))
        (if-let* ((root (lsp--calculate-root (lsp-session) path)))
            (lsp--info "Guessed project root is %s" (abbreviate-file-name root))
          (lsp--info "Could not guess project root.")))))

  ;; Register some new clients

  (add-to-list 'lsp-language-id-configuration '(typst-ts-mode . "typst"))
  
  (lsp-register-client
   (make-lsp-client
    :new-connection (lsp-stdio-connection "tinymist")
    :major-modes '(typst-ts-mode)
    :activation-fn (lsp-activate-on "typst")
    :server-id 'tinymist))

  ;; Set preferred lsp servers here, AFTER lsp-mode loads

  (setq-mode-local awk-ts-mode
                   lsp-enabled-clients '(awkls))

  (setq-mode-local elixir-mode
                   lsp-enabled-clients '(elixir-ls))

  (setq-mode-local lua-mode
                   lsp-enabled-clients '(lua-language-server))

  (setq-mode-local python-mode
                   lsp-enabled-clients '(ruff-lsp pyright))

  (setq-mode-local typst-ts-mode
                   lsp-enabled-clients '(tinymist))
  
  (setq-mode-local web-mode
                   lsp-enabled-clients '(html-ls css-ls emmet-ls))

  (setq-mode-local js-mode
                   lsp-enabled-clients '(eslint))

  (setq-mode-local js-jsx-mode
                   lsp-enabled-clients '(eslint))

  (setq-mode-local js-ts-mode
                   lsp-enabled-clients '(eslint))

  (setq-mode-local typescript-mode
                   lsp-enabled-clients '(eslint))

  (setq-mode-local typescript-ts-mode
                   lsp-enabled-clients '(eslint))
  
  (setq-mode-local web-svelte-mode
                   lsp-enabled-clients '(svelte-ls ts-ls eslint))

  (setq-mode-local web-vue-mode
                   lsp-enabled-clients '(vue-semantic-server ts-ls eslint)))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :init
  ;; This is from Doom Emacs
  (defadvice! lsp-ui:use-hook-instead-a (fn &rest args)
    "Change `lsp--auto-configure' to not force `lsp-ui-mode' on us. Using a hook
instead is more sensible."
    :around #'lsp--auto-configure
    (letf! ((#'lsp-ui-mode #'ignore))
      (apply fn args)))
  :custom
  (lsp-ui-peek-enable t)
  (lsp-ui-doc-enable t)
  (lsp-ui-doc-max-height 8)
  (lsp-ui-doc-max-width 72)
  (lsp-ui-doc-delay 0.75)
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-doc-show-with-mouse nil)  ; don't disappear on mouseover
  (lsp-ui-doc-position 'at-point)
  ;; Don't show symbol definitions in the sideline. They are pretty noisy,
  ;; and there is a bug preventing Flycheck errors from being shown (the
  ;; errors flash briefly and then disappear).
  (lsp-ui-sideline-enable nil)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-actions-icon lsp-ui-sideline-actions-icon-default))

(use-package lsp-treemacs
  :after (treemacs lsp-mode))

(use-package dap-mode
  :ensure (:wait t)
  :after lsp-mode
  :hook (dap-mode . dap-tooltip-mode)
  :init
  (with-eval-after-load 'lsp
    (require 'dap-mode))
  (general-setq gdb-show-main t
                gdb-many-windows t)
  :custom
  (dap-breakpoints-file (concat my-etc-dir "dap-breakpoints"))
  (dap-utils-extension-path (concat my-etc-dir "dap-extension/"))
  :config
  (dap-mode 1)
  (require 'dap-ui)
  (add-hook 'dap-mode-hook #'dap-ui-mode)
  (add-hook 'dap-ui-mode-hook #'dap-ui-controls-mode))

(use-package consult-lsp
  :after (consult lsp-mode)
  :general
  (:keymaps 'lsp-mode-map
   [remap xref-find-apropos] #'consult-lsp-symbols))


;;;;; Assembler
;;;;;

;; (use-package asm-mode
;;   :ensure nil
;;   :mode "\\.inc$")

;; (use-package nasm-mode
;;   :mode "\\.nasm$")

;; (use-package flymake-nasm
;;   :hook (nasm-mode . flymake-nasm-setup))

;; ;; (use-package mips-mode
;; ;;   :mode "\\.mips$")

;; (use-package masm-mode
;;   :if (or windows-nt-p cygwin-p)
;;   :mode "\\.masm$")

;;;;; C/C++, Objective-C
;;;;;

(use-package cc-mode
  :ensure nil
  :mode ("\\.mm\\'" . objc-mode)
  :hook ((c-mode-local-vars
          c++-mode-local-vars
          objc-mode-local-vars
          cmake-mode-local-vars) . lsp)
  :hook (c-mode-common . rainbow-delimiters-mode)
  :custom
  (c-default-style "bsd")
  (c-basic-offset tab-width)
  (c-backspace-function #'delete-backward-char)
  (lsp-clangd-binary-path (executable-find "clangd"))
  (lsp-clients-clangd-executable (executable-find "clangd"))
  (lsp-clients-clangd-library-directories '("/usr" "/usr/lib" "/usr/lib64" "/lib" "/lib64")))

;;;;; Awk
;;;;;

(use-package awk-ts-mode
  :mode "\\.[mg]?awk\\'"
  :hook (awk-ts-mode . lsp))

;;;;; Shell
;;;;;

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

(use-package sh-script
  :ensure nil
  :mode ("\\.\\(?:zunit\\|env\\)\\'" . sh-mode)
  :mode ("/bspwmrc\\'" . sh-mode)
  :preface
  (defvar sh-shell-file)
  (defvar sh-builtin-keywords
    '("cat" "cd" "chmod" "chown" "cp" "curl" "date" "echo" "find" "git" "grep"
      "kill" "less" "ln" "ls" "make" "mkdir" "mv" "pgrep" "pkill" "pwd" "rm"
      "sleep" "sudo" "touch")
    "A list of common shell commands to be fontified especially in `sh-mode'.")

  (defun sh:get-shebang ()
    (let ((shabang "#!/usr/bin/env ")
          (uname-s (shell-command-to-string "uname -s")))
      (cl-case (and (eql major-mode 'sh-mode)
                    (boundp 'sh-shell)
                    (symbol-value 'sh-shell))
        ((or mksh (equal uname-s "MirBSD")) (concat shabang "mksh"))
        ((or ksh (equal uname-s "OpenBSD")) (concat shabang "ksh"))
        (fish                               (concat shabang "fish"))
        (zsh                                (concat shabang "zsh"))
        (bash                               (concat shabang "bash"))
        (t                                  (concat shabang "sh")))))

  (define-auto-insert
    '(sh-mode . "Shell Shebang")
    '(nil
      (if (equal (file-name-extension (buffer-file-name)) "sh")
          ;; Default to using POSIX sh for files with .sh extension.
          ;; For now.
          "#!/usr/bin/env sh"
        (sh:get-shebang))
      ?\n?\n
      _ ))
  :config
  (general-setq bash-ts-mode-hook sh-mode-hook)

  (push
   '(sh-mode
     (flymake-collection-shellcheck
      :predicate (lambda () (executable-find "shellcheck"))))
   flymake-collection-hook-config)

  ;; TODO uhhh...where do I use these two functions?
  (defun sh-script:match-variables-in-quotes (limit)
    "Search for variables in double-quoted strings bounded by LIMIT."
    (with-syntax-table sh-mode-syntax-table
      (let (res)
        (while (and (setq res (re-search-forward
                               "[^\\]\\(\\$\\)\\({.+?}\\|\\<[a-zA-Z0-9_]+\\|[@*#!]\\)"
                               limit t))
                    (not (eq (nth 3 (syntax-ppss)) ?\"))))
        res)))

  (defun sh-script:match-command-subst-in-quotes (limit)
    "Search for variables in double-quoted strings bounded by LIMIT."
    (with-syntax-table sh-mode-syntax-table
      (let (res)
        (while (and (setq res (re-search-forward "[^\\]\\(\\$(.+?)\\|`.+?`\\)"
                                                 limit t))
                    (not (eq (nth 3 (syntax-ppss)) ?\"))))
        res)))

  (setq sh-indent-after-continuation 'always)

  (add-hook! 'sh-mode-hook
    (defun sh-init-extra-fontification ()
      (font-lock-add-keywords nil
                              `((sh-script:match-variables-in-quotes
                                 (1 'font-lock-constant-face preprend)
                                 (2 'font-lock-variable-name-face prepend))
                                (sh-script:match-command-subst-in-quotes
                                 (1 'sh-quoted-exec prepend))
                                (,(regexp-opt sh-builtin-keywords 'symbols)
                                 (0 'font-lock-type-face append))))))

  (add-hook 'sh-mode-hook #'rainbow-delimiters-mode)
  
  (unless (bound-and-true-p lsp-mode)
    (setq-local completion-at-point-functions (list (cape-capf-buster #'cape:prog-capf)
                                                    #'cape-file))))

(use-package fish-mode
  :config
  (general-setq fish-ts-mode-hook fish-mode-hook)

  (def-mode-snippet fn fish-mode
    > "function " @ _ "-d " @ - \n
    > @ - \n
    >  "end" \n)
  
  (setq-local completion-at-point-functions (list (cape-capf-buster #'cape:prog-capf)
                                                  #'cape-file)))

;;;;; Sieve scripts (not really coding but whatever)
;;;;;
;;;; TODO can we enhance this somehow?

(use-package sieve-mode
  :ensure nil
  :mode "\\.s\\(v\\|iv\\|ieve\\)\\'")

;;;;; Emacs-Lisp
;;;;;

(use-package elisp-mode
  :ensure nil
  :mode ("\\.Cask\\'" . emacs-lisp-mode)
  :mode ("\\Eask\\’" . emacs-lisp-mode)
  :bind (:map emacs-lisp-mode-map
         ("C-c C-c" . eval-region))
  :preface
  (defun elisp-mode:indent-function (indent-point state)
    "A replacement for `lisp-indent-function'.
Indents plists more sensibly. Adapted from
https://emacs.stackexchange.com/questions/10230/how-to-indent-keywords-aligned

Also took this from Doom Emacs"
    (let ((normal-indent (current-column))
          (orig-point (point))
          ;; TODO Refactor `target' usage (ew!)
          target)
      (goto-char (1+ (elt state 1)))
      (parse-partial-sexp (point) calculate-lisp-indent-last-sexp 0 t)
      (cond ((and (elt state 2)
                  (or (not (looking-at-p "\\sw\\|\\s_"))
                      (eq (char-after) ?:)))
             (unless (> (save-excursion (forward-line 1) (point))
                        calculate-lisp-indent-last-sexp)
               (goto-char calculate-lisp-indent-last-sexp)
               (beginning-of-line)
               (parse-partial-sexp (point) calculate-lisp-indent-last-sexp 0 t))
             (backward-prefix-chars)
             (current-column))
            ((and (save-excursion
                    (goto-char indent-point)
                    (skip-syntax-forward " ")
                    (not (eq (char-after) ?:)))
                  (save-excursion
                    (goto-char orig-point)
                    (and (eq (char-after) ?:)
                         (eq (char-before) ?\()
                         (setq target (current-column)))))
             (save-excursion
               (move-to-column target t)
               target))
            ((let* ((function (buffer-substring (point) (progn (forward-sexp 1) (point))))
                    (method (or (function-get (intern-soft function) 'lisp-indent-function)
                                (get (intern-soft function) 'lisp-indent-hook))))
               (cond ((or (eq method 'defun)
                          (and (null method)
                               (> (length function) 3)
                               (string-match-p "\\`def" function)))
                      (lisp-indent-defform state indent-point))
                     ((integerp method)
                      (lisp-indent-specform method state indent-point normal-indent))
                     (method
                      (funcall method indent-point state))))))))

  :custom
  (tab-width 8)
  (debugger-bury-or-kill 'kill)
  (mode-name "Elisp")
  (lisp-indent-function #'elisp-mode:indent-function)
  :config
  (setq-mode-local emacs-lisp-mode
                   tab-width 8
                   outline-regexp (rx
                                   (|
                                    (group bol ";;; " alnum)
                                    (group bol ";;;; " alnum)
                                    (group bol ";;;;; " alnum)
                                    (group bol ";;;;;; " alnum)
                                    (group bol ";;;;;;; " alnum)
                                    (group bol ";;;;;;;; " alnum))))
  (put 'add-function 'lisp-indent-function 2)
  (put 'advice-add   'lisp-indent-function 2)
  (put 'plist-put    'lisp-indent-function 2)

  (push '(elisp-mode
          (elisp-flymake-byte-compile :disabled t)
          (elisp-flymake-checkdoc :disabled t) ; checkdoc is annoying.
          (flymake-collection-codespell :predicate
                                        (lambda () (executable-find "codespell"))))
        flymake-collection-hook-config)

  (setf (alist-get 'emacs-lisp-mode flymake-collection-hook-config)
        '((elisp-flymake-byte-compile :disabled t)
          (elisp-flymake-checkdoc :disabled t) ; shut up checkdoc
          (flymake-collection-codespell :predicate
                                        (lambda () (executable-find "codespell")))))

  (setq-mode-local emacs-lisp-mode
                   completion-at-point-functions (list
                                                  (cape-capf-buster #'cape:elisp-capf)
                                                  #'cape-file))

  (def-mode-snippet fun emacs-lisp-mode
    > "(defun " @ - " (" @ _ ")" \n
    > "\"" @ _ "\"" \n
    > @ _ ")")

  (def-mode-snippet fn emacs-lisp-mode
    > "(lambda (" @ _ ") ( " @ - " )")

  (def-mode-snippet macro emacs-lisp-mode
    > "(defmacro " @ - "(" @ _  ")" \n
    > "\"" @ _ "\"" \n
    > @ _ ")")

  (def-mode-snippet var emacs-lisp-mode
    > "(defvar " @ - \n
    > "\"" @ _ "\")")

  (def-mode-snippet lvar emacs-lisp-mode
    > "(defvar-local " @ - \n
    > "\"" @ _ "\")")

  (def-mode-snippet const emacs-lisp-mode
    > "(defconst " @ - \n
    > "\"" @ _ "\")")

  (def-mode-snippet defcustom emacs-lisp-mode
    > "(defcustom " @ - \n
    > ":type" @ _ \n
    > "\"" @ _ "\")")

  (def-mode-snippet autoload emacs-lisp-mode
    ";;;###autoload")

  ;; TODO figure out how to properly expand the "star" functions/macros in abbrevs
  (def-mode-snippet iflett emacs-lisp-mode
    > "(if-let* (" @ - ")" @ _ ")")

  (def-mode-snippet whenlett emacs-lisp-mode
    > "(when-let* (" @ - ")" @ _ ")")

  (def-mode-snippet lett emacs-lisp-mode
    > "(let* (" @ - ")" @ _ ")")

  (def-mode-snippet pt emacs-lisp-mode
    > "(point)")

  (def-mode-snippet setqml emacs-lisp-mode
    > "(setq-mode-local " @ - \n
    >  @ _ ")")

  (def-mode-snippet msnip emacs-lisp-mode
    > "(def-mode-snippet " @ - \n
    > @ _ ")")

  (defadvice! elisp-mode:append-val-to-eldoc (orig-fn sym)
    "Display variable value next to documentation in eldoc."
    :around #'elisp-get-var-docstring
    (when-let* ((ret (funcall orig-fn sym)))
      (if (boundp sym)
          (concat ret " "
                  (let* ((truncated " [...]")
                         (print-escape-newlines t)
                         (str (symbol-value sym))
                         (str (prin1-to-string str))
                         (limit (- (frame-width) (length ret) (length truncated) 1)))
                    (format (format "%%0.%ds%%s" (max limit 0))
                            (propertize str 'face 'warning)
                            (if (< (length str) limit) "" truncated)))))))

  (add-hook! 'emacs-lisp-mode-hook
             #'outline-minor-mode
             #'rainbow-delimiters-mode
             (lambda ()
               "Disable the checkdoc checker."
               (setq-local flycheck-disabled-checkers
                           '(emacs-lisp-checkdoc))))
  (remove-hook 'flymake-diagnostic-functions #'elisp-flymake-checkdoc)
  (add-hook 'help-mode-hook 'cursor-sensor-mode))

(use-package ielm
  :ensure nil
  :commands ielm ielm-send-input inferior-emacs-lisp-mode
  :hook (ielm-indirect-setup-hook . rainbow-delimiters-mode)
  :config
  (setq-local scroll-margin 0)
  ;; Adapted from http://www.modernemacs.com/post/comint-highlighting/ to add
  ;; syntax highlighting to ielm REPLs.
  (setq ielm-font-lock-keywords
        (append '(("\\(^\\*\\*\\*[^*]+\\*\\*\\*\\)\\(.*$\\)"
                   (1 font-lock-comment-face)
                   (2 font-lock-constant-face)))
                (when (require 'highlight-numbers nil t)
                  (highlight-numbers--get-regexp-for-mode 'emacs-lisp-mode))
                (cl-loop for (matcher . match-highlights)
                         in (append lisp-el-font-lock-keywords-2
                                    lisp-cl-font-lock-keywords-2)
                         collect
                         `((lambda (limit)
                             (when ,(if (symbolp matcher)
                                        `(,matcher limit)
                                      `(re-search-forward ,matcher limit t))
                               ;; Only highlight matches after the prompt
                               (> (match-beginning 0) (car comint-last-prompt))
                               ;; Make sure we're not in a comment or string
                               (let ((state (syntax-ppss)))
                                 (not (or (nth 3 state)
                                          (nth 4 state))))))
                           ,@match-highlights)))))

(use-package overseer
  :config
  (remove-hook 'emacs-lisp-mode-hook #'overseer-enable-mode))

(use-package elisp-demos
  :demand t
  :init
  (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
  (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update))

(use-package buttercup
  :mode ("/test[/-].+\\.el$" . buttercup-minor-mode)
  :preface
  (defvar buttercup-minor-mode-map (make-sparse-keymap)))

(use-package highlight-quoted)

;;;;; Common Lisp
;;;;;

(defer-feature! lisp-mode)

(use-package sly
  :ensure (:wait t)
  :commands (sly-editing-mode sly-mrepl-mode)
  :mode "\\.lisp\\’$"
  :when (or (executable-find "sbcl")
            (executable-find "ros"))
  :hook ((sly-mrepl-mode . electric-pair-local-mode))
  :preface
  (defvar inferior-lisp-program (executable-find "sbcl"))

  (defalias 'cape:sly-base (cape-capf-super
                            #'cape-abbrev
                            #'cape-dabbrev
                            #'cape-keyword
                            #'sly-complete-symbol))

  (defalias 'cape:sly-file (cape-capf-super
                            #'sly-complete-filename-maybe
                            #'cape-file))
  :init
  (setq sly-contribs '(sly-fancy))

  (add-hook! 'after-init-hook
    (with-eval-after-load 'sly
      (sly-setup)))
  :custom
  (sly-mrepl-history-file-name (concat my-cache-dir "sly-mrepl-history"))
  (sly-net-coding-system 'utf-8-unix)
  (sly-kill-without-query-p t)
  (sly-lisp-implementations
   `((sbcl ("sbcl" "--dynamic-space-size" "2000"))
     (roswell (,(executable-find "ros") " -Q run"))))
  (sly-default-lisp 'sbcl)
  (sly-description-autofocus t)
  (sly-inhibit-pipelining nil)
  (sly-load-failed-fasl 'always)
  (sly-ignore-protocol-mismatches t)
  (sly-complete-symbol-function 'sly-flex-completions)
  :config
  (defun sly:cleanup-maybe ()
    "Kill processes and leftover buffers when killing the last sly buffer."
    (let ((buf-list (delq (current-buffer) (buffer-list))))
      (unless (cl-loop for buf in buf-list
                       if (and (buffer-local-value 'sly-mode buf)
                               (get-buffer-window buf))
                       return t)
        (dolist (conn (sly--purge-connections))
          (sly-quit-list-internal conn 'sly-quit-sentinel t))
        (let (kill-buffer-hook kill-buffer-query-functions)
          (mapc #'kill-buffer
                (cl-loop for buf in buf-list
                         if (buffer-local-value 'sly-mode buf)
                         collect buf))))))

  (defun sly:init ()
    "Attempt to auto-start sly when opening a lisp buffer."
    (cl-labels ((temp-buf-p (buf)
                            (equal (substring (buffer-name buf) 0 1) " ")))
      (cond ((or (temp-buf-p (current-buffer))
                 (sly-connected-p)))
            ((executable-find inferior-lisp-program)
             (let ((sly-auto-start 'always))
               (sly-auto-start)
               (add-hook 'kill-buffer-hook #'sly:cleanup-maybe nil t)))
            ((message "WARNING: Couldn't find `inferior-lisp-program' (%s)"
                      inferior-lisp-program)))))

  (eval-after-load 'emacs
    (remove-hook 'lisp-mode-hook #'sly--lisp-indent-lisp-mode-hook))
  (eval-after-load 'lisp
    (remove-hook 'lisp-mode-hook #' #'sly--lisp-indent-lisp-mode-hook))

  (if (fboundp 'sly-editing-mode)
      (add-hook 'lisp-mode-local-vars-hook #'sly-editing-mode))

  (add-hook 'lisp-mode-local-vars-hook #'sly-lisp-indent-compatibility-mode 'append)

  (add-hook! 'sly-mode-hook #'sly:init)

  ;; Cool stuff with Consult
  (defvar consult::sly-mrepl-hist-source
    `(:name "Sly History"
      :narrow ?<
      :face 'font-lock-keyword-face
      :history 'comint-input-ring
      :action (lambda (e)
                (insert e))
      :items ,#'sly-mrepl--read-input-ring))

  (defun consult:sly-mrepl-history ()
    "Select from `sly''s REPL histroy with `consult'."
    (interactive)
    (consult--read consult::sly-mrepl-hist-source
                   :prompt "Item: "
                   :history 'comint-input-ring
                   :require-match t
                   :sort nil
                   :keymap sly-mrepl-mode-map))

  (defvar consult::sly-mrepl-shortcut-source
    `(:name "Shortcut"
      :narrow ?s
      :action
      ,(lambda (string)
         (let ((command (and string
                             (cdr (assoc string sly-mrepl-shortcut-alist)))))
           (call-interactively command)))
      :items (mapcar #'car sly-mrepl-shortcut-alist)))

  (defun consult:sly-mrepl-shortcut ()
    "Select a shortcut candidate for `sly''s REPL with `consult'."
    (interactive)
    (consult--read consult::sly-mrepl-shortcut-source
                   :prompt "Action: "
                   :require-match t
                   :sort nil))

  ;; Completions
  (advice-add #'sly-complete-symbol :around
    (lambda (orig-fn)
      (cape-wrap-properties orig-fn :exclusive 'no)))

  (advice-add #'sly-complete-filename-maybe :around
    (lambda (orig-fn)
      (cape-wrap-properties orig-fn :exclusive 'no)))

  (setq-local completion-at-point-functions (list
                                             #'cape-abbrev
                                             (cape-capf-buster #'cape:sly-base)
                                             (cape-capf-buster #'cape:sly-file))))

(use-package sly-quicklisp
  :after sly
  :commands sly-quicklisp)

(use-package sly-repl-ansi-color
  :init
  (add-to-list 'sly-contribs 'sly-repl-ansi-color))

(use-package sly-asdf
  :init
  (add-to-list 'sly-contribs 'sly-asdf))

(use-package sly-named-readtables
  :init
  (add-to-list 'sly-contribs 'sly-named-readtables))

;;;;; Schemin’
;;;;;

;; (use-package scheme
;;   :ensure nil
;;   :hook (scheme-mode . rainbow-delimiters-mode)
;;   :preface
;;   (defvar calculate-lisp-indent-last-sexp)
;;   :config
;;   (defadvice! scheme:indent-function (indent-point state)
;;     "A better indenting function for `scheme-mode'."
;;     :override #'scheme-indent-function
;;     (let ((normal-indent (current-column)))
;;       (goto-char (1+ (elt state 1)))
;;       (parse-partial-sexp (point) calculate-lisp-indnet-last-sexp 0 t)
;;       (if (and (elt state 2)
;;                (not (looking-at-p "\\sw\\|\\s_")))
;;           (progn
;;             (unless (> (save-excursion (forward-line 1) (point))
;;                        calculate-lisp-indent-last-sexp)
;;               (goto-char calculate-lisp-indent-last-sexp)
;;               (beginning-of-line)
;;               (parse-partial-sexp (point) calculate-lisp-indent-last-sexp 0 t))
;;             (backward-prefix-chars)
;;             (current-column))
;;         (let* ((function (buffer-substring
;;                           (point)
;;                           (progn
;;                             (forward-sexp 1)
;;                             (point))))
;;                (method (or (get (intern-soft function) 'scheme-indent-function)
;;                            (get (intern-soft function) 'scheme-indent-hook))))
;;           (cond ((or (eq method 'defun)
;;                      (and (null method)
;;                           (> (length function) 3)
;;                           (string-match-p "\\`def" function)))
;;                  (lisp-indent-defform state indent-point))
;;                 ((and (null method)
;;                       (> (length function) 1)
;;                       (string-match-p "\\`:" function))
;;                  (let ((lisp-body-indent 1))
;;                    (lisp-indent-defform state indent-point)))
;;                 ((integerp method)
;;                  (lisp-indent-specform method state indent-point normal-indent))
;;                 (method
;;                  (funcall method state indent-point normal-indent))))))))

;; (use-package geiser
;;   :defer t
;;   :preface
;;   (defun geiser/open-repl ()
;;     "Open the Geiser REPL."
;;     (interactive)
;;     (call-interactively #'switch-to-geiser)
;;     (current-buffer))
;;   :init
;;   ;; To work with Guile + Geiser
;;   ;; We need these first in order to set our `geiser-activate-implementation' variable
;;   (use-package geiser-gauche  :after geiser)
;;   (use-package geiser-chez    :after geiser)
;;   (use-package geiser-chicken :after geiser)
;;   (use-package geiser-guile   :after geiser)
;;   :custom
;;   (geiser-chicken-binary (expand-file-name (executable-find "chicken-csi")))
;;   (geiser-guile-binary (expand-file-name (executable-find "guile3")))
;;   (geiser-active-implementations '(gauche guile chez chicken))
;;   (geiser-default-implementation 'gauche)
;;   (geiser-autodoc-identifier-format "%s => %s")
;;   (geiser-repl-current-project-function #'project-root))

;; (use-package macrostep-geiser
;;   :after (:or geiser-mode geiser-repl)
;;   :config
;;   (add-hook 'geiser-mode-hook      #'macrostep-geiser-setup)
;;   (add-hook 'geiser-repl-mode-hook #'macrostep-geiser-setup))

;; (use-package racket-mode
;;   :mode "\\.rkt\\'"
;;   :hook (racket-mode-local-vars . (racket-xp-mode lsp-deferred))
;;   :preface
;;   (defun racket:open-repl ()
;;     "Open the Racket REPL."
;;     (interactive)
;;     (pop-to-buffer
;;      (or (get-buffer "*Racket REPL*")
;;          (progn (racket-run-and-switch-to-repl)
;;                 (let ((buf (get-buffer "*Racket REPL*")))
;;                   (bury-buffer buf)
;;                   buf)))))
;;   :config
;;   (add-hook! 'racket-mode-hook
;;              #'rainbow-delimiters-mode
;;              #'highlight-quoted-mode)

;;   (add-hook! 'racket-xp-mode-hook
;;     (defun racket-xp-disable-flycheck ()
;;       (cl-pushnew 'racket flyheck-disabled-checkers)))

;;   (define-key 'racket-xp-mode-map [remap racket-doc]              #'racket-xp-documentation)
;;   (define-key 'racket-xp-mode-map [remap racket-visit-definition] #'racket-xp-visit-definition)
;;   (define-key 'racket-xp-mode-map [remap next-error]              #'racket-xp-next-error)
;;   (define-key 'racket-xp-mode-map [remap previous-error]          #'racket-xp-previous-error))

;;;;; SQL
;;;;;

;; Dream of using complex SQL queries you found on StackOverflow to solve all of
;; your problems, only to realize you’re stuck using some convoluted/mediocre ORM
(use-package sql
  :ensure nil)

;;;;; Java
;;;;;

;; Still waiting for the JVM to startup,
;; and for Project Valhalla to be production ready (*quiet sobbing*)

(use-package lsp-java
  :after lsp-mode
  :hook (java-mode . java-lsp)
  :custom
  (lsp-jt-root (concat lsp-java-server-install-dir "java-test/server/"))
  (dap-java-test-runner (concat lsp-java-server-install-dir "test-runner/junit-platform-console-standalone.jar"))
  ;; Stolen from: https://github.com/dakra/dmacs/blob/master/init.org#java
  ;; Use Google style formatting by default
  (lsp-java-format-settings-url
   "https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml")
  (lsp-java-format-settings-profile "GoogleStyle")
  (lsp-java-vmargs
   `("-XX:+UseParallelGC"
     "-XX:GCTimeRatio=4"
     "-Dsun.zip.disableMemoryMapping=true"
     "-noverify"
     "-Xmx1G"
     "-XX:+UseG1GC"
     "-XX:+UseStringDeduplication"))
  :config
  (general-setq java-ts-mode-hook java-mode-hook)
  ;; Also stolen from: https://github.com/dakra/dmacs/blob/master/init.org#java
  (defun java-lsp ()
    (setq electric-indent-inhibit nil)
    (lsp)))

;;;###autoload
(defun java:run-test ()
  "Runs test at point.
If in a method, runs the test method, otherwise runs the entire test class."
  (interactive)
  (require 'dap-java)
  (condition-case nil
      (dap-java-run-test-method)
    (user-error (dap-java-run-test-class))))

;;;###autoload
(defun java:debug-test ()
  "Runs test at point in a debugger.
If in a method, runs the test method, otherwise runs the entire test class."
  (interactive)
  (require 'dap-java)
  (condition-case nil
      (call-interactively #'dap-java-debug-test-method)
    (user-error (call-interactively #'dap-java-debug-test-class))))

(use-package groovy-mode
  :mode "\\.g\\(?:radle\\|roovy\\)$")

;;;;; .NET Core
;;;;;

;; (use-package csharp-mode
;;   :hook (csharp-mode . rainbow-delimiters-mode)
;;   :config
;;   (add-hook 'csharp-mode-local-vars-hook #'lsp-deferred))

;; (use-package fsharp-mode
;;   :hook (fsharp-mode . rainbow-delimiters-mode)
;;   :custom
;;   (fsharp-ac-intellisense-enabled nil)
;;   :config
;;   (add-hook 'fsharp-mode-local-vars-hook #'lsp-deferred))

;; TODO make some kinda REPL.
;; TODO doc comment snippet

;; (use-package powershell
;;   :if (or windows-nt-p
;;           cygwin-p
;;           (executable-find "powershell")
;;           (executable-find "pwsh"))
;;   :mode ("\\.ps[dm]?1\\'" . powershell-mode)
;;   :interpreter (("pwsh" . powershell-mode)
;;                 ("powershell" . powershell-mode))
;;   :config
;;   (add-hook 'powershell-mode-hook #'lsp-deferred)
;;   (if (package-installed-p 'dap-mode)
;;       (require 'dap-pwsh)))

;; (use-package sharper
;;   :when (executable-find "dotnet")
;;   :bind ("C-c n" . sharper-main-transient))

;; (use-package bmx-mode
;;   :when (or windows-nt-p cygwin-p)
;;   :config
;;   (bmx-mode-setup-defaults))

;;;;; Julia
;;;;;

;; `ob-julia' needs this variable to be defined
;;;###autoload (defvar inferior-julia-program-name (or (executable-find "julia-basic") "julia"))

(use-package julia-mode
  :ensure (:wait t))

(use-package julia-ts-mode
  :interpreter "julia"
  :mode "\\.jl\\'"
  :preface
  (defvar julia-ts-mode-hook nil
    "Mode hook for ‘julia-ts-mode’.")
  :init
  (general-setq julia-ts-mode-hook julia-mode-hook)
  :custom
  (julia-ts-align-argument-list-to-first-sibling t)
  (julia-ts-align-assignment-expressions-to-first-sibling t)
  (julia-ts-align-curly-brace-expressions-to-first-sibling t)
  :config
  ;; Borrow matlab.el's fontification of math operators. From
  ;; <https://web.archive.org/web/20170326183805/https://ogbe.net/emacsconfig.html>
  (font-lock-add-keywords
   'julia-ts-mode
   `((,(let ((OR "\\|"))
         (concat "\\("  ; stolen `matlab.el' operators first
                 ;; `:` defines a symbol in Julia and must not be highlighted
                 ;; as an operator. The only operators that start with `:` are
                 ;; `:<` and `::`. This must be defined before `<`.
                 "[:<]:" OR
                 "[<>]=?" OR
                 "\\.[/*^']" OR
                 "===" OR
                 "==" OR
                 "=>" OR
                 "\\<xor\\>" OR
                 "[-+*\\/^&|$]=?" OR  ; this has to come before next (updating operators)
                 "[-^&|*+\\/~]" OR
                 ;; Julia variables and names can have `!`. Thus, `!` must be
                 ;; highlighted as a single operator only in some
                 ;; circumstances. However, full support can only be
                 ;; implemented by a full parser. Thus, here, we will handle
                 ;; only the simple cases.
                 "[[:space:]]!=?=?" OR "^!=?=?" OR
                 ;; The other math operators that starts with `!`.
                 ;; more extra julia operators follow
                 "[%$]" OR
                 ;; bitwise operators
                 ">>>" OR ">>" OR "<<" OR
                 ">>>=" OR ">>" OR "<<" OR
                 "\\)"))
      1 font-lock-type-face)))

  (defconst julia:faster-startup-flags
    '("--startup-file=no"
      "--history-file=no"
      "--color=no")
    "Startup flags for Julia processes to speed things up.")

  (defvar julia:default-environment
    (string-trim
     (shell-command-to-string
      "julia --compile=min --optimize=0 --startup-file=no --history-file=no --color=no -e 'print(dirname(Base.active_project()))'"))
    "The current Julia environment/project")

  ;; TODO Figure out how to cheat and make formatting even faster with
  ;; --trace-compile to get a list of functions that are compiled by Julia, and
  ;; PackageCompiler.jl to create a sysimage for JuliaFormatter.

  (defconst julia:format-sysimage
    (expand-file-name "~/.local/bin/juliaformatter.so")
    "Path to sysimage for JuliaFormatter.")

  (defvar julia:format-command
    `(,(executable-find "julia")
      ,(concat "--project=" julia:default-environment)
      "--compile=min"
      "--optimize=0"
      ,@julia:faster-startup-flags
      ,(if (file-exists-p julia:format-sysimage)
           (concat "-J" julia:format-sysimage))
      ,(concat "-e \"using JuliaFormatter "
               (if (locate-dominating-file buffer-file-name ".JuliaFormatter.toml")
                   (concat "format_file(\'" (buffer-file-name) "\')\"")
                 (concat "format_file(\'" (buffer-file-name) "\', BlueStyle())\""))))
    "List of string for Julia’s JuliaFormatter package to pass to ‘apheleia’.")


  (push '(julia-formatter julia:format-command)
        apheleia-formatters))

;;;;;; Inferior Julia REPL
;;;;;;

(use-package julia-snail
  :requires vterm
  :after julia-ts-mode
  :hook (julia-ts-mode . julia-snail-mode)
  :custom
  (julia-snail-terminal-type :vterm))

;;;;;; For the LSP support for Julia
;;;;;;

;; Several configurations pull from here:
;; https://github.com/gdkrmr/lsp-julia/issues/49#issuecomment-1806846761

(use-package lsp-julia
  :after (julia-ts-mode lsp-mode)
  :hook (julia-ts-mode . lsp)
  :preface
  (defvar lsp-julia:main-env
    (expand-file-name
     (concat "~/.julia/environments/"
             (string-trim
              (shell-command-to-string
               "julia --version | cut -d' ' -f3"))
             "/")))

  (defconst lsp-julia:emacs-env
    (expand-file-name "~/.julia/environments/emacs/")
    "The julia environment for install LanguageServer.jl.")

  ;; TODO should scripts be
  ;; - compiled, or
  ;; - use --optimize

  (defconst lsp-julia:ls-install-script
    (expand-file-name "~/.local/bin/julia-ls-maybe-install.jl")
    "Path to custom script to install or update the Julia language server.")

  (defconst lsp-julia:ls-script
    (expand-file-name "~/.local/bin/julia-ls.jl")
    "Path to custom script to run the Julia language server.")

  (defun lsp-julia:get-root ()
    "Locate the root project file for a Julia project."
    (expand-file-name
     (or (locate-dominating-file buffer-file-name "Project.toml")
         (locate-dominating-file buffer-file-name "JuliaProject.toml")
         lsp-julia-default-environment)))

  (defun lsp-julia:maybe-install-language-server ()
    "Command to run to update or install the Julia language server."
    (interactive)
    (let ((flags `(,(concat "--project=" lsp-julia-package-dir) ,@julia:faster-startup-flags)))
      (apply #'start-process
             "lsp-julia:maybe-install-language-server"
             "*lsp-julia:maybe-install-language-server*"
             lsp-julia-command
             `(,@flags lsp-julia:ls-install-script))))

  (defun lsp-julia:rls-command ()
    "TODO"
    `(,lsp-julia-command
      ,@lsp-julia-flags
      ,lsp-julia:ls-script
      ,(buffer-file-name)))
  :init
  (if (not (file-directory-p lsp-julia:main-env))
      (make-directory lsp-julia:main-env t))
  (if (not (file-directory-p lsp-julia:emacs-env))
      (make-directory lsp-julia:emacs-env t))
  (general-setq lsp-julia-default-environment nil)
  :custom
  (lsp-julia-command (executable-find "julia"))
  (lsp-julia-package-dir "@emacs")
  (lsp-julia-flags `(,(concat "--project=" lsp-julia-package-dir)
                     ,@lsp-julia:faster-startup-flags
                     ,(concat "-J" lsp-julia:emacs-env "/languageserver.so")))
  (lsp-julia-default-environment julia:default-environment)
  :config
  (setq-mode-local julia-ts-mode
                   lsp-enable-folding t
                   lsp-folding-range-limit 100)

  (add-to-list 'lsp-language-id-configuration '(julia-mode . "julia"))
  (add-to-list 'lsp-language-id-configuration '(ess-julia-mode . "julia"))
  (add-to-list 'lsp-language-id-configuration '(julia-ts-mode . "julia"))
  
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection #'lsp-julia:rls-command)
                    :major-modes '(julia-mode ess-julia-mode julia-ts-mode)
                    :activation-fn (lsp-activate-on "julia")
                    :server-id 'julia-ls
                    :download-server-fn #'lsp-julia:maybe-install-language-server
                    :priority '1
                    :multi-root t)))

;;;; Lua
;;;;

(use-package lua-mode
  :hook (lua-mode . lsp)
  :preface
  (defun lua:open-repl ()
    "Open a Lua REPL"
    (interactive)
    (lua-start-process "lua" "lua")
    (pop-to-buffer lua-process-buffer))

  (defun lua:open-jit-repl ()
    "Open a LuaJIT Repl"
    (interactive)
    (lua-start-process "luajit" "luajit")
    (pop-to-buffer lua-process-buffer))
  :custom
  (lsp-clients-lua-language-server-install-dir (concat lsp-server-install-dir "lua-language-server"))
  ;;(lsp-clients-lua-language-server-bin (executable-find "lua-language-server"))
  (lsp-lua-hint enable t)
  (lsp-lua-hint-set-type t)
  (lsp-lua-diagnostics-workspace-delay 100)
  (lsp-lua-window-progress-bar nil)
  (lsp-lua-window-status-bar nil)
  (lua-indent-level 2)
  :config
  (general-setq lua-ts-mode-hook lua-mode-hook)
  
  (set-electric 'lua-mode :words '("else" "end"))
  
  (push
   '(lua-mode
     (flymake-collection-luacheck
      :predicate (lambda () (executable-find "luacheck"))))
   flymake-collection-hook-config))

;;;; Fennel
;;;;

(use-package fennel-mode
  :mode "\\.fnl\\'"
  :mode "\\.fenneldoc\\'"
  :hook
  (fennel-mode . rainbow-delimiters-mode)
  :custom
  (tab-width 2)
  (outline-regexp "[ \t]*;;;;* [^ \t\n]")
  :config
  (general-setq fennel-ts-mode-hook fennel-mode-hook)

  (require 'fennel-proto-repl)
  (add-hook 'fennel-mode-hook #'fennel-proto-repl-minor-mode))

;;;;; Prolog
;;;;;

;; (use-package prolog
;;   :ensure nil
;;   :when (executable-find "swipl")
;;   :commands (prolog-mode run-prolog)
;;   :hook (prolog-mode . lsp-deferred)
;;   :custom
;;   (prolog-system 'swi)
;;   :config
;;   (lsp-register-client
;;    (make-lsp-client
;;     :new-connection
;;     (lsp-stdio-connection (list "swipl"
;;                                 "-g" "use_module(library(lsp_server))."
;;                                 "-g" "lsp_server:main"
;;                                 "-t" "halt"
;;                                 "--" "stdio"))
;;     :major-modes '(prolog-mode)
;;     :priority 1
;;     :multi-root t
;;     :server-id 'prolog-ls)))

;; (use-package sweeprolog
;;   :when (executable-find "swipl")
;;   :hook (prolog-mode . sweeprolog-mode)
;;   :custom
;;   (sweeprolog-faces-style 'dark))

;;;;; Nim
;;;;;

;; (use-package nim-mode
;;   :hook (nim-mode . lsp-deferred)
;;   :init
;;   (add-hook! 'nim-mode-hook
;;     (defun nim:init-nimsuggest-mode ()
;;       "Conditionally load `nimsuggest-mode', instead of clumsily erroring out if
;; nimsuggest isn't installed."
;;       (unless (stringp nimsuggest-path)
;;         (setq nimsuggest-path (executable-find "nimsuggest")))
;;       (when (and nimsuggest-path (file-executable-p nimsuggest-path))
;;         (nimsuggest-mode))))

;;   (when windows-nt-p
;;     (advice-add #'nimsuggest--get-temp-file-name :filter-return
;;                 (defun nim--suggest-get-temp-file-name (path)
;;                   (replace-regexp-in-string "[꞉* |<>\"?*]" "" path))))
;;   ;; :config
;;   ;; ;; Make use of `dap-mode'
;;   ;; (require 'dap-gdb-lldb)
;;   ;; (dap-register-debug-tempalte "Nim::GDB Run Configuration"
;;   ;;                              (list :type "gdb"
;;   ;;                                    :request "launch"
;;   ;;                                    :name "GDB::Run")
;;   ;;                              :gdbpath "nim-gdb"
;;   ;;                              :target nil
;;   ;;                              :cwd nil)
;;   )

;; (use-package ob-nim
;;   :after ob)

;;;;; Erlang
;;;;;

;; (use-package erlang
;;   :mode ("\\.erlang\\'" . erlang-mode)
;;   :mode ("/rebar\\.config\\(?:\\.script\\)?\\'" . erlang-mode)
;;   :mode ("/\\(?:app\\|sys\\)\\.config\\'" . erlang-mode)
;;   :config
;;   (when (package-installed-p 'lsp-mode)
;;     (add-hook 'erlang-mode-local-vars-hook #'lsp-deferred)))

;;;;; Elixir
;;;;;

(use-package elixir-mode
  :mode "\\.elixir\\'"
  :mode "\\.ex\\'"
  :mode "\\.exs\\'"
  :mode "mix\\.lock"
  :interpreter "elixir"
  :hook (elixir-mode . lsp)
  :init
  ;; Make sure the elixir-ls script is in our path
  (add-to-list 'exec-path (expand-file-name "~/Projects/elixir-ls/release/") t)
  :custom
  (lsp-elixir-mix-env "dev")
  (lsp-elixir-project-dir "")
  :config
  (general-setq elixir-ts-mode-hook elixir-mode-hook)

  (require 'elixir-format)
  (when (bound-and-true-p lsp-mode)
    (require 'dap-elixir))

  (eval-after-load 'highlight-numbers
    (puthash 'elixir-mode
             "\\_<-?[[:digit:]]+\\(?:_[[:digit:]]\\{3\\}\\)*\\_>"
             highlight-numbers-modelist))

  (flymake-collection-define-enumerate flymake-collection-credo
    "Credo is a static code analysis tool for the Elixir language with a focus on teaching
and code consistency.

See https://github.com/rrrene/credo for the project README and
https://hexdocs.pm/credo/overview.html for the HexDocs."
    :title "credo"
    :pre-let ((credo
               (if (project-root (project-current))
                   (list (executable-find "mix") "credo")
                 (list (executable-find "credo")))))
    :pre-check (unless (car credo)
                 (error "Cannot find executable for credo"))
    :write-type 'pipe
    :command
    `(,@credo
      "list"
      ,@(when-let* ((file (buffer-file-name flymake-collection-source)))
          file)
      "--format json"
      "--read-from-stdin")
    :generator
    (alist-get
     'issues
     (caar
      (flymake-collection-parse-json
       (buffer-substring-no-properties
        (point-min) (point-max)))))
    :enumerate-parser
    (let-alist it
      (let* ((base-loc (flymake-diag-region flymake-collection-source .line_no .column))
             (loc (cons (car base-loc)
                        (cdr
                         (if (and .endLine .endColumn)
                             (flymake-diag-region flymake-collection-source
                                                  .endLine (1- .endColumn))
                           base-loc)))))
        (list
         flymake-collection-source
         (car loc)
         (cdr loc)
         :warning
         (concat .message " [Priority: " .priority "]")))))

  (def-mode-snippet module elixir-mode
    "defmodule " @ _ " do" \n
    > @ - \n
    "end" \n)

  (def-mode-snippet def elixir-mode
    > "def " @ _ " (" @ - ") do" \n
    > @ - \n
    > "end" \n)

  (def-mode-snippet if elixir-mode
    > "if " @ _ "do" \n
    > @ - \n
    > "end" \n))

(use-package mix
  :after elixir-mode
  :hook (elixir-mode . mix-minor-mode))

(use-package exunit
  :hook (elixir-mode . exunit-mode))

(use-package inf-elixir
  :general
  (:keymaps 'elixir-mode-map
   :prefix "C-c i"
   "i" #'inf-elixir
   "p" #'inf-elixir-project
   "l" #'inf-elixir-send-line
   "r" #'inf-elixir-send-region
   "b" #'inf-elixir-send-buffer))

(use-package ob-elixir
  :after ob)

;;;;; Perl
;;;;;

;; (use-package cperl-mode
;;   :mode ("\\.\\([pP][Llm]\\|al\\)\\'" . cperl-mode)
;;   :interpreter (("perl" .     cperl-mode)
;;                 ("perl5" .    cperl-mode)
;;                 ("miniperl" . cperl-mode))
;;   :hook (cperl-mode . (lambda ()
;;                          (set (make-local-variable 'eldoc-documentation-function)
;;                               'cperl:eldoc-doc-function)))
;;   :custom
;;   (cperl-invalid-face nil)
;;   (cperl-font-lock t)
;;   (cperl-electric-keywords t)
;;   (cperl-electric-parens t)
;;   (cperl-info-on-command-no-prompt t)
;;   (cperl-clobber-lisp-bindings t)
;;   (cperl-lazy-help-time t)
;;   :config
;;   (defun cperl:eldoc-doc-function ()
;;     "Return meaningful doc string for `eldoc-mode'."
;;     (car
;;      (let ((cperl-message-on-help-error nil))
;;        (cperl-get-help))))

;;   (add-hook 'cperl-mode-local-vars-hook #'lsp-deferred))

;;;;; Ruby
;;;;;

;; (use-package enh-ruby-mode
;;   :mode "\\(?:\\.rb\\|ru\\|rake\\|thor\\|jbuilder\\|gemspec\\|podspec\\|/\\(?:Gem\\|Rake\\|Cap\\|Thor\\|Vagrant\\|Guard\\|Pod\\)file\\)\\'"
;;   :mode "\\.\\(?:a?rb\\|aslsx\\)\\'"
;;   :mode "/\\(?:Brew\\|Fast\\)file\\'"
;;   :interpreter "j?ruby\\(?:[0-9.]+\\)"
;;   :config
;;   (add-hook 'enh-ruby-mode-local-vars-hook #'lsp-deferred)
;;   (setq-mode-local enh-ruby-mode sp-max-pair-length 6))

;; (use-package inf-ruby
;;   :after enh-ruby-mode
;;   :config
;;   (add-hook 'compilation-filter-hook #'inf-ruby-auto-enter))

;; (use-package yard-mode
;;   :after enh-ruby-mode)

;; (use-package rake
;;   :after enh-ruby-mode
;;   :commands (rake rake-rerun rake-regenerate-cache rake-find-task)
;;   :custom
;;   (rake-cache-file (concat my-cache-dir "rake.cache"))
;;   (rake-completion-system 'default))

;; (use-package ruby-test-mode
;;   :after enh-ruby-mode)

;;;;; Python
;;;;;

(use-package python
  :ensure nil
  :mode ("[./]flake8\\'" . conf-mode)
  :mode ("/Pipfile\\'" . conf-mode)
  :hook (python-mode-local-vars . flymake-mode)
  :custom
  (python-indent-guess-indent-offset-verbose nil)
  (python-indent-trigger-commands (list #'indent-for-tab-command #'expand-abbrev))
  (lsp-clients-python-library-directories '("/usr/local/" "/usr/"))
  (lsp-clients-pylsp-library-directories '("/usr/local/" "/usr/"))
  (lsp-clients-pylsp-plugins-black-enabled nil)
  (lsp-clients-pylsp-plugins-jedi-completion-fuzzy t)
  (lsp-pylsp-plugins-rope-completion-enabled t)
  (lsp-ruff-lsp-python-path (executable-find "python3"))
  :config
  (general-setq python-ts-mode-hook python-mode-hook)
  (setq python-indent-guess-indent-offset-verbose nil)
  ;; Default to Python 3. Prefer the versioned Python binaries since some
  ;; systems stupidly make the unversioned one point at Python 2.
  (when (and (executable-find "python3")
             (string= python-shell-interpreter "python"))
    (setq python-shell-interpreter "python3"))

  (define-key python-mode-map (kbd "DEL") nil)

  ;; We have to do a lot to make sure our Python code doesn’t suck.

  (setf (alist-get '(pythom-mode python-ts-mode) flymake-collection-hook-config nil nil #'equal)
        '((flymake-collection-mypy
           :predicate (lambda () (executable-find "mypy")))
          (flymake-collection-bandit
           :predicate (lambda () (executable-find "bandit")))
          (flymake-collection-pycodestyle :disabled t)
          (flymake-collection-pylint :disabled t)
          (flymake-collection-flake8 :disabled t)
          (flymake-collection-pyre :disabled t)
          (flymake-collection-ruff :disabled t))) ;; lsp-mode with ruff-lsp takes care of this

  (push '(blue . ("blue"
                  (when (apheleia-formatters-extension-p "pyi") "--pyi")
                  (apheleia-formatters-fill-column "--line-length")
                  "-"))
        apheleia-formatters)

  (setf (alist-get 'python-mode apheleia-mode-alist)
        '(blue ruff ruff-isort))

  (setf (alist-get 'python-ts-mode apheleia-mode-alist)
        '(blue ruff ruff-isort)))

(use-package lsp-pyright
  :after lsp-mode
  :hook (python-mode-local-vars . (lambda ()
                                    (require 'lsp-pyright)
                                    (lsp)))
  :custom
  (lsp-pyright-langserver-command (executable-find "basedpyright")))

(use-package poetry
  :after python
  :init
  (setq poetry-tracking-strategy 'switch-buffer)
  (add-hook 'python-mode-local-vars-hook #'poetry-tracking-mode))

(use-package python-pytest
  :after python
  :commands python-pytest-dispatch)

(use-package live-py-mode
  :after python
  :commands live-py-mode)

;;;;; lol webdev (*more quiet sobbing*)
;;;;;

(use-package sgml-mode
  :ensure nil
  :hook (html-mode . (sgml-electric-tag-pair-mode
                      sgml-name-8bit-mode))
  :custom
  (sgml-basic-offset 2)
  :config
  (general-setq html-ts-mode-hook html-mode-hook)

  (setf (alist-get 'html-mode apheleia-mode-alist)
        '(eslint-fix))

  (setf (alist-get 'html-ts-mode apheleia-mode-alist)
        '(eslint-fix)))

(use-package mhtml-mode
  :ensure nil
  :hook (html-mode . mhtml-mode))

(use-package css-mode
  :ensure nil
  :hook (css-mode . lsp)
  :config
  (general-setq css-ts-mode-hook css-mode-hook)
  
  (setq-mode-local css-mode
                   tab-width 4)

  (setf (alist-get 'css-mode apheleia-mode-alist)
        '(eslint-fix))

  (setf (alist-get 'css-ts-mode apheleia-mode-alist)
        '(eslint-fix))

  (setf (alist-get 'scss-mode apheleia-mode-alist)
        '(eslint-fix)))

(use-package nxml-mode
  :ensure nil
  :mode "\\.p\\(?:list\\|om\\)\\'" ; plist, pom
  :mode "\\.xs\\(?:d\\|lt\\)\\'"   ; xslt, xsd
  :mode "\\.xaml\\'"
  :mode "\\.rss\\'"
  :magic "<\\?xml"
  :custom
  (nxml-slash-auto-complete-flag t)
  (nxml-auto-insert-xml-declaration-flag t)
  (lsp-xml-jar-file (concat lsp-server-install-dir "org.eclipse.lsp4xml-0.3.0-uber.jar")))

(use-package js
  :ensure nil
  :interpreter "node"
  :hook (js-mode . rainbow-delimiters-mode)
  :hook (js-mode . lsp)
  :init
  ;; Parse node stack traces in the compilation buffer
  (with-eval-after-load 'compile
    (add-to-list 'compilation-error-regexp-alist 'node)
    (add-to-list 'compilation-error-regexp-alist-alist
                 '(node "^[[:blank:]]*at \\(.*(\\|\\)\\(.+?\\):\\([[:digit:]]+\\):\\([[:digit:]]+\\)"
                        2 3 4)))
  :custom
  (js-indent-level 2)
  (js-switch-indent-offset 2)
  (js-chain-indent t)
  :config
  (general-setq js-ts-mode-hook js-mode-hook)

  (push '(eslint-fix . ((if (not (null (file-name-directory "package.json")))
                            "npx eslint --fix"
                          "eslint --fix")
                        (buffer-file-name)))
        apheleia-formatters)

  (setf (alist-get 'js-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'js-jsx-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'js-ts-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'js-json-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'json-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'json-ts-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'js3-mode apheleia-formatters)
        '(eslint-fix)))

(use-package typescript-mode
  :hook (typescript-mode . rainbow-delimiters-mode)
  :hook (typescript-mode . lsp)
  :custom
  (typescript-indent-level 2)
  :config
  (general-setq typescript-ts-mode-hook typescript-mode-hook)

  (setq-mode-local typescript-mode
                   comment-line-break-function #'c-indent-new-comment-line)

  (setf (alist-get 'typescript-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'typescript-ts-mode apheleia-formatters)
        '(eslint-fix))

  (setf (alist-get 'tsx-ts-mode apheleia-mode-alist)
        '(eslint-fix)))

(use-package web-mode
  :mode "\\.[px]?html?\\'"
  :mode "\\.\\(?:tpl\\|blade\\)\\(?:\\.php\\)?\\'"
  :mode "\\.erb\\'"
  :mode "\\.l?eex\\'"
  :mode "\\.jsp\\'"
  :mode "\\.as[cp]x\\'"
  :mode "\\.hbs\\'"
  :mode "\\.mustache\\'"
  :mode ("\\.svelte\\'" . web-svelte-mode)
  :mode "\\.twig\\'"
  :mode "\\.jinja2?\\'"
  :mode "\\.eco\\'"
  :mode "wp-content/themes/.+/.+\\.php\\'"
  :mode "templates/.+\\.php\\'"
  :mode ("\\.vue\\'" . web-vue-mode)
  :hook ((web-mode-local-vars . lsp)
         (web-svelte-mode . lsp)
         (web-vue-mode . lsp))
  :preface
  (defvar web-svelte-mode-hook nil)

  (setf (alist-get 'web-mode apheleia-mode-alist)
        '(eslint-fix))

  (define-derived-mode web-svelte-mode web-mode "Svelte"
    "A mode derived from ‘web-mode’ specifically for working with svelte files."

    (setq mode-name "Svelte")
    (setq major-mode 'web-svelte-mode)

    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2)

    (add-to-list web-mode-extra-auto-pairs
                 '("svelte" (("{#if}" "{/if}"))))

    (setf (alist-get 'web-svelte-mode apheleia-mode-alist)
          '(eslint-fix))

    (when (treesit-ready-p 'svelte)
      (treesit-major-mode-setup))

    (define-abbrev-table web-svelte-mode-abbrev-table '()
      "An abbrev table for ‘web-svelte-mode’."
      :system t
      :case-fixed t)

    (def-mode-snippet web-svelte-mode svif
      > "{#if " @ _ "}" \n
      > @ - \n
      > "{/if}" \n)

    (def-mode-snippet web-svelte-mode svelse
      > "{#if " @ _ "}" \n
      > @ - \n
      > "{:else}" \n
      > @ - \n
      > "{/if}" \n)

    (def-mode-snippet web-svelte-mode svelse-if
      > "{#if " @ _ "}" \n
      > @ - \n
      > "{:else if" @ - "}" \n
      > @ - \n
      > "{:else}" \n
      > @ - \n
      > "{/if}" \n)

    (run-hooks 'web-svelte-mode-hook))

  (defvar web-vue-mode-hook nil)

  (define-derived-mode web-vue-mode web-mode "Vue"
    "A mode derived from ‘web-mode’ specifically for working with vue files."

    (setq mode-name "Vue")
    (setq major-mode 'web-vue-mode)

    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2)

    (setf (alist-get 'web-vue-mode apheleia-mode-alist)
          '(eslint-fix))
    
    (when (treesit-ready-p 'vue)
      (treesit-major-mode-setup))

    (define-abbrev-table web-vue-mode-abbrev-table '()
      "An abbrev table for ‘web-vue-mode’."
      :system t
      :case-fixed t)

    (run-hooks 'web-vue-mode-hook))
  :custom
  (web-mode-enable-engine-detection t)
  (web-mode-engines-alist '(("svelte" . "\\.svelte’\\")
                            ("vue" . "\\.vue’\\")))
  (web-mode-enable-html-entities-fontification t)
  (web-mode-enable-css-colorization t)
  (web-mode-auto-close-style 1)
  (web-mode-enable-auto-quoting nil)
  (web-mode-enable-auto-pairing t)
  (lsp-intelephense-storage-path (concat my-cache-dir "lsp-intelephense/"))
  ;;:config
  ;; You can use this if you use smartparens.
  ;; I’m not sure if it would be needed when using Puni.
  ;;
  ;; 1. Remove web-mode auto pairs whose end pair starts with a latter
  ;;    (truncated autopairs like <?p and hp ?>). Smartparens handles these
  ;;    better.
  ;; 2. Strips out extra closing pairs to prevent redundant characters
  ;;    inserted by smartparens.
  ;; (dolist (alist web-mode-engines-auto-pairs)
  ;;   (setcdr alist
  ;;           (cl-loop for pair in (cdr alist)
  ;;                    unless (string-match-p "^[a-z-]" (cdr pair))
  ;;                    collect (cons (car pair)
  ;;                                  (string-trim-right (cdr pair)
  ;;                                                     "\\(?:>\\|]\\|}\\)+\\'")))))
  )

(use-package lsp-tailwindcss
  :after (lsp-mode web-mode)
  :init
  (setq lsp-tailwindcss-add-on-mode t))

(use-package emmet-mode
  :hook (css-mode web-mode html-mode haml-mode heex-ts-mode nxml-mode)
  :config
  (when (package-installed-p 'yasnippet)
    (add-hook 'emmet-mode-hook #'yas-minor-mode-on))
  (setq emmet-move-cursor-between-quotes t))

;;;;; Containers all the way down
;;;;;

(use-package dockerfile-mode
  :config
  (add-hook 'dockerfile-mode-local-vars-hook #'lsp))

(use-package docker)

(provide 'programming)
;;; programming.el ends here
