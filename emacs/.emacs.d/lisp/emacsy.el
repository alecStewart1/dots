;;; emacsy.el --- Some things that are very Emacs-y -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'lib)

;;; Builtin Emacs packages
;;;

;;;; Dired
;;;;

(use-package dired
  :ensure nil
  :commands dired-jump
  :init
  (general-setq image-dired-dir                    (concat my-cache-dir "image-dired/")
                image-dired-db-file                (concat image-dired-dir "db.el")
                image-dired-gallery-dir            (concat image-dired-dir "gallery/")
                image-dired-temp-image-file        (concat image-dired-dir "temp-image")
                image-dired-temp-rotate-image-file (concat image-dired-dir "temp-rotate-image")
                image-dired-thumb-size             150)
  :custom
  ;; don't prompt to revert; just do it
  (dired-auto-revert-buffer t)

  ;; don't pass --dired to ls
  (dired-use-ls-dired nil)

  ;; suggest a target for moving/copying intelligently
  (dired-dwim-target t)
  (dired-hide-details-hide-symlink-targets nil)
  (dired-garbage-files-regexp "\\.idx\\|\\.run\\.xml$\\|\\.bbl$\\|\\.bcf$\\|.blg$\\|-blx.bib$\\|.nav$\\|.snm$\\|.out$\\|.synctex.gz$\\|\\(?:\\.\\(?:aux\\|bak\\|dvi\\|log\\|orig\\|rej\\|toc\\|pyg\\)\\)\\'")

  ;; Always copy/delete recursively
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'top)

  ;; Ask whether destination dirs should get created when copying/removing files.
  (dired-create-destination-dirs 'ask)

  ;; Let’s not have loads of dired buffers open.
  (dired-kill-when-opening-new-dired-buffer t)
  :config
  (put 'dired-find-alternate-file 'disabled nil)

  (define-key dired-mode-map (kbd "C-c C-e") #'wdired-change-to-wdired-mode))

(use-package image-dired
  :ensure nil
  :commands (image-dired image-dired-display-thumb image-dired-display-thumbs image-dired-minor-mode))

(use-package dired-aux
  :ensure nil
  :defer t
  :config
  (setq dired-create-destination-dirs 'ask
        dired-vc-rename-file t))

(use-package dired-x
  :ensure nil
  :hook (dired-mode . dired-omit-mode)
  :config
  (setq dired-omit-verbose nil
        dired-omit-files
        (concat dired-omit-files
                "\\`[.]?#\\|\\`[.][.]?\\'\\|^.DS_Store\\'\\|^.project\\(?:ile\\)?\\'\\|^.\\(svn\\|git\\)\\'\\|^.ccls-cache\\'\\|\\(?:\\.js\\)?\\.meta\\'\\|\\.\\(?:elc\\|o\\|pyo\\|swp\\|class\\)\\'")
        dired-clean-confirm-killing-deleted-buffers nil)
  (let ((cmd (cond (macos-p "open")
                   (linux-p "xdg-open")
                   (windows-nt-p "start")
                   (t ""))))
    (setq dired-guess-shell-alist-user
          `(("\\.pdf\\'" ,cmd)
            ("\\.docx\\'" ,cmd)
            ("\\.\\(?:djvu\\|eps\\)\\'" ,cmd)
            ("\\.\\(?:jpg\\|jpeg\\|png\\|gif\\|xpm\\)\\'" ,cmd)
            ("\\.\\(?:xcf\\)\\'" ,cmd)
            ("\\.csv\\'" ,cmd)
            ("\\.tex\\'" ,cmd)
            ("\\.\\(?:mp4\\|mkv\\|avi\\|flv\\|rm\\|rmvb\\|ogv\\)\\(?:\\.part\\)?\\'" ,cmd)
            ("\\.\\(?:mp3\\|flac\\)\\'" ,cmd)
            ("\\.html?\\'" ,cmd)
            ("\\.md\\'" ,cmd)))))

(use-package fd-dired
  :when (executable-find "fd")
  :defer t
  :init
  (global-set-key [remap find-dired] #'fd-dired))

(use-package dired-git-info
  :after dired
  :bind (:map dired-mode-map
         (")" . dired-git-info-mode))
  :config
  (setq dgi-commit-message-format "%h %c %s"
        dgi-auto-hide-details-p nil)
  (with-eval-after-load 'wdired
    (defvar dired--git-info-p nil)
    (defadvice! dired:disable-git-info (&rest _)
      :before #'wdired-change-to-wdired-mode
      (setq dired--git-info-p (bound-and-true-p dired-git-info-mode))
      (when dired--git-info-p
        (dired-git-info-mode -1)))
    (defadvice! dired:reactivate-git-info (&rest _)
      :after '(wdired-exit
               wdired-abort-changes
               wdired-finish-edit)
      (when dired--git-info-p
        (dired-git-info-mode +1)))))

(use-package diredfl
  :hook (dired-mode . diredfl-mode))

;;;; IBuffer
;;;;

(use-package ibuffer
  :ensure nil
  :bind (("C-x C-b" . ibuffer)
         ([remap list-buffers] . ibuffer-list-buffers)
         :map ibuffer-mode-map
         ("q" . kill-current-buffer))
  ;; :custom-face
  ;; (ibuffer-filter-group-name-face '(:inherit (success bold)))
  :custom
  (ibuffer-show-empty-filter-groups nil)
  (ibuffer-old-time 12)
  (ibuffer-formats
   `((mark modified read-only locked
           ,@(if (package-installed-p 'all-the-icons)
                 `(;; Here you may adjust by replacing :right with :center
                   ;; or :left According to taste, if you want the icon
                   ;; further from the name
                   " " (icon 2 2 :left :elide)
                   ,(propertize " " 'display `(space :align-to 8)))
               '(" "))
           (name 18 18 :left :elide)
           " " (size 9 -1 :right)
           " " (mode 16 16 :left :elide)
           ,@(when (require 'ibuffer-vc nil t)
               '(" " (vc-status 12 :left)))
           " " filename-and-process)
     (mark " " (name 16 -1) " " filename)))
  :config
  (if (elpaca-installed-p 'all-the-icons)
      (define-ibuffer-column icon (:name "  ")
        (let ((icon (if (and (buffer-file-name)
                             (all-the-icons-auto-mode-match?))
                        (all-the-icons-icon-for-file (file-name-nondirectory (buffer-file-name)) :v-adjust -0.05)
                      (all-the-icons-icon-for-mode major-mode :v-adjust -0.05))))
          (if (symbolp icon)
              (setq icon (all-the-icons-faicon "file-o" :face 'all-the-icons-dsilver :height 0.8 :v-adjust 0.0))
            icon))))
  
  (define-ibuffer-column size
    (:name "Size"
     :inline t
     :header-mouse-map ibuffer-size-header-map)
    (file-size-human-readable (buffer-size)))

  ;; Embark Keymaps
  (with-eval-after-load 'embark
    (defvar-keymap embark:ibuffer-mark-map
      :doc "Marking buffers in IBuffer."
      "*" #'ibuffer-unmark-all
      "M" #'ibuffer-mark-by-mode
      "m" #'ibuffer-mark-modified-buffers
      "u" #'ibuffer-mark-unsaved-buffers
      "s" #'ibuffer-mark-special-buffers
      "r" #'ibuffer-mark-read-only-buffers
      "d" #'ibuffer-mark-dired-buffers)

    (defvar-keymap embark:ibuffer-action-map
      :doc "Doing actions in IBuffer."
      "A" #'ibuffer-do-view
      "H" #'ibuffer-do-view-other-frame
      "E" #'ibuffer-do-eval
      "W" #'ibuffer-do-view-and-eval
      "F" #'ibuffer-do-shell-command-file
      "X" #'ibuffer-do-shell-command-pipe
      "N" #'ibuffer-do-shell-command-pipe-replace
      "Q" #'ibuffer-do-query-replace-regexp
      "U" #'ibuffer-do-replace-regexp
      "V" #'ibuffer-do-revert)

    (defvar-keymap embark:ibuffer-sort-map
      :doc "Sorting buffers in IBuffer."
      "i" #'ibuffer-invert-sorting
      "a" #'ibuffer-do-sort-by-alphabetic
      "v" #'ibuffer-do-sort-by-recency
      "s" #'ibuffer-do-sort-by-size
      "f" #'ibuffer-do-sort-by-filename/process
      "m" #'ibuffer-do-sort-by-major-mode)

    ;; (defvar-keymap embark:ibuffer-filter-map
    ;;   :doc "Filtering buffers in IBuffer."
    ;;   "m" #'ibuffer-filter-by-used-mode
    ;;   "M" #'ibuffer-filter-by-derived-mode
    ;;   "n" #'ibuffer-filter-by-name
    ;;   "c" #'ibuffer-filter-by-content
    ;;   "e" #'ibuffer-filter-by-predicate
    ;;   "f" #'ibuffer-filter-by-filename
    ;;   ">" #'ibuffer-#'filter-by-size-gt
    ;;   "<" #'ibuffer-#'filter-by-size-lt
    ;;   "/" #'ibuffer-#'filter-disable)
    ))

;;;; Casual
;;;;

(use-package casual
  :general
  (:keymaps 'Info-mode-map
   "C-o" #'casual-info-tmenu)
  (:keymaps 'org-agenda-mode-map
   "C-o" #'casual-agenda-tmenu)
  (:keymaps 'calendar-mode-map
   "C-o" #'casual-calendar-tmenu)
  (:keymaps 'calc-mode-map
   "C-o" #'casual-calc-tmenu)
  (:keymaps 'reb-mode-map
   "C-o" #'casual-re-builder-tmenu)
  (:keymaps 'reb-lisp-mode-map
   "C-o" #'casual-re-builder-tmenu))

;;;; Electric
;;;;

(use-package electric
  :ensure nil
  :hook ((find-file . electric-quote-mode)
         (dired-initial-position . electric-quote-mode))
  :preface
  (defvar-local electric:indent-words '()
    "The list of electric words. Typing these will trigger reindentation of the
current line.")
  (defun set-electric (modes &rest plist)
    "From Doom Emacs.
Declare that WORDS (list of strings) or CHARS (lists of chars) should trigger
electric indentation.

Enables `electric-indent-local-mode' in MODES.

\(fn MODES &key WORDS CHARS)"
    (declare (indent defun))
    (dolist (mode (ensure-list modes))
      (let ((hook (intern (format "%s-hook" mode)))
            (fn   (intern (format "+electric--init-%s-h" mode))))
        (cond ((null (car-safe plist))
               (remove-hook hook fn)
               (unintern fn nil))
              ((fset
                fn (lambda ()
                     (when (eq major-mode mode)
                       (setq-local electric-indent-inhibit nil)
                       (cl-destructuring-bind (&key chars words) plist
                         (electric-indent-local-mode +1)
                         (if chars (setq-local electric-indent-chars chars))
                         (if words (setq +electric-indent-words words))))))
               (add-hook hook fn))))))
  :config
  (setq-default electric-indent-chars '(?\n ?\^?))

  (add-hook! 'electric-indent-functions
    (defun electric-indent-char-fn (_c)
      (when (and (eolp) electric:indent-words)
        (save-excursion
          (backward-word)
          (looking-at-p (concat "\\<" (regexp-opt electric:indent-words))))))))

;;; External packages
;;;

;;;; Undo-fu
;;;;

(use-package undo-fu
  :hook ((find-file . undo-fu-mode))
  :config
  (setq undo-limit        400000
        undo-strong-limit 3000000
        undo-outer-limit  48000000)

  (define-minor-mode undo-fu-mode
    "Enables `undo-fu' for the current session."
    :keymap (let ((map (make-sparse-keymap)))
              (define-key map [remap undo]    #'undo-fu-only-undo)
              (define-key map [remap redo]    #'undo-fu-only-redo)
              (define-key map (kbd "C-_")     #'undo-fu-only-undo)
              (define-key map (kbd "M-_")     #'undo-fu-only-redo)
              (define-key map (kbd "C-M-_")   #'undo-fu-only-redo-all)
              (define-key map (kbd "C-x r u") #'undo-fu-session-save)
              (define-key map (kbd "C-x r U") #'undo-fu-session-recover)
              map)
    :init-value nil
    :global t))

(use-package undo-fu-session
  :hook (undo-fu-mode . global-undo-fu-session-mode)
  :init
  (setq undo-fu-session-directory (concat my-cache-dir "undo-fu-session/"))
  :config
  (setq undo-fu-session-incompatible-files '("\\.gpg$" "/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'"))
  (when (executable-find "zstd")
    (defadvice! undo-fu-session:use-zstd (filename)
      "Have `undo-fu-session--make-file-name' use zstd's .zst file extension for compression on FILENAME."
      :filter-return #'undo-fu-session--make-file-name
      (if undo-fu-session-compression
          (concat (file-name-sans-extension filename) ".zst")
        filname))))

;;;; Activities
;;;;

(use-package activities
  :init
  (activities-mode)
  (setq edebug-inhibit-emacs-lisp-mode-bindings t)
  :bind
  (("C-x C-a C-n" . activities-new)
   ("C-x C-a C-d" . activities-define)
   ("C-x C-a C-a" . activities-resume)
   ("C-x C-a C-s" . activities-suspend)
   ("C-x C-a C-k" . activities-kill)
   ("C-x C-a RET" . activities-switch)
   ("C-x C-a b" . activities-switch-buffer)
   ("C-x C-a g" . activities-revert)
   ("C-x C-a l" . activities-list)))

;;;; xr - reverse of rx
;;;;

(use-package xr)

;;;; jack - HTML generator library
;;;;

(use-package jack)

(provide 'emacsy)
;;; emacsy.el ends here
