;;; version-control.el --- Version Control in Emacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'lib)

;;; VC

;; The vc package is a bit slow at times
(remove-hook 'find-file-hooks #'vc-refresh-state)

;;;; Git
;;;;

;;;; Magit
;;;;

(use-package magit
  :ensure (:wait t)
  :commands (magit magit-status magit-info magit-file-delete magit-revert magit-auto-revert-mode)
  :preface
  (defvar magit:stale-p nil)
  (defvar magit:fringe-size 14)

  (defun magit:revert-buffer (buffer)
    (with-current-buffer buffer
      (kill-local-variable 'magit:stale-p)
      (when buffer-file-name
        (if (buffer-modified-p (current-buffer))
            (when (bound-and-true-p vc-mode)
              (vc-refresh-state)
              (force-mode-line-update))
          (revert-buffer t t t)))))


  (defun magit:mark-stale-buffers ()
    "Revert all visible buffers and mark buried buffers as stale.
Stale buffers are reverted when they are switched to, assuming they haven't been
modified."
    (dolist (buffer (buffer-list))
      (when (buffer-live-p buffer)
        (if (get-buffer-window buffer)
            (magit:revert-buffer buffer)
          (with-current-buffer buffer
            (setq-local magit:stale-p t))))))

  (defun magit:revert-buffer-maybe ()
    "Update `vc' and `git-gutter' if out of date."
    (when magit:stale-p
      (magit:revert-buffer (current-buffer))))

  (defun magit:kill-buffer (buf)
    "Kill the buffer (BUF) and it’s associated process."
    (when (and (bufferp buf) (buffer-live-p buf))
      (let ((process (get-buffer-process buf)))
        (if (not (processp process))
            (kill-buffer buf)
          (with-current-buffer buf
            (if (process-live-p process)
                (run-with-timer 5 nil #'magit:kill-buffer buf)
              (kill-process process)
              (kill-buffer buf)))))))

  (defun magit:quit-all ()
    "Kill all magit buffers for the current repository."
    (interactive)
    (mapc #'magit:kill-buffer (magit-mode-get-buffers))
    (magit:mark-stale-buffers))

  (defun magit:quit (&optional kill-buffer)
    "Bury the current magit buffer.
If KILL-BUFFER, kill this buffer instead of burying it.
If the buried/killed magit buffer was the last magit buffer open for this repo,
kill all magit buffers for this repo."
    (interactive "P")
    (let ((topdir (magit-toplevel)))
      (funcall magit-bury-buffer-function kill-buffer)
      (or (cl-find-if (lambda (win)
                        (with-selected-window win
                          (and (derived-mode-p 'magit-mode)
                               (equal magit--default-directory topdir))))
                      (window-list))
          (magit:quit-all))))

  (defun magit:enlarge-fringe ()
    "Make fringe larger in magit."
    (and (display-graphic-p)
         (derived-mode-p 'magit-mode)
         magit:fringe-size
         (let ((left  (or (car-safe magit:fringe-size) magit:fringe-size))
               (right (or (car-safe magit:fringe-size) magit:fringe-size)))
           (set-window-fringes nil left right))))

  (defun magit:optimize-process-calls ()
    "TODO"
    (when-let (path (executable-find magit-git-executable t))
      (setq-local magit-git-executable path)))

  (defun magit:reveal-point-if-invisible ()
    "TODO"
    (if (derived-mode-p 'org-mode)
        (org-reveal '(4))
      (require 'reveal)
      (reveal-post-command)))

;;; Taken from:
;;; https://emacs.stackexchange.com/questions/20154/how-can-i-stage-all-changes-and-commit-them-without-displaying-commit-message-bu
  (defun magit:stage-all-and-commit (msg)
    "Stage all modified files with commit message MSG."
    (interactive "sCommit Message: ")
    (magit-stage-modified)
    (magit-commit (list "-m" msg)))
  :custom
  (magit-auto-revert-mode nil)
  (magit-diff-refine-hunk t)
  (magit-save-repository-buffers nil)
  (magit-revision-insert-related-refs nil)
  (magit-bury-buffer-function #'magit-mode-quit-window)
  :config
  (defadvice! magit:revert-repo-buffers-deferred (&rest _)
    :after '(magit-checkout magit-branch-and-checkout)
    (magit:mark-stale-buffers))

  (add-hook 'magit-process-mode-hook #'goto-address-mode)
  (add-hook 'magit-popup-mode-hook #'hide-mode-line-mode)

  ;; Add additional switches that seem common enough
  (transient-append-suffix 'magit-fetch "-p"
    '("-t" "Fetch all tags" ("-t" "--tags")))
  (transient-append-suffix 'magit-pull "-r"
    '("-a" "Autostash" "--autostash"))

  ;;(define-key transient-map [escape] #'transient-quit-one)

  (add-hook! 'magit-mode-hook
    (add-hook! 'window-configuration-change-hook :local
               #'magit:enlarge-fringe))

  (add-hook 'magit-status-mode-hook #'magit:optimize-process-calls)
  (add-hook 'magit-diff-visit-file-hook #'magit:reveal-point-if-invisible))

;;;; Github Review
;;;;

;;(use-package code-review
;;  :after magit
;; :config
;; (transient-append-suffix 'magit-merge "i"
;;   '("y" "Review pull request")
;;   )
;;  )

;;;; TODOs for Magit
;;;;

(use-package hl-todo
  :ensure (:wait t)
  :bind ((:map hl-todo-mode-map
          ([C-f3] . hl-todo-occur)
          ("C-x M-t p" . hl-todo-previous)
          ("C-x M-t n" . hl-todo-next)
          ("C-x M-t o" . hl-todo-occur)))
  :hook (elpaca-after-init . global-hl-todo-mode)
  :config
  (dolist (keyword '("BUG" "DEFECT" "ISSUE"))
    (cl-pushnew `(,keyword . ,(face-foreground 'error)) hl-todo-keyword-faces))
  (dolist (keyword '("WORKAROUND" "HACK" "TRICK"))
    (cl-pushnew `(,keyword . ,(face-foreground 'warning)) hl-todo-keyword-faces)))

(use-package magit-todos
  :after (magit)
  :config
  (setq magit-todos-keyword-suffix "\\(?:([^)]+)\\)?:?") ; make colon optional
  (define-key magit-todos-section-map "j" nil))

;;;; Gitflow for Magit
;;;;

(use-package magit-gitflow
  :hook (magit-mode . turn-on-magit-gitflow))

(provide 'version-control)
;;; version-control.el ends here
