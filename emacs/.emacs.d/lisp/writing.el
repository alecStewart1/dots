;;; writing.el --- Writing and taking notes in Emacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'lib)
(require 'snippets)
(require 'mode-local)
(require 'autoinsert)
(require 'general)

;;;; Spellin' Good
;;;;

(delq! 'ispell features)

(use-package ispell
  :ensure nil
  :custom
  (ispell-program-name (executable-find "aspell"))
  (ispell-extra-args '("--sug-mode=ultra" "--run-together"))
  (ispell-personal-dictionary (expand-file-name "~/.local/share/dict/words"))
  :config
  (pushnew! ispell-skip-region-alist
            '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:")
            '("#\\+BEGIN_SRC" . "#\\+END_SRC")
            '("#\\+BEGIN_EXAMPLE" . "#\\+END_EXAMPLE")))

(use-package flyspell
  :ensure nil
  :disabled
  :custom
  (flyspell-issue-welcome-flag nil)
  (flyspell-issue-message-flag nil)
  :config
  (provide 'ispell))

(use-package jinx
  :hook ((org-load markdown-mode) . jinx-mode)
  :general
  (:keymaps 'org-mode-map
   "M-$" #'jinx-correct))

;;;; TODO Abbrevs
;;;;

;;;; Org-Mode
;;;;

;; (defun org-gtd:capture-today ()
;;   "Open our GTD workspace and capture a task for today."
;;   (interactive)
;;   (tabspaces-switch-or-create-workspace "GTD")
;;   (org-capture nil "gD"))

;; (defun org-gtd:capture-tt- ()
;;     ""
;;   )

(emacs:leader-def
  "c" #'org-capture
  "a" #'org-agenda)

(use-package org
  :ensure org-contrib
  :demand t
  :preface
  (defvar org-directory nil)
  (defvar org-attach-id-dir nil)

  (setq org-publish-timestamp-directory (concat my-cache-dir "org-timestamps/")
        org-preview-latex-image-directory (concat my-cache-dir "org-latex/"))

  (defvar org-modules
    '(;; ol-w3m
      ;; ol-bbdb
      ;; ol-docview
      ;; ol-gnus
      ;; ol-info
      ;; ol-irc
      ;; ol-mhe
      ;; ol-rmail
      ;; ol-eww
      ol-bibtex
      ol-docview
      org-id
      org-ctags
      org-habit
      org-tempo
      ol-eshell
      org-annotate-file
      ol-bookmark
      org-checklist
      org-collector
      org-depend
      org-interactive-query
      org-learn
      ol-man
      org-panel))

  (defun org:setup-dirs ()
    (setq org-directory (file-truename "~/Documents/Org/"))
    (setq org-id-locations-file (expand-file-name ".orgids" org-directory)))

  (defun org:setup-agenda ()
    (setq org-agenda-files (list org-directory))
    (setq-default
     org-agenda-deadline-faces
     '((1.0001 . error)
       (1.0 . org-warning)
       (0.5 . org-upcoming-deadline)
       (0.0 . org-upcoming-distant-deadline))
     org-agenda-fontify-priorities t
     org-agenda-window-setup 'current-window
     org-agenda-time-in-grid t
     org-agend-show-current-time-in-grid t
     org-agenda-skip-unavailable-files t
     org-agenda-skip-scheduled-if-deadline-is-shown t
     org-agenda-skip-scheduled-if-done t
     org-agenda-skip-deadline-if-done t
     org-agenda-span 10
     org-agenda-start-on-weekday nil
     org-agenda-start-day "-3d"
     org-agenda-block-separator nil
     org-agenda-category-icon-alist nil
     org-agenda-files '("~/Documents/Org/GTD/today.org"
                        "~/Documents/Org/GTD/week.org"
                        "~/Documents/Org/GTD/tt.org"
                        "~/Documents/Org/GTD/habits.org"
                        "~/Documents/Org/cal.org")
     org-agenda-inhibit-startup t
     ;;org-agenda-include-diary t ; <- interesting but we can use org-mode
     org-agenda-sorting-strategy
     '((agenda habit-up deadline-up scheduled-up
               priority-down todo-state-up time-up timestamp-up category-keep)
       (todo priority-down category-keep)
       (tags timestamp-up priority-down category-keep)
       (search category-keep))))

  (defun org:setup-appearance ()
    (general-setq org-indirect-buffer-display 'current-window
                  org-eldoc-breadcrumb-separator " -> "
                  org-ellipsis "…"
                  org-enforce-todo-dependencies t
                  org-entities-user
                  '(("flat" "\\flat" nil "" "" "266D" "♭")
                    ("sharp" "\\sharp" nil "" "" "266F" "♯"))
                  org-pretty-entities t
                  org-pretty-entities-include-sub-superscripts t
                  org-fontify-done-headline t
                  org-fontify-quote-and-verse-blocks t
                  org-fontify-whole-heading-line t
                  org-highlight-latex-and-related '(native script entities)
                  org-hide-emphasis-markers t
                  org-footnote-auto-label t
                  org-hide-leading-stars t
                  org-image-actual-width nil
                  org-priority-faces
                  '((?A . error)
                    (?B . warning)
                    (?C . success))
                  org-imenu-depth 6
                  org-list-allow-alphabetical t
                  org-use-property-inheritance t
                  org-startup-indented t
                  org-startup-truncated nil
                  org-startup-folded nil
                  org-fold-catch-invisible-edits 'smart
                  org-tags-column 0
                  org-use-sub-superscripts '{}
                  org-log-into-drawer t
                  org-log-done 'time)

    (general-setq org-refile-targets
                  '((nil :maxlevel 3)
                    (org-agenda-files :maxlevel 3))
                  org-refile-use-outline-path 'file
                  org-outline-path-complete-in-steps nil)

    (advice-add 'org-refile :after 'org-save-all-org-buffers)

    (plist-put org-format-latex-options :scale 1.5)

    (with-no-warnings
      (custom-declare-face 'org--todo-active  '((t (:inherit (bold font-lock-constant-face org-todo)))) "")
      (custom-declare-face 'org--todo-project '((t (:inherit (bold font-lock-doc-face org-todo)))) "")
      (custom-declare-face 'org--todo-onhold  '((t (:inherit (bold warning org-todo)))) "")
      (custom-declare-face 'org--todo-dnf     '((t (:inherit (bold error org-todo)))) ""))

    (general-setq org-todo-keywords
                  '((sequence
                     "TODO(t)"
                     "PROJ(p)"
                     "STRT(s)"
                     "WAIT(w@/!)"
                     "HOLD(h!)"
                     "MAYBE(m@)"
                     "CHECK(c@/!)"
                     "TO-READ(r)"
                     "|"
                     "DONE(d!)"
                     "KILL(k@)"
                     "KICKED(n@/!)"
                     "READ(R)")
                    (sequence
                     "[ ](T)"
                     "[-](S)"
                     "[?](W@/!)"
                     "|"
                     "[X](D!)"
                     "[!](N@/!)"))
                  org-todo-keyword-faces
                  '(("KILL"    . org--todo-dnf)
                    ("STRT"    . org--todo-active)
                    ("[?]"     . org--todo-onhold)
                    ("[-]"     . org--todo-active)
                    ("[!]"     . org--todo-dnf)
                    ("WAIT"    . org--todo-onhold)
                    ("HOLD"    . org--todo-onhold)
                    ("MAYBE"   . org--todo-onhold)
                    ("CHECK"   . org--todo-onhold)
                    ("TO-READ" . org-todo)
                    ("READ"    . org-done)
                    ("PROJ"    . org--todo-project)))

    (defadvice! org-eldoc:display-line (&rest _)
      "TODO"
      :before-until #'org-eldoc-documentation-function
      (when-let (link (org-element-property :raw-link (org-element-context)))
        (format "Link: %s" link)))

    (add-to-list 'display-buffer-alist '("^\\*Org-Links*\\*$"
                                         (display-buffer-no-window)
                                         (allow-no-window . t)))

    ;; Font stuff
    (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
    (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch))

  (defun org:setup-babel ()
    (general-setq org-src-preserve-indentation t
                  org-src-fontify-natively t
                  org-src-tab-acts-natively t
                  org-src-window-setup 'other-window
                  org-edit-src-content-indentation 0
                  org-edit-src-persistent-message nil
                  org-confirm-babel-evaluate nil
                  org-babel-lisp-eval-fn #'sly-eval
                  org-link-elisp-confirm-function nil)

    ;; (eval-after-load 'ob
    ;;   (add-to-list 'org-babel-default-lob-header-args '(:sync)))

    (defadvice! org-babel:fix-window-excursions (fn &rest args)
      "Suppress changes to the window config anywhere `org-babel-do-in-edit-buffer' is used."
      :around #'org-indent-region
      :around #'org-indent-line
      (save-window-excursion (apply fn args)))

    (defadvice! org-babel:fix-newline-and-indent-in-src-blocks (&optional indent _arg _interactive)
      "Mimic `newline-and-indent' in src blocks w/ lang-appropriate indentation."
      :after #'org-return
      (when (and indent
                 org-src-tab-acts-natively
                 (org-in-src-block-p t))
        (save-window-excursion
          (org-babel-do-in-edit-buffer
           (call-interactively #'indent-for-tab-command)))))

    (add-hook 'org-babel-after-execute-hook #'org-redisplay-inline-images)

    (define-key org-src-mode-map (kbd "C-c C-c") #'org-edit-src-exit))

;;;;; Org-Capture
;;;;;
  
  (defun org:setup-capture ()
    (use-package org-capture
      :ensure nil
      :commands org-capture
      :preface
      (defvar org-capture:frame-parameters
        `((name . "Org Capture")
          (width . 70)
          (height . 25)
          (transient . t)
          ,@(when linux-p
              `((window-system . ,(if (boundp 'pgtk-initialized) 'pgtk 'x))
                (display . ,(or (getenv "WAYLAND_DISPLAY")
                                (getenv "DISPLAY")
                                ":0"))))
          ,(if macos-p '(menu-bar-lines . 1)))
        "Parameters to pass to ‘make-frame’ to spawn a frame for ‘org-capture’.")

      (defun org-capture:cleanup-frame ()
        "Closes the org-capture frame once done adding an entry."
        (when (and (org-capture:frame-p)
                   (not org-capture-is-refiling))
          (delete-frame nil t)))

      (defun org-capture:frame-p (&rest _)
        "Return t if the current frame is an org-capture frame opened by
`org-capture:open-frame'."
        (and (equal (alist-get 'name org-capture:frame-parameters)
                    (frame-parameter nil 'name))
             (frame-parameter nil 'transient)))

      (defun org-capture:open-frame (&optional initial-input key)
        "Opens the org-capture window in a floating frame that cleans itself up once
you're done. This can be called from an external shell script."
        (interactive)
        (when (and initial-input (string-empty-p initial-input))
          (setq initial-input nil))
        (when (and key (string-empty-p key))
          (setq key nil))
        (let* ((frame-title-format "")
               (frame (if (org-capture:frame-p)
                          (selected-frame)
                        (make-frame org-capture:frame-parameters))))
          (select-frame-set-input-focus frame)  ; fix MacOS not focusing new frames
          (with-selected-frame frame
            (require 'org-capture)
            (condition-case ex
                (letf! ((#'pop-to-buffer #'switch-to-buffer))
                  (switch-to-buffer (fallback-buffer))
                  (let ((org-capture-initial initial-input)
                        org-capture-entry)
                    (when (and key (not (string-empty-p key)))
                      (setq org-capture-entry (org-capture-select-template key)))
                    (funcall #'org-capture)))
              ('error
               (message "org-capture: %s" (error-message-string ex))
               (delete-frame frame))))))
      
      (defun capture:get-todays-timestamp (&optional inactive)
        "Get the current acitve or INACTIVE timestamp for today’s date."
        (if inactive
            (format-time-string "[%Y-%m-%d %a]" (current-time))
          (format-time-string "<%Y-%m-%d %a>" (current-time))))

      (defun capture:get-week-timestamp (&optional inactive)
        "FIXME: ‘first-day-of-week’ and ‘last-day-of-week’ get the first of a 7 day period, not Sunday and Saturday."
        (if inactive
            (concat
             (format-time-string "Week: %U" (current-time))
             " - "
             (format-time-string "[%Y-%m-%d %a]" (first-day-of-week))
             "--"
             (format-time-string "[%Y-%m-%d %a]" (last-day-of-week)))
          (concat
           (format-time-string "Week: %U" (current-time))
           " - "
           (format-time-string "<%Y-%m-%d %a>" (first-day-of-week))
           "--"
           (format-time-string "<%Y-%m-%d %a>" (last-day-of-week)))))
      :config
      (org-capture-put :kill-buffer t)
      
      (add-hook 'org-capture-after-finalize-hook #'org-capture:cleanup-frame)
      
      (add-hook 'org-after-refile-insert-hook #'save-buffer)

      (defadvice! org-capture:expand-var-file (file)
        "TODO"
        :filter-args #'org-capture-expand-file
        (if (and (symbolp file) (boundp file))
            (expand-file-name (symbol-value file) org-directory)
          file))

      (if (and (package-installed-p 'evil)
               (bound-and-true-p evil-mode))
          (add-hook 'org-capture-mode-hook #'evil-insert-state))
      (if (and (package-installed-p 'meow)
               (bound-and-true-p meow-mode))
          (add-hook 'org-capture-mode-hook #'meow-insert-mode))

      (add-hook! 'org-capture-mode-hook
        (defun org-capture:show-target-in-header ()
          (setq header-line-format
                (format "%s%s%s"
                        (propertize (abbreviate-file-name (buffer-file-name (buffer-base-buffer)))
                                    'face 'font-lock-string-face)
                        org-eldoc-breadcrumb-separator
                        header-line-format))))))

  (defun org:setup-attachments ()
    (setq org-attach-store-link-p t
          org-attach-use-inheritance t)

    (use-package org-attach
      :ensure nil
      :commands (org-attach-new
                 org-attach-open
                 org-attach-open-in-emacs
                 org-attach-reveal-in-emacs
                 org-attach-url
                 org-attach-set-directory
                 org-attach-sync)
      :config
      (unless org-attach-id-dir
        (setq org-attach-id-dir (expand-file-name ".attach/" org-directory)))))

;;;;; Org Links
  (defun org:setup-custom-links ()
    (org-link-set-parameters
     "file"
     :face (lambda (path)
             (if (or (file-remote-p path)
                     (file-exists-p path))
                 'org-link
               'error)))
    (pushnew! org-link-abbrev-alist
              '("github"      . "https://github.com/%s")
              '("youtube"     . "https://youtube.com/watch?v=%s")
              '("google"      . "https://google.com/search?q=")
              '("gimages"     . "https://google.com/images?q=%s")
              '("gmap"        . "https://maps.google.com/maps?q=%s")
              '("duckduckgo"  . "https://duckduckgo.com/?q=%s")
              '("wikipedia"   . "https://en.wikipedia.org/wiki/%s")
              '("wolfram"     . "https://wolframalpha.com/input/?i=%s"))

    ;; (use-package org-yt
    ;;   :init
    ;;   (advice-add org-yt-image-data-fun :before-while (lambda (&rest _)
    ;;                                                     (not (eq org-display-remote-inline-images 'skip)))))
    )

;;;;; Org-Export
  (defun org:setup-export ()
    (general-setq org-export-with-smart-quotes t
                  org-html-validation-link nil
                  org-html-doctype "html5"
                  org-html-html5-fancy t
                  org-latex-prefer-user-labels t
                  org-export-with-toc nil
                  org-export-headline-levels 4
                  org-export-with-section-numbers nil)
    (add-to-list 'org-export-backends 'md)

    (use-package ox-gfm
      :config
      (add-to-list 'org-export-backends 'gfm))


    (defadvice! org-export:dont-trigger-save-hooks-on-export (orig-fn &rest args)
      "TODO"
      :around #'org-export-to-file
      (let (before-save-hook after-save-hook)
        (apply orig-fn args))))

;;;;; Org-Habit
  (defun org:setup-habit ()
    (add-hook! 'org-agenda-mode-hook
      (defun org-habit:resize-graph ()
        "TODO"
        ())))

;;;;; Hacks for Org-Mode
  (defun org:setup-hacks ()
    "A series of hack from Doom Emacs and around the internet."
    (general-setq org-element-use-cache nil) ;; potentially improve input latency
    (setf (alist-get 'file org-link-frame-setup) #'find-file)

    (add-to-list 'org-file-apps '(directory . emacs))
    (add-to-list 'org-file-apps '(remote . emacs))

    (advice-add #'org-link--open-help :around #'use-helpful)

    (defadvice! org:recenter-after-follow-link (&rest _args)
      "Recenter after following a link, but only internal or file links."
      :after '(org-footnote-action
               org-follow-timestamp-link
               org-link-open-as-file
               org-link-search)
      (when (get-buffer-window)
        (recenter)))

    (with-eval-after-load 'org-eldoc
      (puthash "org" #'ignore org-eldoc-local-functions-cache)
      (puthash "plantuml" #'ignore org-eldoc-local-functions-cache)
      (puthash "python" #'python-eldoc-function org-eldoc-local-functions-cache))

    (defadvice! org:show-parents (&optional arg)
      "Show all headlines in the buffer, like a table of contents.
With numerical argument N, show content up to level N."
      :override #'org-content
      (interactive "p")
      (org-show-all '(headings drawers))
      (save-excursion
        (goto-char (point-max))
        (let ((regexp (if (and (wholenump arg) (> arg 0))
                          (format "^\\*\\{%d,%d\\} " (1- arg) arg)
                        "^\\*+ "))
              (last (point)))
          (while (re-search-backward regexp nil t)
            (when (or (not (wholenump arg))
                      (= (org-current-level) arg))
              (org-flag-region (line-end-position) last t 'outline))
            (setq last (line-end-position 0))))))

    (defadvice! org:strip-properties-from-outline (fn &rest args)
      "Fix variable height faces in eldoc breadcrumbs."
      :around #'org-format-outline-path
      (let ((org-level-faces
             (cl-loop for face in org-level-faces
                      collect `(:foreground ,(face-foreground face nil t)
                                :weight bold))))
        (apply fn args)))

    (defadvice! org:export-agenda-from-recentf (orig-fn file)
      "TODO"
      :around #'org-get-agenda-file-buffer
      (let ((recentf-exclude (list (lambda (_file) t))))
        (funcall orig-fn file)))

    (defvar recentf-exclude)
    (defadvice! org:optimize-backgrounded-agenda-buffers (fn file)
      "Prevent temporarily opened agenda buffers from polluting recentf."
      :around #'org-get-agenda-file-buffer
      (let ((recentf-exclude (list (lambda (_file) t)))
            org-startup-indented
            org-startup-folded
            vc-handled-backends
            org-mode-hook
            find-file-hook)
        (funcall fn file)))

    (defadvice! org:fix-inline-images-imagemagick (orig-fn &rest args)
      "TODO"
      :around #'org-display-inline-images
      (letf! (defun create-image (file-or-data &optional type data-p &rest props)
               (let ((type (if (plist-get props :width) type)))
                 (apply create-image file-or-data type data-p props)))
        (apply orig-fn args)))

    (defadvice! org:fix-uuidgen (uuid)
      "TODO"
      :filter-return #'org-id-new
      (if (eq org-id-method 'uuid)
          (downcase uuid)
        uuid)))

  (defun org:element-descendant-of (type element)
    "Return non-nil if ELEMENT is a descendant of TYPE.
TYPE should be an element type, like `item' or `paragraph'.
ELEMENT should be a list like that returned by `org-element-context'.

Taken from: https://github.com/alphapapa/unpackaged.el/blob/master/unpackaged.el"
    ;; MAYBE: Use `org-element-lineage'.
    (when-let* ((parent (org-element-property :parent element)))
      (or (eq type (car parent))
          (org:element-descendant-of type parent))))

  (defun org:return-dwim (&optional default)
    "A helpful replacement for `org-return'.  With prefix, call `org-return'.
On headings, move point to position after entry content.  In
lists, insert a new item or end the list, with checkbox if
appropriate.  In tables, insert a new row or end the table.

Taken from: https://github.com/alphapapa/unpackaged.el/blob/master/unpackaged.el"

    (interactive "P")
    (if default
        (org-return)
      (cond
       ;; Act depending on context around point.

       ;; NOTE: I prefer RET to not follow links, but by uncommenting this block, links will be
       ;; followed.

       ;; ((eq 'link (car (org-element-context)))
       ;;  ;; Link: Open it.
       ;;  (org-open-at-point-global))

       ((org-at-heading-p)
        ;; Heading: Move to position after entry content.
        ;; NOTE: This is probably the most interesting feature of this function.
        (let ((heading-start (org-entry-beginning-position)))
          (goto-char (org-entry-end-position))
          (cond ((and (org-at-heading-p)
                      (= heading-start (org-entry-beginning-position)))
                 ;; Entry ends on its heading; add newline after
                 (end-of-line)
                 (insert "\n\n"))
                (t
                 ;; Entry ends after its heading; back up
                 (forward-line -1)
                 (end-of-line)
                 (when (org-at-heading-p)
                   ;; At the same heading
                   (forward-line)
                   (insert "\n")
                   (forward-line -1))
                 ;; FIXME: looking-back is supposed to be called with more arguments.
                 (while (not (looking-back (rx (repeat 3 (seq (optional blank) "\n")))))
                   (insert "\n"))
                 (forward-line -1)))))

       ((org-at-item-checkbox-p)
        ;; Checkbox: Insert new item with checkbox.
        (org-insert-todo-heading nil))

       ((org-in-item-p)
        ;; Plain list.  Yes, this gets a little complicated...
        (let ((context (org-element-context)))
          (if (or (eq 'plain-list (car context))  ; First item in list
                  (and (eq 'item (car context))
                       (not (eq (org-element-property :contents-begin context)
                                (org-element-property :contents-end context))))
                  (org:element-descendant-of 'item context))  ; Element in list item, e.g. a link
              ;; Non-empty item: Add new item.
              (org-insert-item)
            ;; Empty item: Close the list.
            ;; TODO: Do this with org functions rather than operating on the text. Can't seem to find the right function.
            (delete-region (line-beginning-position) (line-end-position))
            (insert "\n"))))

       ((when (fboundp 'org-inlinetask-in-task-p)
          (org-inlinetask-in-task-p))
        ;; Inline task: Don't insert a new heading.
        (org-return))

       ((org-at-table-p)
        (cond ((save-excursion
                 (beginning-of-line)
                 ;; See `org-table-next-field'.
                 (cl-loop with end = (line-end-position)
                          for cell = (org-element-table-cell-parser)
                          always (equal (org-element-property :contents-begin cell)
                                        (org-element-property :contents-end cell))
                          while (re-search-forward "|" end t)))
               ;; Empty row: end the table.
               (delete-region (line-beginning-position) (line-end-position))
               (org-return))
              (t
               ;; Non-empty row: call `org-return'.
               (org-return))))
       (t
        ;; All other cases: call `org-return'.
        (org-return)))))


  (defun org:indent-maybe ()
    "From Doom Emacs
Indent the current item (header or item), if possible.
Made for `org-tab-first-hook' in evil-mode."
    (interactive)
    (cond ((not (and (bound-and-true-p evil-local-mode)
                     (evil-insert-state-p)))
           nil)
          ((not (bound-and-true-p meow-insert-mode))
           nil)
          ((org-at-item-p)
           (if (eq this-command 'org-shifttab)
               (org-outdent-item-tree)
             (org-indent-item-tree))
           t)
          ((org-at-heading-p)
           (ignore-errors
             (if (eq this-command 'org-shifttab)
                 (org-promote)
               (org-demote)))
           t)
          ((org-in-src-block-p t)
           (org-babel-do-in-edit-buffer
            (call-interactively #'indent-for-tab-command))
           t)
          ((and (save-excursion
                  (skip-chars-backward " \t")
                  (bolp))
                (org-in-subtree-not-table-p))
           (call-interactively #'tab-to-tab-stop)
           t)))

  (defun org-refile:to-current-file (arg &optional file)
    "Refile current heading to elsewhere in current buffer.
If prefix ARG, copy instead of move."
    (interactive "P")
    (let ((org-refile-targets `((,file :maxlevel 10)))
          (org-refile-keep arg)
          current-prefix-arg)
      (call-interactively #'org-refile)))

  (defun org-refile:to-file (arg file)
    "Refile current heading to a particular org FILE.
If prefix ARG, copy instead of move."
    (interactive
     (let current-prefix-arg
       (read-file-name "Select file to refile to:"
                       default-directory
                       (buffer-file-name (buffer-base-buffer))
                       t nil
                       (lambda (f) (string-match-p "\\.org$" file)))))
    (org-refile:to-current-file arg file))


  (defun org-refile:to-other-window (arg)
    "Refile current heading to an org buffer visible in another window.
If prefix ARG, copy instead of move."
    (interactive "P")
    (let ((org-refile-keep arg)
          org-refile-targets
          current-prefix-arg)
      (dolist (win (delq (selected-window) (window-list)))
        (with-selected-window win
          (let ((file (buffer-file-name (buffer-base-buffer))))
            (and (eq major-mode 'org-mode)
                 file
                 (cl-pushnew (cons file (cons :maxlevel 10))
                             org-refile-targets)))))
      (call-interactively #'org-refile)))

;;;;; Keys for Org-Mode
  ;; TODO figure out some local prefix/leader key.
  (defun org:setup-keys ()
    (setq org-special-ctrl-a/e t
          org-M-RET-may-split-line nil
          ;; insert new headings after current subtree rather than inside it
          org-insert-heading-respect-content t)

    (add-hook 'org-tab-first-hook #'org:indent-maybe)

    ;; Org-Mode keys
    (define-key org-mode-map (kbd "TAB") #'org-cycle)
    (define-key org-mode-map (kbd "<tab>") #'org-cycle)
    (define-key org-mode-map (kbd "C-{") #'snippet:prev-position)
    (define-key org-mode-map (kbd "C-}") #'snippet:next-position)
    (define-key org-mode-map (kbd "RET") #'org:return-dwim)
    (define-key org-mode-map (kbd "<return>") #'org:return-dwim)
    (define-key org-mode-map (kbd "C-S-RET") #'org-insert-subheading)
    (define-key org-mode-map (kbd "<C-S-return>") #'org-insert-subheading))
  :init
  (add-hook! 'org-load-hook
             #'org:setup-dirs
             #'org:setup-appearance
             #'visual-line-mode
             #'org-indent-mode
             #'org:setup-agenda
             #'org:setup-habit
             #'org:setup-attachments
             #'org:setup-babel
             #'org:setup-capture
             #'org:setup-custom-links
             #'org:setup-export
             #'org:setup-hacks
             #'org:setup-keys)
  :custom
  (org-archive-subtree-save-file-p t)
  (org-id-locations-file-relative t)
  (org-ctrl-k-protect-subtree t)
  (tab-width 8)
  :config
  (setq-mode-local org-mode
                   tab-width 8
                   completion-at-point-functions
                   (list (cape-capf-buster #'cape:mega-writing-capf)))
  (add-hook 'org-mode-local-vars-hook #'eldoc-mode)

  (define-auto-insert
    '("\\.org\\’" . "Org File Title")
    '(nil
      "#+TITLE: "
      (string-join (mapcar #'capitalize (string-split (file-name-nondirectory (buffer-file-name)) "-")))
      \n
      "#+AUTHOR: Alec Stewart" \n
      "#+DATE: " (format-time-string "%Y-%m-%d" (current-time))
      \n
      @ _ ))

  (def-mode-snippet oauth org-mode
    "#+AUTHOR: " str \n)

  (def-mode-snippet oblk org-mode
    > "#+begin_" @ - " " @ _  \n
    > @ _ \n
    > "#+end_" @ -)

  (def-mode-snippet osrc org-mode
    > "#+begin_src " @ _  \n
    > @ _ \n
    > "#+end_src")

  (def-mode-snippet belsp org-mode
    > "#+begin_src emacs-lisp " @ _ \n
    > @ - \n
    > "#+end_src")

  (def-mode-snippet oqt org-mode
    > "#+begin_quote " @ _ \n
    > @ _ \n
    > "#+end_quote")

  (def-mode-snippet ovrs org-mode
    > "#+begin_verse" \n
    > @ - \n
    > "#+end_verse")

  (def-mode-snippet ocmnt org-mode
    > "#+begin_comment" \n
    > @ - \n
    > "#+end_comment")

  (def-mode-snippet ocntr org-mode
    > "#+begin_center" \n
    > @ - \n
    > "#+end_center")

  (def-mode-snippet ptd org-mode
    :prompt "Priority [A-C]: "
    (org-insert-todo-heading-respect-content) "[#" str "] " @ -)

  (def-mode-snippet excrstbl org-mode
    > "| Exercise | Sets | Reps | Notes |" \n
    > "|----------+------+------+-------|" \n
    > "| " @ - '(org-table-justify-field-maybe) " |  |  | |" \n))

;;;;; Other Nicities
;;;;;

(use-package org-modern
  :after org
  :demand t
  :custom
  (org-modern-star 'replace)
  (org-modern-priority-faces
   '((?A :background "red"
         :foreground "white")
     (?B :background "yellow"
         :foreground "white")
     (?C :background "green"
         :foreground "white")))
  :config
  (global-org-modern-mode +1))

(use-package org-chef)

(use-package doct
  :custom
  (org-capture-templates
   (doct `(("GTD" :keys "g"
            :jump-to-captured t
            :immediate-finish t
            :empty-lines-before 1
            :after-finalize (lambda ()
                              (org-update-statistics-cookies t)
                              (org-mode-restart))
            :children
            ((:group "Clocked"
              :clock-in t
              :clock-keep t
              :clock-resume t
              :children
              (("Today (Clocked)" :keys "D"
                :type entry
                :file ,(expand-file-name "~/Documents/Org/GTD/today.org")
                :function (lambda ()
                            (let* ((ts  (capture:get-todays-timestamp))
                                   (pnt (ignore-errors (org-find-olp (list ts) t))))
                              (if pnt
                                  (goto-char pnt)
                                (progn
                                  (end-of-buffer)
                                  (org-insert-heading nil nil 1)
                                  (end-of-line)
                                  (insert "[%] " ts)
                                  (goto-char (org-find-olp (list ts) t))))))
                :template-file ,(expand-file-name (concat my-emacs-dir "templates/capture/weekly-clocked-tasks.tmplt"))
                :before-finalize (lambda ()
                                   (funcall-interactively
                                    #'org-deadline nil (current-time))))
               ("Time tracking" :keys "t"
                :file ,(expand-file-name "~/Documents/Org/GTD/tt.org")
                :children
                (("Tasks" :type entry :keys "t" :template "* TODO Task %^{Task Name } :task: \n %?")
                 ("Meetings" :type entry :keys "m" :template "* TODO Meeting %^{Meeting Name } :meeting: \n %?")
                 ("Phone" :type entry :keys "p" :template "* TODO Phone Call %^{Phone Call Subject } :phone: \n %?")
                 ("Other" :type entry :keys "o" :template "* TODO %^{What are we doing? } :other: \n %?")))))
             (:group "Not clocked"
              :children
              (("Today (Manual)" :keys "m"
                :type entry
                :file ,(expand-file-name "~/Documents/Org/GTD/today.org")
                :function (lambda ()
                            (let* ((ts  (capture:get-todays-timestamp))
                                   (pnt (ignore-errors (org-find-olp (list ts) t))))
                              (if pnt
                                  (goto-char pnt)
                                (progn
                                  (end-of-buffer)
                                  (org-insert-heading nil nil 1)
                                  (end-of-line)
                                  (insert "[%] " ts)
                                  (goto-char (org-find-olp (list ts) t))))))
                :template-file
                ,(expand-file-name (concat my-emacs-dir "templates/capture/weekly-manual-tasks.tmplt")))
               ("Today (Unclocked)" :keys "d"
                :type entry
                :file ,(expand-file-name "~/Documents/Org/GTD/today.org")
                :function (lambda ()
                            (let* ((ts  (capture:get-todays-timestamp))
                                   (pnt (ignore-errors (org-find-olp (list ts) t))))
                              (if pnt
                                  (goto-char pnt)
                                (progn
                                  (end-of-buffer)
                                  (org-insert-heading nil nil 1)
                                  (end-of-line)
                                  (insert "[%] " ts)
                                  (goto-char (org-find-olp (list ts) t))))))
                :template-file
                ,(expand-file-name (concat my-emacs-dir "templates/capture/weekly-unclocked-tasks.tmplt"))
                :before-finalize (lambda ()
                                   (funcall-interactively
                                    #'org-deadline nil (current-time))))
               ("Week" :keys "W"
                :type entry
                :file ,(expand-file-name "~/Documents/Org/GTD/week.org")
                :function (lambda ()
                            (let* ((ts  (capture:get-week-timestamp))
                                   (pnt (ignore-errors (org-find-olp (list ts) t))))
                              (if pnt
                                  (goto-char pnt)
                                (progn
                                  (end-of-buffer)
                                  (org-insert-heading nil nil 1)
                                  (end-of-line)
                                  (insert "[%] " ts)
                                  (goto-char (org-find-olp (list ts) t))))))
                :template-file
                ,(expand-file-name (concat my-emacs-dir "templates/capture/weekly-unclocked-tasks.tmplt"))
                :before-finalize (lambda ()
                                   (funcall-interactively
                                    #'org-deadline nil (concat
                                                        (format-time-string "<%Y-%m-%d %a>" (first-day-of-week))
                                                        "--"
                                                        (format-time-string "<%Y-%m-%d %a>" (last-day-of-week))))))
               ("Habits" :keys "h"
                :file ,(expand-file-name "~/Documents/Org/GTD/habits.org")
                :children
                (("Daily" :keys "d"
                  :type entry
                  :template-file
                  ,(expand-file-name (concat my-emacs-dir "templates/capture/daily-habit.tmplt")))
                 ("Every Other Day" :keys "D"
                  :type entry
                  :template-file ,(expand-file-name (concat my-emacs-dir "templates/capture/bidaily-habit.tmplt")))
                 ("TODO Weekly" :keys "w"
                  :disabled t
                  :type entry
                  :template-file ,(expand-file-name (concat my-emacs-dir "templates/capture/weekly-habit.tmplt")))
                 ("TODO Monthly" :keys "m"
                  :disabled t
                  :type entry
                  :template-file
                  ,(expand-file-name (concat my-emacs-dir "templates/capture/monthly-habit.tmplt")))))))))
           ("Calendar" :keys "c"
            :file ,(expand-file-name "~/Documents/Org/cal.org")
            :empty-lines-before 1
            :immediate-finish t
            :children
            (("Appointment" :keys "a"
              :headline "Appointments"
              :children
              (("On day" :keys "d"
                :type entry

                :template-file ,(expand-file-name (concat my-emacs-dir "templates/capture/appt.tmplt")))
               ("At time" :keys "D"
                :type entry
                :headline "Appointments"
                :template-file ,(expand-file-name (concat my-emacs-dir "templates/capture/appt-time.tmplt")))
               ("Repeat appointment" :keys "r"
                :disabled t
                :type entry
                :template-file ,(expand-file-name (concat my-emacs-dir "templates/capture/rappt.tmplt")))))))
           ("Notes" :keys "n"
            :empty-lines 1
            :children
            (("Work" :keys "w"
              :type entry
              :file ,(expand-file-name "~/Documents/Org/Notes/work.org")
              :template "* %^{Title} %? %i")))
           ("Ideas/Thoguhts" :keys "i"
            :empty-lines 1
            :jump-to-captured t
            :immediate-finish t
            :children
            (("Ideas" :keys "i"
              :type entry
              :file ,(expand-file-name "~/Documents/Org/Notes/ideas.org")
              :template "* %^{Idea Name} \n %? %a")
             ("Thought" :keys "t"
              :type entry
              :file ,(expand-file-name "~/Documents/Org/Notes/thoughts.org")
              :template "* %^{Thought} \n %? %a")))
           ("Cookbook" :keys "c"
            :empty-lines 1
            :children
            (("From URL" :keys "u"
              :type entry
              :file ,(expand-file-name "~/Documents/Org/cookbook.org")
              :template "%(org-chef-get-recipe-from-url)")
             ("Manual Entry"
              :type entry
              :file ,(expand-file-name "~/Documents/Org/cookbook.org")
              :template
              "* %^{Recipe title: }\n  :PROPERTIES:\n  :source-url:\n  :servings:\n  :prep-time:\n  :cook-time:\n  :ready-in:\n  :END:\n** Ingredients\n   %?\n**Directions %i %a")))
           ("Add Contact" :keys "p"
            :empty-lines-before 1
            :immediate-finish t
            :type entry
            :file ,(expand-file-name "~/Documents/Org/phonebook.org")
            :template-file ,(concat my-emacs-dir "templates/capture/contact.tmplt"))))))

(use-package org-crypt
  :ensure nil
  :commands org-encrypt-entries org-encrypt-entry org-decrypt-entries org-decrypt-entry
  :hook (org-reveal-start . org-decrypt-entry)
  :preface
  ;; org-crypt falls back to CRYPTKEY property then `epa-file-encrypt-to', which
  ;; is a better default than the empty string `org-crypt-key' defaults to.
  (defvar org-crypt-key nil)
  :config
  (add-to-list 'org-tags-exclude-from-inheritance "crypt")
  (add-hook! 'org-mode-hook
    (add-hook 'before-save-hook 'org-encrypt-entries nil t)))

(use-package org-clock
  :ensure nil
  :commands org-clock-save
  :init
  (general-setq org-clock-persist-file (concat my-etc-dir "org-clock-save.el"))
  :custom
  (org-clock-persist 'history)
  (org-clock-in-resume t)
  :config
  (defadvice! org-clock:lazy-load (&rest _)
    "Lazy load org-clock until its commands are used."
    :before '(org-clock-in
              org-clock-out
              org-clock-in-last
              org-clock-goto
              org-clock-cancel)
    (org-clock-load))
  (add-hook 'kill-emacs-hook #'org-clock-save))

(use-package toc-org
  :hook (org-mode-hook . toc-org-enable)
  :config
  (setq toc-org-hrefify-default "gh"))

(use-package org-transclusion
  :after org
  :general
  (:keymaps 'global-map
   "<f12>" #'org-transclusion-add)
  (:keymaps 'org-mode-map
   "C-c t" #'org-transclusion-mode))

(use-package org-pdftools
  :when (package-installed-p 'pdftools)
  :commands org-pdftools-export
  :init
  (with-eval-after-load 'org
    (org-link-set-parameters (or (bound-and-true-p org-pdftools-link-prefix) "pdf")
                             :follow #'org-pdftools-open
                             :complete #'org-pdftools-complete-link
                             :store #'org-pdftools-store-link
                             :export #'org-pdftools-export)
    (add-hook! 'org-open-link-functions
      (defun org-pdftools:open-legacy-pdf-links-fn (link)
        (let ((regexp "^pdf\\(?:tools\\|view\\):"))
          (when (string-match-p regexp link)
            (org-pdftools-open (replace-regexp-in-string regexp "" link))
            t))))))

;;;; LaTeX
;;;;

(use-package tex-mode
  :ensure (auctex :repo "https://git.savannah.gnu.org/git/auctex.git"
                  :branch "main"
                  :pre-build (("make" "elpa"))
                  :build (:not elpaca--compile-info) ;; Make will take care of this step
                  :files ("*.el" "doc/*.info*" "etc" "images" "latex" "style")
                  :version (lambda (_) (require 'tex-site) AUCTeX-version))
  :mode ("\\.tex\\'" . LaTeX-mode)
  :hook ((LaTeX-mode . visual-line-mode)
         (LaTeX-mode . LaTeX-math-mode))
  :preface
  (defun latex:indent-item-fn ()
    "Indent LaTeX \"itemize\",\"enumerate\", and \"description\" environments.

\"\\item\" is indented `LaTeX-indent-level' spaces relative to the beginning
of the environment.

See `LaTeX-indent-level-item-continuation' for the indentation strategy this
function uses."
    (save-match-data
      (let* ((re-beg "\\\\begin{")
             (re-end "\\\\end{")
             (re-env "\\(?:itemize\\|\\enumerate\\|description\\)")
             (indent (save-excursion
                       (when (looking-at (concat re-beg re-env "}"))
                         (end-of-line))
                       (LaTeX-find-matching-begin)
                       (+ LaTeX-item-indent (current-column))))
             (contin LaTeX-indent-level))
        (cond ((looking-at (concat re-beg re-env "}"))
               (or (save-excursion
                     (beginning-of-line)
                     (ignore-errors
                       (LaTeX-find-matching-begin)
                       (+ (current-column)
                          LaTeX-item-indent
                          LaTeX-indent-level
                          (if (looking-at (concat re-beg re-env "}"))
                              contin
                            0))))
                   indent))
              ((looking-at (concat re-end re-env "}"))
               (save-excursion
                 (beginning-of-line)
                 (ignore-errors
                   (LaTeX-find-matching-begin)
                   (current-column))))
              ((looking-at "\\\\item")
               (+ LaTeX-indent-level indent))
              ((+ contin LaTeX-indent-level indent))))))
  :init
  (with-eval-after-load 'tex-site
    (TeX-modes-set 'TeX-modes TeX-modes)
    (setq-default TeX-master t))
  :custom
  (TeX-parse-self t) ; parse on load
  (TeX-auto-save t)  ; parse on save
  ;; use hidden dirs for auctex files
  (TeX-auto-local ".auctex-auto")
  (TeX-style-local ".auctex-style")
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-method 'synctex)
  ;; don't start the emacs server when correlating sources
  (TeX-source-correlate-start-server nil)
  ;; automatically insert braces after sub/superscript in math mode
  (TeX-electric-sub-and-superscript t)
  ;; Use xelatex to support unicode
  (TeX-engine 'xetex)
  (tex-command "xelatex")
  (LaTeX-command "xelatex")
  (LaTeX-section-hook '(LaTeX-section-heading
                        LaTeX-section-title
                        LaTeX-section-toc
                        LaTeX-section-section
                        LaTeX-section-label))
  (LaTeX-fill-break-at-separators nil)
  (LaTeX-item-indent 0)
  :config
  ;; Fix #1849: allow fill-paragraph in itemize/enumerate
  (defadvice! latex:re-indent-itemize-and-enumerate (fn &rest args)
    "TODO"
    :around #'LaTeX-fill-region-as-para-do
    (let ((LaTeX-indent-environment-list
           (append LaTeX-indent-environment-list
                   '(("itemize"   latex:indent-item-fn)
                     ("enumerate" latex:indent-item-fn)))))
      (apply fn args)))

  (defadvice! latex:dont-indent-itemize-and-enumerate (fn &rest args)
    "TODO"
    :around #'LaTeX-fill-region-as-paragraph
    (let ((LaTeX-indent-environment-list LaTeX-indent-environment-list))
      (delq! "itemize" LaTeX-indent-environment-list 'assoc)
      (delq! "enumerate" LaTeX-indent-environment-list 'assoc)
      (apply fn args))))

;;;; Typst, LaTeX but better (easier)
;;;;

(use-package typst-ts-mode
  :ensure (typst-ts-mode :repo "https://git.sr.ht/~meow_king/typst-ts-mode"
                         :files ("*.el"))
  :hook (typst-ts-mode . lsp-deferred)
  :mode "\\.typ\\'"
  :custom
  (typst-ts-mode-watch-options "--open")
  (typst-ts-mode-enable-raw-blocks-highlight t))

(use-package ox-typst
  :ensure (ox-typst :repo "https://github.com/jmpunkt/ox-typst")
  :after org)

;;;; EKG
;;;;

(use-package ekg
  :general (emacs:leader-def
             "ee" #'ekg:capture
             "eu" #'ekg:capture-url
             "ef" #'ekg:capture-file
             "ew" #'ekg:capture-750words
             "eg" #'ekg:capture-gripboard-log)
  :preface
  (defun ekg:capture ()
    "Wrapper of ‘ekg-capture’ opening a workspace
with ‘tabspaces-switch-or-create-workspace’."
    (interactive)
    (tabspaces-switch-or-create-workspace "Notes")
    (call-interactively #'ekg-capture))

  (defun ekg:capture-url ()
    "Wrapper of ‘ekg-capture-url’ opening a workspace
with `tabspaces-switch-or-create-workspace'."
    (interactive)
    (tabspaces-switch-or-create-workspace "Notes")
    (call-interactively #'ekg-capture-url))

  (defun ekg:capture-file ()
    "Wrapper of `ekg-capture-file' opening a workspace
with `tabspaces-switch-or-create-workspace'."
    (interactive)
    (tabspaces-switch-or-create-workspace "Notes")
    (call-interactively #'ekg-capture-file))
  
  (defun ekg:capture-750words ()
    "Capture writing for 750words for the day.
https://new.750words.com"
    (interactive)
    (tabspaces-switch-or-create-workspace "Notes")
    (ekg-capture :tags (list "750words"
                             (format-time-string "%m-%d-%Y" (current-time)))))

  (defun ekg:capture-gripboard-log ()
    "Capture a training log to post on the Gripboard.
https://www.gripboard.com/"
    (interactive)
    (tabspaces-switch-or-create-workspace "Notes")
    (ekg-capture :tags (list "grip" "gripboard log"
                             (format-time-string "%m-%d-%Y" (current-time)))))
  :init
  (general-setq triples-default-database-filename (concat my-local-dir "triples.db")
                ekg-db-file (concat org-directory "EKG/ekg.db")
                ;; Don’t automatically add date tag to notes.
                ekg-capture-auto-tag-funcs '()))

;;;; SES: Simple Emacs Spreadsheet
;;;;

(use-package ses
  :ensure nil
  :mode ("\\.ses\\'" . ses-mode)
  :config
  ;; Pull from the Emacs Wiki:
  ;; http://emacswiki.org/emacs/SimpleEmacsSpreadsheet
  (defun ses:read-from-csv-file (file)
    "Insert the contents of a CSV file named FILE into the current position."
    (interactive "fCSV file: ")
    (let ((buf (get-buffer-create "*ses-csv*"))
          text)
      (save-excursion
        (set-buffer buf)
        (erase-buffer)
        ;; TODO what in God's name is this?
        (process-file "ruby" file buf nil "-e" "require 'csv'; CSV::Reader.parse(STDIN) { |x| puts x.join(\"\\t\") }")
        (setq text (buffer-substring (point-min) (point-max))))
      (ses-yank-tsf text nil)))

  (defun ses:write-to-csv-file (file)
    "Write the values of the current buffer into a CSV file named FILE."
    (interactive "FCSV file: ")
    (push-mark (point-min) t t)
    (goto-char (- (point-max) 1))
    (ses-set-curcell)
    (ses-write-to-csv-file-region file))

  (defun ses:write-to-csv-file-region (file)
    "Write the values of the region into a CSV file named FILE."
    (interactive "FCSV file: ")
    (ses-export-tab nil)
    (let ((buf (get-buffer-create "*ses-csv*")))
      (save-excursion
        (set-buffer buf)
        (erase-buffer)
        (yank)
        ;; TODO why?
        ;; TODO redo this?
        (call-process-region
         (point-min)
         (point-max)
         "ruby" t buf nil "-e" "require 'csv'; w = CSV::Writer.create(STDOUT); STDIN.each { |x| w << x.chomp.split(/\\t/) }")
        (write-region (point-min) (point-max) file)))))

;;;; Artist
;;;;

(use-package artist
  :ensure nil
  :config
  ;; TODO need to be able to save art to file and clear scratch buffer
  ;; when finished
  (defun artist:switch ()
    "Switch to `*scratch*' in order to draw ASCII art."
    (interactive)
    (switch-to-buffer "*scratch*" nil t)
    (artist-mode)))

;;;; Markdown, the **inferior** markup format
;;;;

(use-package markdown-mode
  :mode ("/README\\(?:\\.md\\)?\\'" . gfm-mode)
  :hook (markdown-mode . lsp)
  :init
  (with-eval-after-load 'org-src
    (add-to-list 'org-src-lang-modes '("md" . markdown)))
  :custom
  (markdown-enable-math t)
  (markdown-enable-wiki-links t)
  (markdown-italic-underscore t)
  (markdown-asymmetric-header t)
  (markdown-fontify-code-blocks-natively t)
  (markdown-gfm-additional-languages '("sh"))
  (markdown-make-gfm-checkboxes-buttons t)
  (markdown-open-command
   (cond (macos-p "open")
         (linux-p "xdg-open")))
  (markdown-content-type  "application/xhtml+xml")
  (markdown-css-paths
   '("https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
     "https://cdn.jsdelivr.net/npm/github-markdown-css/github-markdown.min.css"
     "https://cdn.jsdelivr.net/gh/highlightjs/cdn-release/build/styles/github.min.css"))
  (markdown-xhtml-header-content
   (concat "<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>"
           "<link rel=’stylesheet’ type=’text/css’ href=’https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css’"
           "<style> body { box-sizing: border-box; max-width: 740px; width: 100%; margin: 40px auto; padding: 0 10px; } </style>"
           "<script id='MathJax-script' async src='https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js'></script>"
           "<script src='https://cdn.jsdelivr.net/gh/highlightjs/cdn-release/build/highlight.min.js'></script>"
           "<script>document.addEventListener('DOMContentLoaded', () => { document.body.classList.add('markdown-body'); document.querySelectorAll('pre[lang] > code').forEach((code) => { code.classList.add(code.parentElement.lang); }); document.querySelectorAll('pre > code').forEach((code) => { hljs.highlightBlock(code); }); });</script>"))
  :config
  (advice-add #'markdown-match-generic-metadata
      :override (lambda (&rest _)
                  (ignore (goto-char (point-max)))))

  (setf (alist-get '(markdown-mode markdown-ts-mode)
                   flymake-collection-hook-config nil nil #'equal)
        '((flymake-collection-vale
           :predicate (lambda () (executable-find "vale")))
          (flymake-collection-proselint :disabled t)))

  ;; Don't trigger autofill in code blocks (see `auto-fill-mode')
  (setq-mode-local markdown-mode
                   fill-nobreak-predicate (cons #'markdown-code-block-at-point-p
                                                fill-nobreak-predicate)
                   completion-at-point-functions
                   (list (cape-capf-buster #'cape:mega-writing-capf))))

;;;; Literate Calc
;;;;

(use-package literate-calc-mode)

(provide 'writing)
;;; writing.el ends here
