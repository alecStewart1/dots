;;; app.el --- Using Emacs Like an actual application -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'lib)
(require 'mode-local)
(require 'general)
(require 'transient)
(require 'casual-lib)
(require 'auth-source)
(require 'auth-source-pass)

;;;; Builtin Packages


;;;;; Allow us to enter passwords in Emacs
;;;;;

;; (use-package epg-config
;;   :ensure nil
;;   :demand t
;;   :init
;;   (setq epg-pinentry-mode 'loopback))

;;;; External Packages
;;;;

;;;;; (Go)Pass
;;;;;

(use-package password-store)

(use-package pass
  :requires password-store
  :preface
  (defun consult:pass (arg pass)
    "Stolen from Doom Emacs.
https://github.com/doomemacs/doomemacs/blob/master/modules/tools/pass/autoload/consult.el"
    (interactive
     (list current-prefix-arg
           (progn
             (require 'consult)
             (consult--read (password-store-list)
                            :prompt "(Go)Pass: "
                            :sort nil
                            :require-match t
                            :category 'pass))))
    (funcall (if arg
                 #'password-store-url
               #'password-store-copy)
             pass))
  :custom
  (password-store-executable (executable-find "gopass"))
  (password-store-password-length 64)
  (auth-source-pass-filename
   (or (getenv "PASSWORD_STORE_DIR")
       (expand-file-name "~/.local/share/gopass/stores/root")))
  :config
  (auth-source-pass-enable))

;;;;; ledger (or rather hledger)
;;;;;

(use-package ledger-mode
  :when (executable-find "hledger")
  :mode ("\\.hledger\\'"
         "\\.journal\\'")
  :hook (ledger-mode . outline-minor-mode)
  :bind (:map ledger-mode-map
         ([remap save-buffer] . ledger:clean-and-save-buffer))
  :init
  (general-setq
   ledger-binary-path (executable-find "hledger")
   ledger-test-binary (executable-find "hledger")
   ledger-mode-should-check-version nil
   ledger-clear-whole-transactions 1
   ledger-copy-transaction-insert-blank-line-after t
   ledger-report-native-highlighting-arguments '("--color=always")
   ledger-highlight-xact-under-point nil
   ledger-report-links-in-register nil
   ledger-report-use-header-line t
   ledger-report-auto-width nil
   ledger-report-use-strict t
   ledger-report-auto-refresh-sticky-cursor t)
  :preface
  (defvar ledger:current-journal
    (expand-file-name
     (concat "~/Documents/Ledger/"
             (format-time-string "%Y" (current-time)) "/index.hledger"))
    "The current year’s ledger file.")

  (defconst ledger:outline-regexp
    (rx
     (|
      "^[\\*]+[ \t]+\\([^\n\r]+\\)\\"
      (group bol ";; " alnum)
      (group bol ";;; " alnum)
      (group bol ";;;; " alnum)))
    "Regexp for outline headings in ‘ledger-mode’.")

  (defconst ledger:imenu-regexp
    (rx (|
                                        ; outline headings
         (regexp ledger:outline-regexp)
                                        ; transactions
         "^[0-9]\\{4\\}[-/.][0-9]\\{2\\}[-/.][0-9]\\{2\\}[ \t]+[^\n]+"))
    "Regexp for imenu for ‘ledger-mode’.")

  (defalias 'ledger:cape
    (cape-capf-super
     #'ledger-complete-at-point
     #'cape-abbrev
     #'cape-dabbrev)
    "A Capf for ‘ledger-mode’.")

  ;; From: https://old.reddit.com/r/emacs/comments/8x4xtt/tip_how_i_use_ledger_to_track_my_money/e22pfjw/
  (defun ledger:clean-and-save-buffer ()
    "Clean the current buffer with ‘ledger-mode-clean-buffer’
then save the buffer."
    (interactive)
    (save-excursion
      (when (buffer-modified-p)
        (with-demoted-errors
            (ledger-mode-clean-buffer))
        (save-buffer))))
  :config
  (defadvice! ledger:fail-gracefully-if-absent (fn)
    "Fail gracefully if ledger binary isn't available."
    :around #'ledger-check-version
    (if (executable-find ledger-binary-path)
        (funcall fn)
      (message "Couldn't find '%s' executable" ledger-binary-path)))

  (font-lock-add-keywords 'ledger-mode outline-font-lock-keywords)

  (setq-mode-local ledger-mode
                   completion-at-point-functions (list (cape-capf-buster #'ledger:cape))
                   outline-regexp ledger:outline-regexp
                   imenu-generic-expression
                   `((nil
                      ,ledger:imenu-regexp
                      0))))

(use-package flymake-hledger
  :after (ledger-mode flymake)
  :hook (ledger-mode . flymake-hledger-enable)
  :custom
  (flymake-hledger-checks
   '("parseable" "tags" "recentassertions" "payees" "ordereddates" "accounts" "commodities" "autobalanced" "uniqueleafnames")))

;;;;; Password Entry in Emacs
;;;;;

(use-package pinentry
  :hook
  (elpaca-after-init . pinentry-start))

;;;;; Emacs Multimedia System
;;;;;

;;; TODO make better and more clear use of EMMS
;;; TODO See if listen.el is better: https://github.com/alphapapa/listen.el
(use-package emms
  :commands (emms emms-locate emms-browser emms-add-url emms-add-file emms-add-find emms-play-file emms-play-url)
  :preface
  (defconst emms:music-dir
    (expand-file-name "~/Music/")
    "The main directory of music files (mp3, opus, etc.) for ‘emms’ to play.")

  (defconst emms:audiobook-dir
    (concat emms:music-dir "Audiobooks/")
    "The directory to specifically play audiobooks.")

  (defconst emms:video-dir
    (expand-file-name "~/Videos/")
    "The main directory of vidoes (mp4, ogg, mov, etc) for ‘emms’ to play.")
  :init
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)
  (emms-cache-enable)
  :custom
  (emms-player-mpv-parameters '("--no-msg-color" "--idle" "--quiet" "--audio-display=no"))
  (emms-directory (concat my-etc-dir "emms"))
  (emms-cache-file (concat my-cache-dir "emms"))
  (emms-source-file-default-directory emms:music-dir)
  (emms-player-mpv-update-metadata t)
  (emms-browser-covers #'emms-browser-cache-thumbnail-async))

(use-package listen
  :defer t
  :custom
  (listen-directory (expand-file-name "~/Music/"))
  (listen-backend #'make-listen-player-mpv))

;;;;; EWW
;;;;;

(use-package eww
  :ensure nil
  :general
  (:keymaps 'eww-mode-map
   "C-c +"      #'eww:increase-font-size
   "C-c -"      #'eww:decrease-font-size
   "C-x w q"    #'eww:quit
   "C-<return>" #'eww:open-in-other-window)
  :preface
  (defun eww:page-title-or-url (&rest _)
    "Stolen from Doom Emacs: https://github.com/doomemacs/doomemacs/blob/master/modules/emacs/eww/autoload.el"
    (let ((prop (if (string-empty-p (plist-get eww-data :title)) :url :title)))
      (format "*%s # eww*" (plist-get eww-data prop))))

  (defun eww:quit ()
    "Quit ‘eww’ and kill all of its buffers."
    (interactive nil 'eww-mode)
    (when (yes-or-no-p "Are you sure you want to quit eww?")
      (save-match-data
        (cl-loop with case-fold-search = t
                 for buf in (buffer-list)
                 if (with-current-buffer buf
                      (or (eq major-mode 'eww-mode)
                          (and (derived-mode-p 'special-mode)
                               (string-match "\\*.*eww.*\\*" (buffer-name)))))
                 do (kill-buffer buf)))))

  (defun eww:open-in-other-window ()
    "Use `eww-open-in-new-buffer' in another window."
    (interactive nil 'ewe-mode)
    (other-window-prefix)
    (eww-open-in-new-buffer))

  (defun eww:increase-font-size ()
    "Increase the font size in `eww-mode'."
    (interactive nil 'eww-mode)
    (if shr-use-fonts
        (let* ((cur (face-attribute 'shr-text :height nil))
               (cur (if (floatp cur) cur 1.0)))
          (set-face-attribute 'shr-text nil :height (+ cur 0.1)))
      (text-scale-increase 0.5)))

  (defun eww:decrease-font-size ()
    "Decrease the font size in `eww-mode'."
    (interactive nil 'eww-mode)
    (if shr-use-fonts
        (let* ((cur (face-attribute 'shr-text :height nil))
               (cur (if (floatp cur) cur 1.0)))
          (set-face-attribute 'shr-text nil :height (- cur 0.1)))
      (text-scale-decrease 0.5))))

(use-package shrface
  :after eww
  :general
  (:keymaps  'eww-mode-map
   "<tab>"   #'shrface-outline-cycle
   "S-<tab>" #'shrface-outline-cycle-buffer
   "C-t"     #'shrface-toggle-bullets
   "C-n"     #'shrface-next-headline
   "C-p"     #'shrface-previous-headline
   "M-l"     #'shrface-links-consult
   "M-h"     #'shrface-headline-consult)
  :preface
  (defun shrface:setup ()
    "TODO"
    (unless shrface-toggle-bullets
      (shrface-regexp)
      (setq-local imenu-create-index-function #'shrface-imenu-get-tree)))

  (defun shrface:advise-eww (orig-fun &rest args)
    "TODO"
    (let ((shrface-org nil)
          (shr-bullet (concat (char-to-string shrface-item-bullet) " "))
          (shr-table-vertical-line "|")
          (shr-width 65)
          (shr-indentation 0)
          (shr-external-rendering-functions
           (append '((title . eww-tag-title)
                     (form . eww-tag-form)
                     (input . eww-tag-input)
                     (button . eww-form-submit)
                     (textarea . eww-tag-textarea)
                     (select . eww-tag-select)
                     (link . eww-tag-link)
                     (meta . eww-tag-meta)
                     ;; (a . eww-tag-a)
                     (code . shrface-tag-code)
                     ;;(pre . shrface-shr-tag-pre-highlight)
                     )
                   shrface-supported-faces-alist))
          (shrface-toggle-bullets nil)
          (shrface-href-versatile t)
          (shr-use-fonts nil))
      (apply orig-fun args)))
  :config
  (add-hook 'eww-after-render-hook #'org-indent-mode)
  (add-hook 'eww-after-render-hook #'eldoc-mode)
  (add-hook 'eww-after-render-hook #'eldoc-box-hover-mode)
  (add-hook 'eww-after-render-hook #'shrface-eww-setup)
  (advice-add 'eww-display-html :around #'shrface:advise-eww))

;;;;; RSS Feeds in Emacs
;;;;;

(use-package elfeed
  :ensure (:wait t)
  :commands (elfeed)
  :general
  (:keymaps 'elfeed-search-mode-map
   "o" #'elfeed:eww-open-dwim)
  (:keymaps 'elfeed-show-mode-map
   "o" #'elfeed:eww-open-dwim)
  :preface
  (defun elfeed:wrap ()
    "Enhances an elfeed entry's readability by wrapping it to a width of `fill-column'."
    (let ((inhibit-read-only t)
          (inhibit-modification-hooks t))
      (setq-local truncate-lines nil)
      (setq-local shr-use-fonts nil)
      (setq-local shr-width 85)
      (set-buffer-modified-p nil)))

  (defun elfeed:cleanup ()
    "Clean up after an elfeed session. Kill all elfeed and elfeed-org files."
    (interactive)
    (elfeed-db-compact)
    (let ((buf (previous-buffer)))
      (when (null buf)
        (switch-to-buffer (fallback-buffer))))
    (let ((search-buffers (buffers-in-mode 'elfeed-search-mode))
          (show-buffers (buffers-in-mode 'elfeed-show-mode))
          kill-buffer-query-functions)
      (dolist (file (bound-and-true-p rmh-elfeed-org-files))
        (when-let (buf (get-file-buffer (expand-file-name file org-directory)))
          (kill-buffer buf)))
      (dolist (b search-buffers)
        (with-current-buffer b
          (remove-hook 'kill-buffer-hook #'elfeed:cleanup :local)
          (kill-buffer b)))
      (mapc #'kill-buffer show-buffers)))

  (defun elfeed:put-sliced-image (spec alt &optional flags)
    "TODO"
    (letf! (defun insert-image (image &optional alt _area _slice)
             (let ((height (cdr (image-size image t))))
               (insert-sliced-image image alt nil (max 1 (/ height 20.0)) 1)))
      (shr-put-image spec alt flags)))

  (defun elfeed:render-image-tag-without-underline (dom &optional url)
    "TODO"
    (let ((start (point)))
      (shr-tag-img dom url)
      ;; And remove underlines in case images are links, otherwise we get an
      ;; underline beneath every slice.
      (put-text-property start (point) 'face '(:underline nil))))

  (defun elfeed:eww-open-dwim (entry)
    "Browse ‘elfeed’ entry with ‘eww’."
    (interactive (list (elfeed-search-selected :ignore-region)))
    (when (elfeed-entry-p entry)
      (when-let ((link (elfeed-entry-link entry)))
        (elfeed-untag entry 'unread)
        (eww-browse-url link)
        (elfeed-search-update-entry entry)
        (unless (use-region-p) (forward-line)))))

  (transient-define-prefix elfeed-transient-v-tmenu ()
    "My ‘elfeed’ video games ‘transient’ menu."
    ["Video games"
     ("g" "General" (lambda ()
                      (interactive)
                      (elfeed-search-set-filter "@6-months-ago +videogames")))
     ("d" "Development" (lambda ()
                          (interactive)
                          (elfeed-search-set-filter "@6-months-ago +videogames +dev")))
     ("c" "Commentary" (lambda ()
                         (interactive)
                         (elfeed-search-set-filter "@6-months-ago +videogames +commentary")))
     ("p" "Podcast" (lambda ()
                      (interactive)
                      (elfeed-search-set-filter "@6-months-ago +videogames +podcast")))]
    [:class transient-row
     (casual-lib-quit-one)
     (casual-lib-quit-all)])

  (transient-define-prefix elfeed-transient-d-tmenu ()
    "My ‘elfeed’ dev ‘transient’ menu."
    ["Development"
     ("n" "News" (lambda ()
                   (interactive)
                   (elfeed-search-set-filter "@6-months-ago +dev +news")))
     ("b" "Blog" (lambda ()
                   (interactive)
                   (elfeed-search-set-filter "@6-months-ago +dev +blog")))
     ("v" "Videos" (lambda ()
                     (interactive)
                     (elfeed-search-set-filter "@6-months-ago +dev +youtue")))]
    [:class transient-row
     (casual-lib-quit-one)
     (casual-lib-quit-all)])

  (transient-define-prefix elfeed-filter-tmenu ()
    "‘transient’ menu for filtering ‘elfeed-search-mode’ entries."
    ["Filter"
     ["v" "Video games›" elfeed-transient-v-tmenu :transient t]
     ["d" "Development›" efleed-transient-d-tmenu :transient t]
     ["p" "Politics" (lambda ()
                       (interactive)
                       (elfeed-search-set-filter "@6-months-ago +politics"))]
     ["c" "Commentary" (lambda ()
                         (interactive)
                         (elfeed-search-set-filter "@6-months-ago +commentary"))]
     ["y" "Youtube" (lambda ()
                      (interactive)
                      (elfeed-search-set-filter "@6-months-ago +youtube"))]
     ["t" "Think Tanks" (lambda ()
                          (interactive)
                          (elfeed-search-set-filter "@6-months-ago +thinktanks"))]]
    [:class transient-row
     (casual-lib-quit-one)
     (casual-lib-quit-all)])
  :custom
  (elfeed-db-directory (concat my-etc-dir "elfeed/db/"))
  (elfeed-enclosure-default-dir (concat my-etc-dir "elfeed/enclosures/"))
  (elfeed-search-filter "@6-months-ago +unread")
  (elfeed-show-entry-switch #'pop-to-buffer)
  (shr-max-image-proportion 0.8)
  (shr-cookie-policy nil)
  (shrface-bullets-bullet-list '("▼" "▽" "▿" "▾"))
  (url-queue-timeout 60)
  (elfeed-curl-timeout 60)
  :config
  (make-directory elfeed-db-directory t)
  (make-directory elfeed-enclosure-default-dir t)

  (add-hook 'elfeed-show-mode-hook #'elfeed:wrap)
  (add-hook! 'elfeed-search-mode-hook
    (add-hook 'kill-buffer-hook #'elfeed:cleanup nil 'local))

  (setq-mode-local elfeed-show-mode
                   shr-put-image-function #'elfeed:put-sliced-image
                   shr-external-rendering-functions
                   '((img . elfeed:render-image-tag-without-underline)))

  (defun embark-elfeed-target-url ()
    "Target the URL of the elfeed entry at point."
    (when-let (((derived-mode-p 'elfeed-search-mode))
               (entry (elfeed-search-selected :ignore-region))
               (url (elfeed-entry-link entry)))
      `(url ,url ,(line-beginning-position) . ,(line-end-position))))

  (defun embark-elfeed-url-candidates ()
    "Target the URLs of the selected elfeed entries."
    (when-let (((derived-mode-p 'elfeed-search-mode))
               (entries (elfeed-search-selected))
               (urls (mapcar #'elfeed-entry-link entries)))
      (cons 'url urls)))

  (with-eval-after-load 'embark
    (add-to-list 'embark-target-finders #'embark-elfeed-target-url)
    (add-to-list 'embark-candidate-collectors #'embark-elfeed-url-candidates)))

;;;;;; Configure and view elfeed in a nice way
;;;;;;

(use-package elfeed-org
  :after elfeed
  :preface
  (general-setq rmh-elfeed-org-files (list (expand-file-name (concat org-directory "elfeed.org"))))
  :config
  (elfeed-org)
  (defadvice! elfeed-org:skip-missing-files (&rest _)
    "TODO"
    :before '(elfeed rmh-elfeed-org-mark-feed-ignore elfeed-or-export-opml)
    (unless (file-name-absolute-p (car rmh-elfeed-org-files))
      (let* ((default-directory org-directory)
             (files (mapcar #'expand-file-name rmh-elfeed-org-files)))
        (dolist (file (cl-remove-if #'file-exists-p files))
          (message "elfeed-org: ignoring %S because it can't be read" file))
        (setq rmh-elfeed-org-files (cl-remove-if-not #'file-exists-p files))))))


;;;;;; Extra nice things for elfeed
;;;;;;

;; (use-package elfeed-goodies
;;   :after elfeed
;;   :demand t
;;   :config
;;   (elfeed-goodies/setup))


;;;;;; Get a summary of feeds
;;;;;;

(use-package elfeed-summary
  :after elfeed
  :demand t)

;;;;;; Inject YouTube feeds right into elfeed
;;;;;;

(use-package mpv
  :ensure (:wait t)
  :when (executable-find "mpv"))

(use-package elfeed-tube
  :ensure (:wait t)
  :after elfeed
  :when (and
         (executable-find "mpv")
         (executable-find "yt-dlp"))
  :demand t
  :bind (:map elfeed-show-mode-map
         ("F" . elfeed-tube-fetch)
         ([remap save-buffer] . elfeed-tube-save)
         :map elfeed-search-mode-map
         ("F" . elfeed-tube-fetch)
         ([remap save-buffer] . elfeed-tube-save))
  :custom
  (elfeed-tube-invidious-url "https://app.materialio.us/")
  :config
  (elfeed-tube-setup))

(use-package elfeed-tube-mpv
  :after (mpv elfeed-tube))

;;;;; TODO Email
;;;;;

;; (use-package mu4e
;;   :commands mu4e mu4e-compose-new
;;   :init
;;   (provide 'html2text)
;;   (when (or (not (or (require 'mu4e-config nil t)
;;                      (require 'mu4e-meta nil t)))
;;             (version< mu4e-mu-version "1.4"))
;;     (setq mu4e-maildir "~/.mail"
;;           mu4e-user-mail-address-list nil)))

(use-package notmuch
  :defer t
  :when (executable-find "notmuch")
  :hook (notmuch-message-mode . jinx-mode)
  :preface
  (defvar notmuch:backend
    'offlineimap
    "Backend to use for email synchronization.")

  (defvar notmuch:has-afew
    (executable-find "afew")
    "Predicate for if ‘afew’ tagging tool is installed.")

  (defvar notmuch:maildir
    (or (getenv "MAILDIR")
        (expand-file-name "~/.mail/"))
    "Default mail directory for ‘notmuch’.")

  (defvar notmuch:delete-tags
    '("+trash" "-inbox" "-unread")
    "Tags applied to mark emails for deletion.")

  (defvar notmuch:spam-tags
    '("+spam" "-inbox" "-unread")
    "Tags applied to mark emails for spam.")

  (defun notmuch:get-sync-command ()
    "Return a shell command string to synchronize your notmuch mail with."
    (let* ((afew-cmd "afew -n -t")
           (sync-cmd
            (pcase notmuch:backend
              (`gmi
               (concat "cd " notmuch:maildir " && gmi sync && notmuch new"))
              ((or `mbsync
                   `mbsync-xdg) ; DEPRECATED `mbsync-xdg' is now just `mbsync'
               (format "mbsync %s -a && notmuch new"
                       (if-let* ((config-file
                                  (expand-file-name (or (getenv "XDG_CONFIG_HOME")
                                                        "~/.config")
                                                    "isync/mbsyncrc")))
                           (format "-c %S" (car config-file))
                         "")))
              (`offlineimap
               "offlineimap && notmuch new")
              ((and (pred stringp) it) it)
              (_ (user-error "Invalid notmuch backend specified: %S"
                             notmuch:backend)))))
      (if notmuch:has-afew
          (format "%s && %s" sync-cmd afew-cmd)
        sync-cmd)))

  (defun notmuch:update ()
    "Sync notmuch emails with server."
    (interactive)
    (let ((compilation-buffer-name-function (lambda (_) (format "*notmuch update*"))))
      (with-current-buffer (compile (notmuch:get-sync-command))
        (add-hook
         'compilation-finish-functions
         (lambda (buf status)
           (if (equal status "finished\n")
               (progn
                 (delete-windows-on buf)
                 (bury-buffer buf)
                 (notmuch-refresh-all-buffers)
                 (message "Notmuch sync successful"))
             (user-error "Failed to sync notmuch data")))
         nil
         'local))))

  (defun notmuch:compose ()
    "Compose new mail."
    (interactive)
    (notmuch-mua-mail
     nil nil
     (list (cons 'From (completing-read "From: " (notmuch-user-emails))))))

  (defun notmuch:search-delete ()
    (interactive)
    (notmuch-search-add-tag notmuch:delete-tags)
    (notmuch-tree-next-message))

  (defun notmuch:tree-delete ()
    (interactive)
    (notmuch-tree-add-tag notmuch:delete-tags)
    (notmuch-tree-next-message))

  (defun notmuch:show-delete ()
    "Mark email for deletion in notmuch-show"
    (interactive)
    (notmuch-show-add-tag notmuch:delete-tags)
    (notmuch-show-next-thread-show))

  (defun notmuch:search-spam ()
    (interactive)
    (notmuch-search-add-tag notmuch:spam-tags)
    (notmuch-search-next-thread))

  (defun notmuch:tree-spam ()
    (interactive)
    (notmuch-tree-add-tag notmuch:spam-tags)
    (notmuch-tree-next-message))

  (defun notmuch:show-filter-thread ()
    "Show the current thread with a different filter"
    (interactive)
    (setq notmuch-show-query-context (notmuch-read-query "Filter thread: "))
    (notmuch-show-refresh-view t))

  (defun notmuch:show-expand-only-unread ()
    (interactive)
    (let ((unread nil)
          (open (notmuch-show-get-message-ids-for-open-messages)))
      (notmuch-show-mapc (lambda ()
                           (when (member "unread" (notmuch-show-get-tags))
                             (setq unread t))))
      (when unread
        (let ((notmuch-show-hook (remove 'notmuch:show-expand-only-unread notmuch-show-hook)))
          (notmuch-show-filter-thread "tag:unread")))))

  (defun notmuch:quit ()
    "Kill all ‘notmuch’ buffers."
    (interactive)
    (my-kill-matching-buffers "^\\*notmuch"))

  (defun notmuch:dont-confirm-on-kill-process (fn &rest args)
    "Don't prompt for confirmation when killing notmuch sentinel."
    (let (confirm-kill-processes)
      (apply fn args)))

  (defalias 'cape:notmuch-capf
    (cape-capf-buster (cape-company-to-capf #'notmuch-company)))
  :custom
  (notmuch-search-oldest-first nil)
  (notmuch-fcc-dirs nil)
  (message-kill-buffer-on-exit t)
  (message-send-mail-function #'message-send-mail-with-sendmail)
  (send-mail-function #'sendmail-send-it)
  (notmuch-search-result-format
   '(("date" . "%12s ")
     ("count" . "%-7s ")
     ("authors" . "%-30s ")
     ("subject" . "%-72s ")
     ("tags" . "(%s)")))
  (notmuch-tag-formats
   '(("unread" (propertize tag 'face 'notmuch-tag-unread))))
  (notmuch-saved-searches
   '((:name "inbox"   :query "tag:inbox not tag:trash"   :key "i")
     (:name "flagged" :query "tag:flagged"               :key "f")
     (:name "archive" :query "tag:archive not tag:trash" :key "a")
     (:name "sent"    :query "tag:sent"                  :key "s")
     (:name "drafts"  :query "tag:draft"                 :key "d")))
  (notmuch-archive-tags '("+archive" "-inbox" "-unread"))
  :config
  (defadvice! notmuch:search-show-thread (fn &rest args)
    "Give email buffers a sane name so they can be targeted via
`display-buffer-alist' (and the :ui popup module)."
    :around #'notmuch-search-show-thread
    (letf! (defun notmuch-show (thread-id &optional elide-toggle parent-buffer query-context buffer-name)
             (funcall notmuch-show
                      thread-id elide-toggle parent-buffer query-context
                      (format "*subject:%s*" (substring buffer-name 1 -1))))
      (apply fn args)))

  (add-hook 'notmuch-show-hook #'notmuch:show-expand-only-unread)
  (advice-add #'notmuch-start-notmuch-sentinel :around #'notmuch:dont-confirm-on-kill-process)

  (add-hook! '(notmuch-show-mode-hook
               notmuch-tree-mode-hook
               notmuch-search-mode-hook)
             #'hide-mode-line-mode)

  (general-define-key
   :keymaps '(notmuch-hello-mode-map notmuch-search-mode-map notmuch-tree-mode-map notmuch-show-mode-map)
   :prefix "C-x"
   "c" #'notmuch:compose
   "u" #'notmuch:update
   "q" #'notmuch:quit)

  (general-define-key
   :keymaps 'notmuch-search-mode-map
   :prefix "C-x"
   "d" #'notmuch:search-delete
   "s" #'notmuch:search-spam)

  (general-define-key
   :keymaps 'notmuch-tree-mode-map
   :prefix "C-x"
   "d" #'notmuch:tree-delete
   "s" #'notmuch:tree-spam)

  (setq-mode-local notmuch-message-mode
                   completion-at-point-functions (list #'cape:notmuch-capf
                                                       #'cape-dict
                                                       #'cape-dabbrev)))

(use-package org-mime
  :after (org notmuch)
  :custom
  (org-mime-library 'mml))

;; (use-package ol-notmuch
;;   :after (org notmuch))

(use-package consult-notmuch
  :commands consult-notmuch
  :after notmuch)

(provide 'app)
;;; app.el ends here
