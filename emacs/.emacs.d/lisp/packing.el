;;; packing.el --- Managing package.el and use-package.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;

;;; Code:

(require 'lib)
(require 'cl-lib)
(require 'comp)

;;; Do stuff with package.el HERE
;;;

;;;; Before we require everything else and packages are compiled, set these
;;;;

(when (and nativecomp-p
           (featurep 'native-compile))
  ;; We're going for distance, we're going for speed
  (setq-default
   native-comp-jit-compilation nil   
   comp-deferred-compilation t
   native-comp-speed 2
   comp-speed 2
   package-native-compile t)

  ;; Don't store eln files in ~/.emacs.d/eln-cache
  (add-to-list 'native-comp-eln-load-path (concat my-cache-dir "eln/"))
  ;; Disable some troublesome packages
  ;; (with-eval-after-load 'comp
  ;;   ;; HACK Disable native-compilation for some troublesome packages
  ;;   (mapc (apply-partially #'add-to-list 'native-comp-jit-compilation-deny-list)
  ;;         (let ((local-dir-re (concat "\\`" (regexp-quote my-local-dir))))
  ;;           (list
  ;;            ;;(concat local-dir-re ".*/evil-collection-vterm\\.el\\'")
  ;;            (concat local-dir-re ".*/with-editor\\.el\\'")))))
  )

;;; Some settings
;;;

(setq gnutls-verify-error t)
(setq package-enable-at-startup nil
      package-user-dir (concat my-local-dir "elpa/")
      package-gnupghome-dir (expand-file-name "gpg" package-user-dir)
      ;; I omit Marmalade because its packages are manually submitted rather
      ;; than pulled, so packages are often out of date with upstream.
      ;; Also Org ELPA is going to be closed on further releases of Org
      package-archives
      (let ((proto (if gnutls-verify-error "https" "http")))
        ;; may not actually need this with NonGNU ELPA, except for corfu.
        `(("gnu"   . ,(concat proto "://elpa.gnu.org/packages/"))
          ("melpa" . ,(concat proto "://melpa.org/packages/"))
          ("nongnu" . ,(concat proto "://elpa.nongnu.org/nongnu/"))))
      package-archive-priorities
      '(("nongnu" . 30)
        ("gnu" . 20)
        ("melpa" . 10)))

;;; Emacs really like to put stuff in your init.el if you don’t tell it not to
;;;

(advice-add #'package--ensure-init-file :override #'ignore)

;;;; HACK: DO NOT WRITE `custome-set-variables' AND FRIENDS TO INIT.EL
;;;;

(defadvice! write-to-sane-paths (fn &rest args)
  "Write 3rd party files to `my-etc-dir' to keep `user-emacs-directory' clean.

Also writes `put' calls for saved safe-local-variables to `custom-file' instead
of `user-init-file' (which `en/disable-command' in novice.el.gz is hardcoded to
do)."
  :around #'en/disable-command
  :around #'locate-user-emacs-file
  (let* ((user-emacs-directory my-etc-dir)
         (custom-file (expand-file-name "custom.el" user-emacs-directory))
         (user-init-file custom-file))
    (apply fn args)))

;;;; HACK: DO NOT copy package-selected-packages to init/custom file forcibly.
;;;; https://github.com/jwiegley/use-package/issues/383#issuecomment-247801751

(defadvice! dont-write-save-selected-packages (val)
  "Set `package-selected-packages' to VALUE but don't save to `custom-file'."
  :override #'package--save-selected-packages
  (when val
    (setq package-selected-packages val)))

;;; Glorious Elpaca
;;;

;;;###autoload
(defvar elpaca-installer-version 0.9
  "Current version of elpaca installer.")

;;;###autoload
(defvar elpaca-directory (expand-file-name (concat my-local-dir "elpaca/"))
  "Directory containing packages elpaca has downloaded.")

;;;###autoload
(defvar elpaca-builds-directory (expand-file-name (concat elpaca-directory "builds/"))
  "Directory for packages for elpaca it build.")

;;;###autoload
(defvar elpaca-repos-directory (expand-file-name (concat elpaca-directory "repos/"))
  "Directory for repos for elpaca to track.")

;;;###autoload
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package))
  "Uh, the actual elpaca package?")

(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let* ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                  ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                  ,@(when-let* ((depth (plist-get order :depth)))
                                                      (list (format "--depth=%d" depth) "--no-single-branch"))
                                                  ,(plist-get order :repo) ,repo))))
                  ((zerop (call-process "git" nil buffer t "checkout"
                                        (or (plist-get order :ref) "--"))))
                  (emacs (concat invocation-directory invocation-name))
                  ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                        "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                  ((require 'elpaca))
                  ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))

(add-hook 'after-init-hook #'elpaca-process-queues)

;;;###autoload
(defun elpaca-reload-package (package &optional allp)
  "Reload PACKAGE's features.
If ALLP is non-nil (interactively, with prefix), load all of its
features; otherwise only load ones that were already loaded.

This is useful to reload a package after upgrading it.  Since a
package may provide multiple features, to reload it properly
would require either restarting Emacs or manually unloading and
reloading each loaded feature.  This automates that process.

Note that this unloads all of the package's symbols before
reloading.  Any data stored in those symbols will be lost, so if
the package would normally save that data, e.g. when a mode is
deactivated or when Emacs exits, the user should do so before
using this command."
  (interactive
   (list (let ((elpaca-overriding-prompt "Reload package: "))
           (elpaca--read-queued))
         current-prefix-arg))
  ;; This finds features in the currently installed version of PACKAGE, so if
  ;; it provided other features in an older version, those are not unloaded.
  (when (yes-or-no-p (format "Unload all of %s's symbols and reload its features? " package))
    (let* ((package-name (symbol-name package))
           (package-dir (file-name-directory
                         (locate-file package-name load-path (get-load-suffixes))))
           (package-files (directory-files package-dir 'full (rx ".el" eos)))
           (package-features
            (cl-loop for file in package-files
                     when (with-temp-buffer
                            (insert-file-contents file)
                            (when (re-search-forward (rx bol "(provide" (1+ space)) nil t)
                              (goto-char (match-beginning 0))
                              (cadadr (read (current-buffer)))))
                     collect it)))
      (unless allp
        (setf package-features (seq-intersection package-features features)))
      (dolist (feature package-features)
        (ignore-errors
          ;; Ignore error in case it's not loaded.
          (unload-feature feature 'force)))
      (dolist (feature package-features)
        (require feature))
      (when package-features
        (message "Reloaded: %s" (mapconcat #'symbol-name package-features " "))))))

(elpaca `(,@elpaca-order))

;;(setq elpaca-ignored-dependencies
;;      (delq 'transient elpaca-ignored-dependencies))

;;;; Load the dumb ‘custom-file’ (that I don’t actually use) after elpaca’s `elpaca-after-init-hook'
;;;;

(when custom-file
  (add-hook 'elpaca-after-init-hook (lambda () (load custom-file 'noerror))))

;;; For use-package
;;;

(eval-when-compile
  (require 'use-package))

;; The following contents of this ‘with-eval-after-load’ is all from Doom Emacs.
(with-eval-after-load 'use-package-core

  ;; `use-package' adds syntax highlighting for the `use-package' macro, but
  ;; Emacs 26+ already highlights macros, so it's redundant.
  (font-lock-remove-keywords 'emacs-lisp-mode use-package-font-lock-keywords)

  ;; We define :minor and :magic-minor from the `auto-minor-mode' package here
  ;; so we don't have to load `auto-minor-mode' so early.
  (dolist (keyword '(:minor :magic-minor))
    (setq use-package-keywords
          (use-package-list-insert keyword use-package-keywords :commands)))

  (defalias 'use-package-normalize/:minor #'use-package-normalize-mode)
  (defun use-package-handler/:minor (name _ arg rest state)
    (use-package-handle-mode name 'auto-minor-mode-alist arg rest state))

  (defalias 'use-package-normalize/:magic-minor #'use-package-normalize-mode)
  (defun use-package-handler/:magic-minor (name _ arg rest state)
    (use-package-handle-mode name 'auto-minor-mode-magic-alist arg rest state))

  ;; HACK Fix `:load-path' so it resolves relative paths to the containing file,
  ;;      rather than `user-emacs-directory'. This is a done as a convenience
  ;;      for users, wanting to specify a local directory.
  (defadvice! resolve-load-path-from-containg-file (fn label arg &optional recursed)
    "Resolve :load-path from the current directory."
    :around #'use-package-normalize-paths
    ;; `use-package-normalize-paths' resolves paths relative to
    ;; `user-emacs-directory', so we change that.
    (let ((user-emacs-directory
           (or (and (stringp arg)
                    (not (file-name-absolute-p arg))
                    (ignore-errors (dir!)))
               sys-emacs-dir)))
      (funcall fn label arg recursed))))

(elpaca elpaca-use-package
  (elpaca-use-package-mode))

(use-package diminish :ensure (:wait t) :demand t)
(use-package bind-key :ensure nil :demand t)

(use-package general
  :ensure (:wait t)
  :demand t
  :preface
  (defvar ctrl-c-map (make-sparse-keymap)
    "The keymap for generic key combinations prefixed with Ctrl-c.")
  (defvar ctrl-x-map (make-sparse-keymap)
    "The keymap for mode local specific key combinations with Ctrl-x.")
  :config
  (general-create-definer emacs:leader-def
    :prefix "C-c"
    :keymap 'ctrl-c-map)

  (general-create-definer emacs:localleader-def
    :prefix "C-x"
    :keymap 'ctrl-x-map))

(use-package f
  :ensure (:wait t)
  :demand t)

(provide 'packing)
;;; packing.el ends here
