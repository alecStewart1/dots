;;; ui-ux.el --- To make the UI & UX of Emacs more sensible and modern  -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;  A lot of customizations for the UI/UX of Emacs.
;;
;;; Code:

(require 'general)
(require 'lib)

;;; My Splash Screen
;;;

;;;###autoload
(defvar my-splash-text (with-temp-buffer
                         (insert-file-contents (expand-file-name (concat user-emacs-directory "splash.txt")))
                         (buffer-string))
  "A string that's the text found in splash.txt.
This is used in my splash screen.")

;;;###autoload
(defconst splash:title-text
  `((:face font-lock-builtin-face
     ,(lambda ()
        (concat my-spalsh-text
                "\n<-=================================================->\n")))))

;;;###autoload
(defconst splash:subtitle-text
  `((:face variable-pitch
     "Alec Stewart’s Emacs\n"
     :face (font-lock-comment-face (:height 0.8))
     ,(lambda ()
        (if macos-p
            (shell-command-to-string
             "system_profiler SPSoftwareDataType | rg \"System Version\" | cut -d':' -f2 | xargs")
          linux-distro)))))

;;;###autoload
(defconst splash:links
  `((:face variable-pitch
     :link
     ("Emacs Config"
      ,(lambda (_button)
         (let ((browse-url-browser-function #'eww-browse-url))
           (browse-url "https://codeberg.org/alecStewart1/dots/src/branch/main/emacs/.emacs.d")))
      "View my Emacs configuration.\n")
     :link
     ("Other Configurations"
      ,(lambda (_button)
         (let ((browse-url-browser-function #'eww-browse-url))
           (browse-url "https://codeberg.org/alecStewart1/dots")))
      "View my other configurations.\n"))))


;;;###autoload
;; TODO actually use this
(defun my-splash ()
  "My personal splash screen for Emacs.

This is more or less a copy of the Nano Emacs splash screen:

https://github.com/rougier/nano-emacs/blob/master/nano-splash.el"
  (interactive)
  (let* ((splash-buffer (get-buffer-create "*Splash*"))
         (height          (round (- (window-body-height nil) 1)))
         (width           (round (window-body-width)))
         (paddding-center (+ (/ height 2) 1)))
    (with-current-buffer splash-buffer
      (setq header-line-format nil)
      (setq mode-line-format nil)
      (setq inhibit-read-only t)
      (erase-buffer)
      (setq cursor-type nil)
      (setq line-spacing 0)
      (setq vertical-scroll-bar nil)
      (setq horizontal-scroll-bar nil)
      (setq fill-column width)
      (if (not (display-graphic-p)) (menu-bar-mode 0))
      (insert-char ?\n paddding-center)
      (dolist (text splash:title-text)
        (apply #'fancy-splash-insert text))
      (center-line)
      (dolist (text splash:subtitle-text)
        (apply #'fancy-splash-insert text))
      (center-line)
      (insert "\n\n")
      (dolist (text splash:link)
        (apply #'fancy-splash-insert text))
      (center-line)
      (insert "\n\n")
      (goto-char 0)
      (display-buffer-same-window splash-buffer nil))))

;;; Some Transient keys
;;; TODO may add more in the future

;; (with-eval-after-load 'transient
;;   (transient-define-prefix zoom-transient ()
;;     "Zoomin' in and out on text"
;;     :transient-suffix 'transient--do-stay
;;     :transient-non-suffix 'transient--do-stay
;;     [["Zoom"
;;       ("j" "In"  text-scale-increase)
;;       ("k" "Out" text-scale-decrease)]])
;;   (global-set-key (kbd "<f2>") #'zoom-transient))

;;; Setting Fonts
;;;

(set-face-attribute 'default nil
                    :font my-default-font
                    :width 'normal
                    :slant 'normal)

(set-face-attribute 'fixed-pitch nil
                    :font my-default-font
                    :width 'normal)

(set-face-attribute 'fixed-pitch-serif nil
                    :font my-default-serif-font
                    :width 'normal)

(set-face-attribute 'variable-pitch nil
                    :font my-default-variable-font
                    :width 'normal
                    :weight 'regular)

;;; Packages
;;;

;;;; Help
;;;;

(use-package help
  :ensure nil
  :bind
  (("C-?" . help-command)
   (:map mode-specific-map
    ("h" . help-command)))
  :custom
  (help-window-select t))

(use-package help-fns
  :ensure nil
  :bind ("C-h K" . describe-keymap))

;;;; Helpful
;;;; a better *help* buffer

(use-package helpful
  :commands helpful--read-symbol
  :hook (helpful-mode . visual-line-mode)
  :bind ((:map global-map
          ([remap describe-function] . helpful-callable)
          ([remap describe-command] . helpful-command)
          ([remap describe-key] . helpful-key)
          ([remap describe-variable] . helpful-variable)
          ([remap describe-symbol] . helpful-symbol))
         (:map help-map
          ("k" . helpful-key)
          ("f" . helpful-callable)
          ("x" . helpful-command)
          ("o" . helpful-symbol)
          ("v" . helpful-variable)))
  :init
  (require 'f)
  (setq apropos-do-all t)

  (defun use-helpful (orig-fn &rest args)
    "Force ORIG-FN to use helpful instead of the old describe-* commands."
    (letf! ((#'describe-function #'helpful-function)
            (#'describe-variable #'helpful-variable))
      (apply orig-fn args)))
  ;; (with-eval-after-load 'apropos
  ;;   ;; patch apropos buttons to call helpful instead of help
  ;;   (dolist (fun-bt '(apropos-function apropos-macro apropos-command))
  ;;     (button-type-put
  ;;      fun-bt 'action
  ;;      (lambda (button)
  ;;        (helpful-callable (button-get button 'apropos-symbol)))))
  ;;   (dolist (var-bt '(apropos-variable apropos-user-option))
  ;;     (button-type-put
  ;;      var-bt 'action
  ;;      (lambda (button)
  ;;        (helpful-variable (button-get button 'apropos-symbol)))))
  :custom
  ;; Minimuze the amount of unused helpful buffers open
  (helpful-max-buffers 2))

;;;; So-Long
;;;; So we manage working with large files easier.

(defvar ui-ux:inhibit-large-file-detection nil
  "If non-nil, inhibit large/long file detection when opening files.")

(defvar ui-ux:large-file-p nil)
(put 'ui-ux:large-file-p 'permanent-local t)

(defvar ui-ux:large-file-size-alist '(("." . 1.0)))

(defvar ui-ux:large-file-excluded-modes
  '(so-long-mode special-mode archive-mode tar-mode jka-compr
                 git-commit-mode image-mode doc-view-mode doc-view-mode-maybe
                 ebrowse-tree-mode pdf-view-mode tags-table-mode))

(defadvice! ui-ux:prepare-for-large-files (size _ filename &rest _)
  "From Doom Emacs.
Sets `ui-ux:large-file-p' if the file is considered large.
Uses `ui-ux:large-file-size-alist' to determine when a file is too large. When
`ui-ux:large-file-p' is set, other plugins can detect this and reduce their
runtime costs (or disable themselves) to ensure the buffer is as fast as
possible."
  :before #'abort-if-file-too-large
  (and (numberp size)
       (null ui-ux:inhibit-large-file-detection)
       (ignore-errors
         (> size
            (* 1024 1024
               (assoc-default filename ui-ux:large-file-size-alist
                              #'string-match-p))))
       (setq-local ui-ux:large-file-p size)))

(add-hook! 'find-file-hook
  (defun ui-ux:optimize-for-large-files ()
    "Trigger `so-long-minor-mode' if the file is large."
    (when (and ui-ux:large-file-p buffer-file-name)
      (if (or ui-ux:inhibit-large-file-detection
              (memq major-mode ui-ux:large-file-excluded-modes))
          (kill-local-variable 'ui-ux:large-file-p)
        (when (fboundp 'so-long-minor-mode)
          (so-long-minor-mode +1))
        (message "Large file detected! Cutting a few corners to improve performance...")))))

(use-package so-long
  :ensure nil
  :delight
  :hook ((find-file . global-so-long-mode)
         (dired-initial-position . global-so-long-mode)
         (org-mode . so-long-minor-mode)
         (prog-mode . so-long-minor-mode))
  :config
  (defun so-long:does-buffer-have-long-lines ()
    (let ((so-long-skip-leading-comments (bound-and-true-p comment-use-syntax))
          (so-long-threshold
           (if visual-line-mode
               (* so-long-threshold
                  (if (derived-mode-p 'text-mode)
                      3
                    2))
             so-long-threshold)))
      (so-long-detected-long-line-p)))

  (setq so-long-threshold 400
        so-long-predicate #'so-long:does-buffer-have-long-lines)
  (delq! 'font-lock-mode so-long-minor-modes)
  (delq! 'display-line-numbers-mode so-long-minor-modes)
  (delq! 'buffer-read-only so-long-variable-overrides 'assq)
  (add-to-list 'so-long-variable-overrides '(font-lock-maximum-decoration . 1))
  (add-to-list 'so-long-variable-overrides '(save-place-alist . nil))
  (add-to-list 'so-long-target-modes 'text-mode)
  (appendq! so-long-minor-modes
            '(flymake-mode
              flycheck-mode
              flyspell-mode
              spell-fu-mode
              jinx-mode
              eldoc-mode
              puni-mode
              highlight-numbers-mode
              better-jumper-local-mode
              ws-butler-mode
              auto-composition-mode
              undo-fu-mode
              undo-tree-mode
              highlight-indent-guides-mode
              hl-fill-column-mode)))

;;;; Pulse
;;;;

;;;###autoload
(defun pulse:momentary-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point) 'region))

;;;###autoload
(defun pulse:momentary (&rest _)
  "Pulse the region or the current line."
  (if (fboundp 'xref-pulse-momentarily)
      (xref-pulse-momentarily)
    (pulse:momentary-line)))

;;;###autoload
(defun pulse:recenter-and-pulse (&rest _)
  "Recenter and pulse the region or the current line."
  (recenter nil)
  (pulse:momentary))

;;;###autoload
(defun pulse:recenter-and-pulse-line (&rest _)
  "Recenter and pulse the current line."
  (recenter nil)
  (pulse:momentary-line))

;;;###autoload
(defun pulse:recenter-top (&rest _)
  "Recenter point at the top of window and pulse the line."
  (interactive)
  (recenter 0)
  (pulse:momentary-line))

(use-package pulse
  :ensure nil
  :demand t
  :hook ((bookmark-after-jump
          magit-diff-visit-file
          next-error) . pulse:recenter-and-pulse-line)
  :custom
  (pulse-flag t)
  :config
  (dolist (command '(scroll-up-command
                     scroll-down-command
                     forward-page
                     backward-page
                     forward-paragraph
                     backward-paragraph
                     recenter-top-bottom
                     other-window
                     other-frame
                     ace-window
                     aw--select-window
                     windmove-left
                     windmove-right
                     windmove-down
                     windmove-up
                     org-next-visible-heading
                     org-previous-visible-heading
                     org-forward-heading-same-level
                     org-backward-heading-same-level
                     outline-backward-same-level
                     outline-forward-same-level
                     outline-next-visible-heading
                     outline-previous-visible-heading
                     outline-up-heading))
    (advice-add command :after #'pulse:momentary-line))

  (dolist (command '(pop-to-mark-command
                     pop-global-mark
                     goto-last-change
                     jump-to-register
                     goto-line
                     bookmark-jump
                     bookmark-jump-other-window
                     bookmark-jump-other-frame
                     better-jumper-jump-forward
                     better-jumper-jump-backward))
    (advice-add command :after #'pulse:recenter-and-pulse))

  ;; From here: https://christiantietze.de/posts/2020/12/emacs-pulse-highlight-yanked-text/
  (defadvice! pulse:yank (orig-fn &rest args)
    "Pulse on yanking text, using ‘pulse-momentary-highlight-region’."
    :around #'yank
    (let (begin end)
      (setq begin (point))
      (apply orig-fn args)
      (setq end (point))
      (pulse-momentary-highlight-region begin end)))

  (defadvice! pulse:kill-ring-save (orig-fn &rest args)
    :around #'kill-ring-save
    (let (begin end)
      (setq begin (region-beginning))
      (apply orig-fn args)
      (setq end (point))
      (pulse-momentary-highlight-region begin end)))

  (set-face-attribute 'pulse-highlight-start-face nil :inherit 'region)
  (set-face-attribute 'pulse-highlight-face nil :inherit 'region))

;;;; Transient
;;;;

(use-package transient
  :custom
  (transient-levels-file (concat my-etc-dir "transient/levels"))
  (transient-values-file (concat my-etc-dir "transient/values"))
  (transient-history-file (concat my-etc-dir "transient/history"))
  (transient-detect-key-conflicts t)
  (transient-force-fixed-pitch t)
  (transient-show-popup t)
  (transient-default-level 5)
  (transient-display-buffer-action '(display-buffer-below-selected)))

;;;; Outline
;;;;

;; Some of this stolen from Oliver Taylor's config:
;; https://github.com/olivertaylor/dotfiles/blob/master/emacs/init.el
;; TODO define outline headings for other modes
(use-package outline
  :ensure nil
  :diminish outline-minor-mode
  :hook (emacs-lisp-mode . outline-minor-mode)
  :config
  (with-eval-after-load 'transient
    (transient-define-prefix outline:transient-dispatch ()
      "Transient for Outline Minor Mode navigation"
      :transient-suffix 'transient--do-stay
      :transient-non-suffix 'transient--do-stay
      [["Show/Hide"
        ("<right>" "Show Subtree" outline-show-subtree)
        ("<left>" "Hide Subtree" outline-hide-subtree)
        ("o" "Hide to This Sublevel" outline-hide-sublevels)
        ("a" "Show All" outline-show-all)]
       ["Navigate"
        ("<down>" "Next" outline-next-visible-heading)
        ("<up>" "Previous" outline-previous-visible-heading)]
       ["Edit"
        ("M-<left>"  "Promote" outline-promote)
        ("M-<right>" "Demote"  outline-demote)
        ("M-<up>"    "Move Up" outline-move-subtree-up)
        ("M-<down>"  "Move Down" outline-move-subtree-down)]
       ["Other"
        ("C-/" "Undo" undo-only)
        ("M-/" "Redo" undo-redo)
        ("c" "Consult" consult-outline :transient nil)]])))

;;;; Better in-buffer search
;;;;

;; TODO add in killing/yank at current matching search
(use-package ctrlf
  :hook (pre-command . ctrlf-mode))

;;;; Scroll on Jump (TODO get this to work?)
;;;; 

;; (use-package scroll-on-jump
;;   :defer t
;;   :config
;;   (scroll-on-jump-advice-add beginning-of-sexp)
;;   (scroll-on-jump-advice-add end-of-sexp)
;;   (scroll-on-jump-advice-add beginning-of-defun)
;;   (scroll-on-jump-advice-add end-of-defun)
;;   (scroll-on-jump-advice-add beginning-of-buffer)
;;   (scroll-on-jump-advice-add end-of-buffer)
;;   (scroll-on-jump-advice-add forward-paragraph)
;;   (scroll-on-jump-advice-add backward-paragraph)
;;   (scroll-on-jump-advice-add bookmark-jump)
;;   (scroll-on-jump-advice-add jump-to-register)
;;   (scroll-on-jump-advice-add ctrlf-forward-alternate)
;;   (scroll-on-jump-advice-add ctrlf-backward-alternate)
;;   (scroll-on-jump-with-scroll-advice-add goto-line)
;;   (scroll-on-jump-with-scroll-advice-add consult-goto-line))

;;;; Line numbers
;;;;

(use-package display-line-numbers
  :ensure nil
  :hook (find-file . global-display-line-numbers-mode)
  :custom
  (display-line-numbers-type 'visual)
  (display-line-numbers-width 2)
  (display-line-numbers-grow-only t)
  :config
  (dolist (mode '(shell-mode-hook
                  eshell-mode-hook
                  vterm-mode-hook
                  treemacs-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode -1)))))

;;;; Highlight Line
;;;;
;;;; I hate this

(use-package hl-line
  :ensure nil
  :disabled t
  :commands hl-line-mode global-hl-line-mode)

;;;; Modeline

(use-package nerd-icons
  :ensure (:wait t)
  :init
  (general-setq inhibit-compacting-font-caches t))

(defun modeline:project-section ()
  "Return the ‘project-name’ of the current project or just return the `default-directory'."
  (if (project-current)
      (project-name (project-current))
    (file-name-nondirectory
     (directory-file-name default-directory))))

(setq-default mode-line-format '((vc-mode vc-mode) "  " (:eval (modeline:project-section)) "  %b%+  " " %n  " " %f : %l"))

(if (and (package-installed-p 'meow)
         (bound-and-true-p meow-global-mode))
    (prependq! mode-line-format '("[" (:eval (meow-indicator)) "] ")))

(use-package hide-mode-line
  :hook (completion-list-mode . hide-mode-line-mode))

(use-package minions
  :config
  (minions-mode 1))

(use-package doom-modeline ; doesn’t make use of my modeline when I think it should.
  ;;:hook (elpaca-after-init . doom-modeline-mode)
  :custom
  (doom-modeline-height 22)
  (doom-modeline-project-detection 'ffip)
  (doom-modeline-buffer-file-name-style 'file-name)
  (doom-modeline-env-version t)
  :config
  (doom-modeline-def-modeline 'my-doom-modeline
                              '(eldoc bar workspace-name window-number modals vcs major-mode minor-modes lsp check)
                              '(buffer-default-directory remote-host buffer-position word-count process time))
  (doom-modeline-set-modeline 'my-doom-modeline t))

;;;; Better moving across windows
;;;;

(use-package windmove
  :ensure nil
  :bind
  (("s-h" . windmove-left)
   ("s-l" . windmove-right)
   ("s-j" . windmove-down)
   ("s-n" . windmove-down)
   ("s-k" . windmove-up)
   ("s-p" . windmove-up)))

;;;; Avy and Ace
;;;;

(use-package avy
  :hook (elpaca-after-init . avy-setup-default)
  :preface
  (defun avy:goto-word-beg ()
    "Jump to beginning of word in line."
    (interactive)
    (avy-with avy-goto-word-0
      (avy-goto-word-0 nil (line-beginning-position) (line-end-position))))

  (defun avy:goto-char-2 (char1 char2)
    "Jump to a place that contain the two specified characters."
    (interactive (list (read-char "1> " t)
                       (read-char "2> " t)))
    (avy-with avy-goto-char-2
      (avy-goto-char-2 char1 char2 nil (line-beginning-position) (line-end-position))))

  (defun avy:goto-lisp-cond ()
    "Jump to a Lisp conditional."
    (interactive)
    (avy--generic-jump "\\s(\\(if\\|cond\\|when\\|unless\\)\\b" nil 'pre))
  :custom
  (avy-all-windows nil)
  (avy-all-windows-alt t)
  (avy-background t)
  (avy-style 'pre)
  (avy-single-candidate-jump nil)
  :config
  (emacs:leader-def
    "g c" #'avy-goto-char
    "g w" #'avy-goto-word-0
    "g l" #'avy-goto-line))

(use-package avy-zap
  :bind (("M-z" . avy-zap-to-char-dwim)
         ("M-Z" . avy-zap-up-to-char-dwim)))

(use-package ace-window
  :bind (("s-w" . ace-window)
         ([remap other-window] . ace-window))
  :custom
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  (aw-scope 'frame)
  (aw-background t))

;;;; Solaire-Mode
;;;;

(use-package solaire-mode
  :demand t
  :hook ((change-major-mode after-revert) . turn-on-solaire-mode)
  :config
                                        ;(solaire-mode-swap-bg)
                                        ;(add-hook 'minibuffer-setup-hook #'solaire-mode-in-minibuffer)
  (add-hook 'focus-in-hook #'solaire-mode-reset)
  (add-hook 'org-capture-mode-hook #'turn-on-solaire-mode)
  (add-hook 'solaire-mode-hook (lambda () (set-window-fringes (minibuffer-window) 0 0 nil))))

;;;; Highlighting
;;;;

(use-package highlight-numbers
  :demand t
  :hook ((prog-mode . highlight-numbers-mode)
         (emacs-lisp-mode . highlight-numbers-mode) ;; <- doesn’t enable here?
         (conf-mode . highlight-numbers-mode))
  :config
  (setq highlight-numbers-generic-regexp "\\_<[[:digit:]]+\\(?:\\.[0-9]*\\)?\\_>"))

(use-package diff-hl
  :hook ((dired-mode . diff-hl-dired-mode-unless-remote)
         (magit-post-refresh . diff-hl-magit-post-refresh))
  :custom-face
  (diff-hl-change ((t (:inherit 'highlight))))
  (diff-hl-delete ((t (:inherit 'error :inverse-video t))))
  (diff-hl-insert ((t (:inherit 'success :inverse-video t))))
  :bind (:map diff-hl-command-map
         ("SPC" . diff-hl-mark-hunk))
  :config
  (diff-hl-margin-mode)
  
  ;; Highlight on-the-fly
  (diff-hl-flydiff-mode +1)

  ;; Set fringe style
  (setq-default fringes-outside-margins t)
  (setq diff-hl-draw-borders nil)
  (if (eq system-type 'darwin) (set-fringe-mode '(4 . 8)))

  (unless (display-graphic-p)
    (setq diff-hl-margin-symbols-alist
          '((insert . " ") (delete . " ") (change . " ")
            (unknown . " ") (ignored . " ")))
    ;; Fall back to the display margin since the fringe is unavailable in tty
    (diff-hl-margin-mode 1)
    ;; Avoid restoring `diff-hl-margin-mode'
    (with-eval-after-load 'desktop
      (add-to-list 'desktop-minor-mode-table
                   '(diff-hl-margin-mode nil)))))

;; (use-package rainbow-mode
;;   :diminish
;;   :bind (:map special-mode-map
;;          ("w" . rainbow-mode))
;;   :hook ((web-mode css-mode scss-mode help-mode org-mode) . rainbow-mode)
;;   :config
;;   (with-no-warnings
;;     ;; HACK: Use overlay instead of text properties to override `hl-line' faces.
;;     ;; @see https://emacs.stackexchange.com/questions/36420
;;     (defun my-rainbow-colorize-match (color &optional match)
;;       (let* ((match (or match 0))
;;              (ov (make-overlay (match-beginning match) (match-end match))))
;;         (overlay-put ov 'ovrainbow t)
;;         (overlay-put ov 'face `((:foreground ,(if (> 0.5 (rainbow-x-color-luminance color))
;;                                                   "white" "black"))
;;                                 (:background ,color)))))
;;     (advice-add #'rainbow-colorize-match :override #'my-rainbow-colorize-match)

;;     (defun rainbow:clear-overlays ()
;;       (remove-overlays (point-min) (point-max) 'ovrainbow t))
;;     (advice-add 'rainbow-turn-off :after #'rainbow:clear-overlays)))

;; TODO might replace this
(use-package rainbow-delimiters
  :demand t
  :hook (prog-mode . rainbow-delimiters-mode)
  :config
  (setq rainbow-delimiters-max-face-count 3))

;;;; Shackle
;;;;

(use-package shackle
  :demand t
  :hook (elpaca-after-init . shackle-mode)
  :preface
  (defvar shackle--popup-window-list nil) ; all popup windows
  (defvar-local shackle--current-popup-window nil) ; current popup window
  (put 'shackle--current-popup-window 'permanent-local t)
  :custom
  (shackle-default-alignment 'below)
  (shackle-default-size 0.28)
  (shackle-rules '(((help-mode helpful-mode)          :select t :align 'below)
                   (comint-mode                       :select t :size 0.4 :align 'below)
                   ("*Completions*"                   :size 0.3 :align 'below)
                   (compilation-mode                  :select t :align 'below)
                   ("*compilation*"                   :select nil :size 0 :align 'below)
                   ("*Error*"                         :select nil :size 0 :align 'below)
                   ("*package update results*"        :size 0.2)
                   ("*Package-Lint*"                  :size 0.4)
                   ("^\\*macro expansion\\**"         :regexp t :size 0.4 :align 'right)
                   ("\\`\\*WoMan .*\\'"               :regexp t :select t :size 0.3 :align 'below)
                   ("* Regexp Explain *"              :size 0.3 :select t :align 'below)
                   ("*RE-Builder*"                    :popup t :select t :size 0.5 :align 'below)
                   ("*Calendar*"                      :select t :size 0.3)
                   ("^\\*Ibuffer\\*$"                 :regexp t :select t :size 0.3 :align 'below)
                   ("^\\*image-dired"                 :regexp t :select t :size 0.8)
                   ("^\\*F\\(?:d\\|ind\\)\\*$"        :regexp t :ignore t)
                   ("^?\\*\\(?:Agenda Com\\|Calendar\\|Org Export Dispatcher\\)"
                    :regexp t :size 0.25 :align 'below)
                   ("^\\*Org \\(?:Select\\|Attach\\)" :regexp t :size 0.3 :align 'below)
                   ("^\\*Org Agenda"                  :ignore t)
                   (("^CAPTURE-.*\\.org$" "EXPORT*")  :regexp t :select t :size 0.3 :align 'below)
                   ("*EKG Capture*"                   :select t :size 0.3 :align 'below)
                   (("*shell*" "*ielm*")
                    :popup t :select t :size 0.3 :align 'below)
                   ("My Eshell" :select t :size 0.5 :align 'right)
                   (("\\`\\*.*-shell\\*\\'" "\\`\\*.*-eshell\\*\\'" "\\`\\*.*-vterm\\*\\'")
                    :regexp t :popup t :select t :size 0.3 :align 'below)
                   ("*Install vterm*"                 :size 0.35 :same t)
                   ("vterm"                           :select t :size 0.5 :align 'right)
                   ("*scheme*"                        :size 0.45 :align 'right)
                   ("^\\*sly-mrepl"                   :regexp t :size 0.45 :align 'right)
                   (("^\\*sly-compilation" "^\\*sly-traces" "^\\*sly-description")
                    :regexp t :size 0.33 :align 'below)
                   (("^\\*sly-\\(?:db\\|inspector\\)") :regexp t :ignore t)
                   ("^\\*geiser messages\\*$"          :regexp t :ignore t)
                   ("^\\*Geiser dbg\\*$"               :regexp t :ignore t)
                   ("^\\*Geiser xref\\*$"              :regexp t)
                   ("^\\*Geiser documentation\\*$"     :regexp t :select t :size 0.35)
                   ("^\\* [A-Za-z0-9_-]+ REPL \\*"     :regexp t)
                   (magit-log-mode                     :same t)
                   (magit-commit-mode                  :ignore t)
                   (magit-diff-mode                    :select nil :size 0.5 :align 'right)
                   (git-commit-mode                    :same t)
                   (vc-annotate-mode                   :same t)
                   ("^\\*git-gutter.+\\*$"             :regexp t :noselect t :size 15)))
  (shackle-select-reused-windows t))

;;;; Treemacs
;;;;

(use-package treemacs
  :init
  (setq treemacs-follow-after-init t
        treemacs-space-between-root-nodes nil
        treemacs-is-never-other-window t
        treemacs-sorting 'alphabetic-case-insensitive
        treemacs-persist-file (concat my-cache-dir "treemacs-persist")
        treemacs-last-error-persist-file (concat my-cache-dir
                                                 "treemacs-last-error-persist"))
  :config
  (treemacs-follow-mode -1))

(use-package treemacs-magit
  :after treemacs magit)

;;;; Dogears
;;;;

(use-package dogears
  :functions dogears-remember
  :general (:keymaps 'global-map
            "M-g d" #'dogears-go
            "M-g b" #'dogears-back
            "M-g f" #'dogears-forward
            "M-g l" #'dogears-list
            "M-g r" #'dogear-remember
            "M-g D" #'dogears-sidebar))

;;;; Workspaces with Tabspaces
;;;;

;; TODO do more with this.
(use-package tabspaces
  :ensure (tabspaces :repo "https://github.com/mclear-tools/tabspaces")
  :hook (elpaca-after-init . tabspaces-mode)
  :commands (tabspaces-switch-or-create-workspace
             tabspaces-open-or-create-project-and-workspace)
  :preface
  (defvar consult--source-workspace
    (list :name     "Workspace Buffers"
          :narrow   ?w
          :history  'buffer-name-history
          :category 'buffer
          :state    #'consult--buffer-state
          :default  t
          :items  (lambda ()
                    (consult--buffer-query
                     :predicate #'tabspaces--local-buffer-p
                     :sort      'visibility
                     :as        #'buffer-name)))
    "‘consult’ source for workspace buffer list for ‘consult-buffer’.")
  :custom
  (tabspaces-use-filtered-buffers-as-default t)
  (tabspaces-default-tab "Main")
  (tabspaces-remove-to-default t)
  (tabspaces-include-buffers '("*scratch*"))
  (tabspaces-session t)
  (tabspaces-session-auto-restore nil)
  (tab-bar-new-tab-choice "*scratch*")
  (tabspaces-session-project-session-store
   (lambda (project-root)
     (expand-file-name
      (concat "sessions/" (file-name-nondirectory project-root) "-tabspaces-session.el")
      project-root)))
  :config
  (with-eval-after-load 'consult
    (add-to-list 'consult-buffer-sources 'consult--soruce-workspace)))

;;;; Themes
;;;;

;;;;; Cleanly load themes
;;;;;
;;;;; Taken from here: https://old.reddit.com/r/emacs/comments/rlli0u/whats_your_favorite_defadvice/

(define-advice load-theme (:before (&rest _args) cleanly-load-themes)
  "Disable all active themes and /then/ load the new theme."
  (mapc #'disable-theme custom-enabled-themes))

;;;;; Theme packages
;;;;;

(use-package doom-themes
  :hook ((elpaca-after-init . (lambda () (load-theme 'doom-nord t)))
         (org-load . doom-themes-org-config)))

;; (use-package nano-theme
;;   ;;:hook (after-init . nano-dark)
;;   ;;:config
;;   ;;(load-theme 'nano-dark t)
;;   )

(provide 'ui-ux)
;;; ui-ux.el ends here
