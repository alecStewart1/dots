;;; lib.el --- Set of variables, constants, functions and macros -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'cl-macs)
(require 'abbrev)

;;; Useful variables and constants
;;;

;;;; Useful predicates
;;;;

;;;###autoload
(defconst emacs27-p    (= emacs-major-version 27))
;;;###autoload
(defconst emacs28-p    (= emacs-major-version 28))
;;;###autoload
(defconst emacs29-p    (= emacs-major-version 29))
;;;###autoload
(defconst emacs30-p    (= emacs-major-version 30))
;;;###autoload
(defconst emacs31-p    (= emacs-major-version 31))
;;;###autoload
(defconst nativecomp-p (if (fboundp 'native-comp-available-p) (native-comp-available-p)))
;;;###autoload
(defconst linux-p      (eq system-type 'gnu/linux))
;;;###autoload
(defconst macos-p      (eq system-type 'darwin))
;;;###autoload
(defconst bsd-p        (eq system-type 'berkeley-unix))
;;;###autoload
(defconst cygwin-p     (eq system-type 'cygwin))
;;;###autoload
(defconst windows-nt-p (memq system-type '(cygwin windows-nt ms-dos)))
;;;###autoload
(defconst linux-distro (if linux-p (substring (shell-command-to-string "cat /etc/os-release | head -n 1 | cut -f2 -d'='") 0 -1)))
;;;###autoload
(defconst gentoo-p     (string= "Gentoo" linux-distro))

;; For whatever reason, on MacOS, Emacs doesn’t set the path correctly
;; TODO investigate this
(when macos-p
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/opt/node@18/bin")))

;;;; For fonts
;;;;

;;;###autoload
(defconst my-default-font (font-spec :family "IBM Plex Mono"
                                     :size (if macos-p 12 14)))
;;;###autoload
(defconst my-default-variable-font (font-spec :family "IBM Plex Sans"
                                              :size (if macos-p 12 14)))
;;;###autoload
(defconst my-default-serif-font (font-spec :family "IBM Plex Serif"
                                           :size (if macos-p 12 14)))

;;;; Directory stuff to keep ‘user-emacs-directory’ clean
;;;; as well as other things.
;;;;

(when (and windows-nt-p (null (getenv "HOME")))
  (setenv "HOME" (getenv "USERPROFILE")))

;;;###autoload
(defconst my-dotfiles-dir (eval-when-compile (file-truename "~/.dotfiles/"))
  "The location of the .dotfiles directory where I keep my configuration files.
This is for using with stow.")

;;;###autoload
(defconst my-emacs-dir (concat my-dotfiles-dir "emacs/.emacs.d/")
  "The locaction of the directory in .dotfiles that holds my emacs configuration.
This is for using with stow.")

;;;###autoload
(defconst sys-emacs-dir (eval-when-compile (expand-file-name user-emacs-directory))
  "The location of the emacs directory on the system, not what’s in the  .dotfiles directory.")

;;;###autoload
(defconst my-local-dir (concat sys-emacs-dir ".local/")
  "The parent directory to contain a lot of excess files created by emacs or other packages.")

;;;###autoload
(defconst my-etc-dir (concat (expand-file-name my-local-dir) "etc/")
  "The directory to contain generated files that don’t change all that often.")

;;;###autoload
(defconst my-cache-dir (concat my-local-dir "cache/")
  "The directory to contain generated files that change frequently.")

;;;###autoload
(defconst my-personal-blog-dir (expand-file-name "~/Documents/Org/Blog/alec-stewart/")
  "Directory of where my personal blog is to point to for ‘ox-publish’.")

;;; Utilties and a lot of stuff from Doom Emacs

;;;; Functions
;;;;

(defun unquote (exp)
  "Return EXP unquoted."
  (declare (pure t) (side-effect-free t))
  (while (memq (car-safe exp) '(quote function))
    (setq exp (cadr exp)))
  exp)

(defun enlist (exp)
  "Return EXP wrapped in a list, or as-is if already a list."
  (declare (pure t) (side-effect-free t))
  (if (listp exp) exp (list exp)))

(defun rpartial (fn &rest args)
  "Return a partial application of FUN to right-hand ARGS.

ARGS is a list of the last N arguments to pass to FUN. The result is a new
function which does the same as FUN, except that the last N arguments are fixed
at the values with which this function was called."
  (declare (side-effect-free t))
  (lambda (&rest pre-args)
    (apply fn (append pre-args args))))

;;;###autoload
(defun simple-call-process (command &rest args)
  "From Doom Emacs.
Execute COMMAND with ARGS synchronously.
Returns (STATUS . OUTPUT) when it is done, where STATUS is the returned error
code of the process and OUTPUT is its stdout output."
  (with-temp-buffer
    (cons (or (apply #'call-process command nil t nil (remq nil args))
              -1)
          (string-trim (buffer-string)))))

;;;; Buffer stuff
;;;;

;;;###autoload
(defun get-buffer-mode (buf)
  "Get `major-mode' of BUF."
  (with-current-buffer buf
    major-mode))

;;;###autoload
(defun buffers-in-mode (modes &optional buffer-list derived-p)
  "Return a list of buffers whose `major-mode' is `eq' to MODE(S).

If DERIVED-P, test with `derived-mode-p', otherwise use `eq'."
  (let ((modes (enlist modes)))
    (cl-remove-if-not (if derived-p
                          (lambda (buf)
                            (with-current-buffer buf
                              (apply #'derived-mode-p modes)))
                        (lambda (buf)
                          (memq (buffer-local-value 'major-mode buf) modes)))
                      (buffer-list))))

(defun visible-buffer-p (buf)
  "Return non-nil if BUF is visible"
  (get-buffer-window buf))

;;;###autoload
(defun visible-buffers (&optional buffer-list all-frames)
  "Return a list of visible buffers (i.e. not buried)."
  (let ((buffers (delete-dups
                  (cl-loop for frame in (if all-frames (visible-frame-list) (list (selected-frame)))
                           if (window-list frame)
                           nconc (mapcar #'window-buffer it)))))
    (if buffer-list
        (cl-loop for buf in buffers
                 unless (memq buf buffer-list)
                 collect buffers)
      buffers)))

;;;###autoload
(defun get-buried-buffers (&optional buffer-list)
  "Get a list of buffers that are buried."
  (cl-loop for buf in (or buffer-list (buffer-list))
           unless (visible-buffer-p buf)
           collect buf))

;;;###autoload
(defun get-matching-buffers (pattern &optional buffer-list)
  "Get a list of all buffers that match the regex PATTERN."
  (cl-loop for buf in (or buffer-list (buffer-list))
           if (string-match-p pattern (buffer-name buf))
           collect buf))

;;;###autoload
(defvar fallback-buffer-name "*scratch*"
  "The name of the buffer to fall back to if no other buffers exist (will create
it if it doesn't exist).")

;;;###autoload
(defun fallback-buffer ()
  "Returns the fallback buffer, creating it if necessary. By default this is the
scratch buffer. See `fallback-buffer-name' to change this."
  (let (buffer-list-update-hook)
    (get-buffer-create fallback-buffer-name)))

;;;###autoload
(defun my-kill-matching-buffers (pattern &optional buffer-list)
  "From Doom Emacs, and is different from the existing Emacs ‘kill-matching-buffers’.
Kill all buffers (in current workspace *or* in BUFFER-LIST) that match the regex PATTERN.
Returns the number of killed buffers."
  (let (buffers (get-matching-buffers pattern buffer-list))
    (dolist (buf buffers (length buffers))
      (kill-buffer buf))))

;; From: https://github.com/angrybacon/dotemacs/blob/master/lisp/widowmaker/widowmaker.el#L48
;;;###autoload
(defun kill-buffer-and-process (&optional buffer-or-name)
  "Kill BUFFER-OR-NAME with no confirmation."
  (interactive)
  (let ((kill-buffer-query-functions (remq 'process-kill-buffer-query-function
                                           kill-buffer-query-functions)))
    (kill-buffer (or buffer-or-name (current-buffer)))))

;;;; File stuff
;;;;

;;;###autoload
(defun resolve-file-path-forms (spec &optional directory)
  "Converts a simple nested series of or/and forms into a series of
`file-exists-p' checks.
For example
  (resolve-file-path-forms
    '(or A (and B C))
    \"~\")
Returns (approximately):
  '(let* ((_directory \"~\")
          (A (expand-file-name A _directory))
          (B (expand-file-name B _directory))
          (C (expand-file-name C _directory)))
     (or (and (file-exists-p A) A)
         (and (if (file-exists-p B) B)
              (if (file-exists-p C) C))))
This is used by `file-exists-p!' and `project-file-exists-p!'."
  (declare (pure t) (side-effect-free t))
  (if (and (listp spec)
           (memq (car spec) '(or and)))
      (cons (car spec)
            (mapcar (rpartial #'resolve-file-path-forms directory)
                    (cdr spec)))
    (let ((filevar (make-symbol "file")))
      `(let ((,filevar ,spec))
         (and (stringp ,filevar)
              ,(if directory
                   `(let ((default-directory ,directory))
                      (file-exists-p ,filevar))
                 `(file-exists-p ,filevar))
              ,filevar)))))


;;;###autoload
(defmacro file-exists-p! (files &optional directory)
  "From Doom Emacs.
Returns non-nil if the FILES in DIRECTORY all exist.

DIRECTORY is a path; defaults to `default-directory'.

Returns the last file found to meet the rules set by FILES, which can be a
single file or nested compound statement of `and' and `or' statements."
  `(let ((p ,(resolve-file-path-forms files directory)))
     (and p (expand-file-name p ,directory))))


;;;###autoload
(defmacro file! ()
  "From Doom Emacs.
Return the file of the file this macro was called."
  (or
   ;; REVIEW: Use `macroexp-file-name' once 27 support is dropped.
   (let ((file (car (last current-load-list))))
     (if (stringp file) file))
   (bound-and-true-p byte-compile-current-file)
   load-file-name
   buffer-file-name   ; for `eval'
   (error "file!: cannot deduce the current file path")))

;;;###autoload
(defmacro dir! ()
  "From Doom Emacs.
Return the directory of the file this macro was called."
  (let (file-name-handler-alist)
    (file-name-directory (macroexpand '(file!)))))

;;; Editing
;;;

;;;; Useful advice and functions
;;;;
;;;; Some pulled from this thread:
;;;; https://old.reddit.com/r/emacs/comments/rlli0u/whats_your_favorite_defadvice/
;;;; Some from this package:
;;;; https://github.com/bbatsov/crux

;; TODO these don’t quiet work the way I’d like them to
;; (define-advice kill-ring-save (:before (&rest _) copy-line-dwim)
;;   "Copy single line when there is no region active."
;;   (interactive
;;    (if mark-active (list (region-beginning) (region-end))
;;      (message "Single line copied")
;;      (list (line-beginning-position)
;;            (line-beginning-position 2)))))

;; (define-advice kill-region (:before (&rest _) kill-line-dwin)
;;   "Kill single line when there is no region active."
;;   (interactive
;;    (if mark-active (list (region-beginning) (region-end))
;;      (list (line-beginning-position)
;;            (line-beginning-position 2)))))

(defadvice backward-kill-word (around delete-pair activate)
  (if (eq (char-syntax (char-before)) ?\()
      (progn
        (backward-char 1)
        (save-excursion
          (forward-sexp 1)
          (delete-char -1))
        (forward-char 1)
        (append-next-kill)
        (kill-backward-chars 1))
    ad-do-it))

(defun thing-at-point-or-region (&optional thing prompt)
  "From Doom Emacs (a stripped down version):
Grab the current selection, THING at point, or xref identifier at point.

Returns THING if it is a string. Otherwise, if nothing is found at point and
PROMPT is non-nil, prompt for a string (if PROMPT is a string it'll be used as
the prompting string). Returns nil if all else fails.

NOTE: Don't use THING for grabbing symbol-at-point. The xref fallback is smarter
in some cases."
  (declare (side-effect-free t))
  (cond ((stringp thing)
         thing)
        ((use-region-p)
         (buffer-substring-no-properties
          (region-beginning)
          (region-end)))
        (thing
         (thing-at-point thing t))
        ((require 'xref nil t)
         (xref-backend-identifier-at-point (xref-find-backend)))
        (prompt
         (read-string (if (stringp prompt) prompt "")))))

;;;###autoload
(defvar line-start-regex-alist
  '((org-mode . "^\\(\*\\|[[:space:]]*\\)* ")
    (default . "^[[:space:]]*")))

;;;###autoload
(defun move-to-mode-line-start ()
  "Move to the beginning, skipping mode specific line start regex."
  (interactive)
  (if line-move-visual
      (beginning-of-visual-line nil)
    (move-beginning-of-line nil))
  (let ((line-start-regex (cdr (seq-find
                                (lambda (e) (derived-mode-p (car e)))
                                line-start-regex-alist
                                (assoc 'default line-start-regex-alist)))))
    (search-forward-regexp line-start-regex (line-end-position) t)))

;;;###autoload
(defun smart-open-line-above ()
  "Insert an empty line above the current line.
Position the cursor at its beginning, according to the current mode."
  (interactive)
  (move-beginning-of-line nil)
  (insert "\n")
  (if electric-indent-inhibit
      ;; We can't use `indent-according-to-mode' in languages like Python,
      ;; as there are multiple possible indentations with different meanings.
      (let* ((indent-end (progn (move-to-mode-line-start) (point)))
             (indent-start (progn (move-beginning-of-line nil) (point)))
             (indent-chars (buffer-substring indent-start indent-end)))
        (forward-line -1)
        ;; This new line should be indented with the same characters as
        ;; the current line.
        (insert indent-chars))
    ;; Just use the current major-mode's indent facility.
    (forward-line -1)
    (indent-according-to-mode)))

;;;###autoload
(defun smart-open-line (arg)
  "Insert an empty line after the current line.
Position the cursor at its beginning, according to the current mode.
With a prefix ARG open line above the current line."
  (interactive "P")
  (if arg
      (smart-open-line-above)
    (move-end-of-line nil)
    (newline-and-indent)))

;;; Other utilities
;;;

;;;###autoload
(defun first-day-of-week ()
  "Get the first day of the week in seconds."
  (let* ((time (current-time))
         (datetime (decode-time time))
         (dow (nth 1 datetime)))
    (time-subtract time (days-to-time 6))))

;;;###autoload
(defun last-day-of-week ()
  "Get the last day of the week in seconds."
  (let* ((now (current-time))
         (datetime (decode-time now))
         (dow (nth 6 datetime)))
    (time-add now (days-to-time (- 7 dow)))))

;;; Stuff for certain packages that a lot of configurations need
;;;

;;;; TODO Project Mode
;;;;

(defun project:file-exists-p (file)
  "TODO"
  (let ((proot (project-root (project-current))))
    (file-exists-p (concat proot file))))

(cl-defmacro def-project-mode (name
                               &optional
                               docstring
                               lighter
                               &key
                               modes
                               files
                               match
                               when
                               add-hooks
                               on-load
                               on-enter
                               on-exit)
  "Mostly from Doom Emacs:
A macro to define a minor-mode (’sub-mode’) for whatever we like. This could be
for specific file/directory types, framework or whatever.

Creates minor-mode NAME-mode with a NAME-keymap.

NAME - a valid Emacs lisp symbol name for the new minor mode.

DOCSTRING - optional docstring for the new minor mode.

LIGHTER - optional lighter for ‘define-minor-mode’; a string to show in the modeline.

The follow keys can be used in order to tell when the minor mode should start:

  :modes LIST - a list containing one or many modes that a buffer can be derived from.

  :files LIST - a list of files.

  :match STRING - a regexp to match on a file.

  :when FORM - a condition that returns non-nil.

Only one of the keys above truly needs to be specified, but all can be used as well.

The follow keys are for additional configurations for the new minor mode:

  :add-hooks LIST - a list of hooks to add this mode's hook.

  :on-load FORM - a Lisp form to run when the minor mode is first loaded.

  :on-enter FORM - a Lisp form to run each time the mode is activated.

  :on-exit FORM - a Lisp form to run each time the mode is disabled."
  (declare (indent 1)
           (doc-string 2))
  (let ((init-var (intern (format "%s-init" name))))
    (macroexp-progn
     (append
      (when on-load
        `((defvar ,init-var nil)))
      `((define-minor-mode ,name
          ,(or docstring
               "A project minor-mode generated by ‘def-project-mode’")
          :init-value nil
          :lighter ,(or lighter "")
          :keymap (make-sparse-keymap)
          (if (not ,name)
              ,on-exit
            ,(when on-load
               `(unless ,init-var
                  ,on-load
                  (setq ,init-var t)))
            ,on-enter))
        (dolist (hook ,add-hooks)
          (add-hook ',(intern (format "%s-hook" name)) hook))

        ,(when files
           `(def-project-type ,(intern (split-string name "-mode"))
              :files ,files))

        (define-abbrev-table ',(intern (format "%s-abbrev-table" name)) '()
          ,(format "An abbrev table for %s" name)
          :system t
          :case-fixed t))

      (cond ((or files modes when)
             (cl-check-type files (or null list string))
             (let ((fn
                    `(lambda ()
                       (and (not (bound-and-true-p ,name))
                            (and buffer-file-name (not (file-remote-p buffer-file-name nil t)))
                            ,(or (null match)
                                 `(if buffer-file-name (string-match-p ,match buffer-file-name)))
                            ,(or (null files)
                                 `(if (stringp (car (files)))
                                      (cl-every #'project:file-exists-p files)
                                    (project:file-exists-p files)))
                            ,(or when t)
                            (,name 1)))))
               (if modes
                   `((dolist (mode ,modes)
                       (let ((hook-name
                              (intern (format "%s::enable-hook" ',name
                                              (if (eq mode t) "" (format "-in-%s" mode))))))
                         (fset hook-name #',fn)
                         (if (eq mode t)
                             (add-to-list 'auto-minor-mode-magic-alist (cons hook-name #',name))
                           (add-hook (intern (format "%s-hook" mode)) hook-name)))))
                 `((add-hook 'change-major-mode-after-body-hook #',fn)))))
            (match
             `((add-to-list 'auto-minor-mode-alist (cons ,match #',name)))))))))

;;; Generic Macros
;;;

(defvar transient-counter 0)

;;;###autoload
(defmacro add-transient-hook! (hook-or-function &rest forms)
  "From Doom Emacs.
Attaches a self-removing function to HOOK-OR-FUNCTION.
FORMS are evaluated once, when that function/hook is first invoked, then never
again.
HOOK-OR-FUNCTION can be a quoted hook or a sharp-quoted function (which will be
advised)."
  (declare (indent 1))
  (let ((append (if (eq (car forms) :after) (pop forms)))
        ;; Avoid `make-symbol' and `gensym' here because an interned symbol is
        ;; easier to debug in backtraces (and is visible to `describe-function')
        (fn (intern (format "transient-%d-h" (cl-incf transient-counter)))))
    `(let ((sym ,hook-or-function))
       (defun ,fn (&rest _)
         ,(format "Transient hook for %S" (unquote hook-or-function))
         ,@forms
         (let ((sym ,hook-or-function))
           (cond ((functionp sym) (advice-remove sym #',fn))
                 ((symbolp sym)   (remove-hook sym #',fn))))
         (unintern ',fn nil))
       (cond ((functionp sym)
              (advice-add ,hook-or-function ,(if append :after :before) #',fn))
             ((symbolp sym)
              (put ',fn 'permanent-local-hook t)
              (add-hook sym #',fn ,append))))))

;;;###autoload
(defmacro add-hook-trigger! (hook-var &rest targets)
  "TODO"
  `(let ((fn (intern (format "%s-h" ,hook-var))))
     (fset fn (lambda (&rest _) (run-hooks ,hook-var) (set ,hook-var nil)))
     (put ,hook-var 'permanent-local t)
     (dolist (on (list ,@targets))
       (if (functionp on)
           (advice-add on :before fn)
         (add-hook on fn)))))

(defun resolve-hook-forms (hooks)
  (declare (pure t) (side-effect-free t))
  (let ((hook-list (enlist (unquote hooks))))
    (if (eq (car-safe hooks) 'quote)
        hook-list
      (cl-loop for hook in hook-list
               if (eq (car-safe hook) 'quote)
               collect (cadr hook)
               else collect (intern (format "%s-hook" (symbol-name hook)))))))

;;;###autoload
(defmacro add-hook! (hooks &rest rest)
  "From Doom Emacs.

A convenience macro for adding N functions to M hooks.

This macro accepts, in order:

  1. The mode(s) or hook(s) to add to. This is either an unquoted mode, an
     unquoted list of modes, a quoted hook variable or a quoted list of hook
     variables.
  2. Optional properties :local, :append, and/or :depth [N], which will make the
     hook buffer-local or append to the list of hooks (respectively),
  3. The function(s) to be added: this can be one function, a quoted list
     thereof, a list of `defun's, or body forms (implicitly wrapped in a
     lambda).

\(fn HOOKS [:append :local] FUNCTIONS)"
  (declare (indent (lambda (indent-point state)
                     (goto-char indent-point)
                     (when (looking-at-p "\\s-*(")
                       (lisp-indent-defform state indent-point))))
           (debug t))
  (let* ((hook-forms (resolve-hook-forms hooks))
         (func-forms ())
         (defn-forms ())
         append-p
         local-p
         remove-p
         depth
         forms)
    (while (keywordp (car rest))
      (pcase (pop rest)
        (:append (setq append-p t))
        (:depth  (setq depth (pop rest)))
        (:local  (setq local-p t))
        (:remove (setq remove-p t))))
    (let ((first (car-safe (car rest))))
      (cond ((null first)
             (setq func-forms rest))

            ((eq first 'defun)
             (setq func-forms (mapcar #'cadr rest)
                   defn-forms rest))

            ((memq first '(quote function))
             (setq func-forms
                   (if (cdr rest)
                       (mapcar #'unquote rest)
                     (enlist (unquote (car rest))))))

            ((setq func-forms (list `(lambda (&rest _) ,@rest)))))
      (dolist (hook hook-forms)
        (dolist (func func-forms)
          (push (if remove-p
                    `(remove-hook ',hook #',func ,local-p)
                  `(add-hook ',hook #',func ,(or depth append-p) ,local-p))
                forms)))
      (macroexp-progn
       (append defn-forms
               (if append-p
                   (nreverse forms)
                 forms))))))

;;;###autoload
(defmacro letf! (bindings &rest body)
  "From Doom Emacs
Temporarily rebind function and macros in BODY.
BINDINGS is either a) a list of, or a single, `defun' or `defmacro'-ish form, or
b) a list of (PLACE VALUE) bindings as `cl-letf*' would accept.
TYPE is either `defun' or `defmacro'. NAME is the name of the function. If an
original definition for NAME exists, it can be accessed as a lexical variable by
the same name, for use with `funcall' or `apply'. ARGLIST and BODY are as in
`defun'.
\(fn ((TYPE NAME ARGLIST &rest BODY) ...) BODY...)"
  (declare (indent defun))
  (setq body (macroexp-progn body))
  (when (memq (car bindings) '(defun defmacro))
    (setq bindings (list bindings)))
  (dolist (binding (reverse bindings) (macroexpand body))
    (let ((type (car binding))
          (rest (cdr binding)))
      (setq
       body (pcase type
              (`defmacro `(cl-macrolet ((,@rest)) ,body))
              (`defun `(cl-letf* ((,(car rest) (symbol-function #',(car rest)))
                                  ((symbol-function #',(car rest))
                                   (lambda ,(cadr rest) ,@(cddr rest))))
                         (ignore ,(car rest))
                         ,body))
              (_
               (when (eq (car-safe type) 'function)
                 (setq type (list 'symbol-function type)))
               (list 'cl-letf (list (cons type rest)) body)))))))

;;;###autoload
(defmacro appendq! (sym &rest lists)
  "Append LISTS to SYM in place."
  `(setq ,sym (append ,sym ,@lists)))

;;;###autoload
(defmacro setq! (&rest settings)
  "A stripped-down `customize-set-variable' with the syntax of `setq'.
This can be used as a drop-in replacement for `setq'. Particularly when you know
a variable has a custom setter (a :set property in its `defcustom' declaration).
This triggers setters. `setq' does not."
  (macroexp-progn
   (cl-loop for (var val) on settings by 'cddr
            collect `(funcall (or (get ',var 'custom-set) #'set)
                              ',var ,val))))

;;;###autoload
(defmacro delq! (elt list &optional fetcher)
  "`delq' ELT from LIST in-place.
If FETCHER is a function, ELT is used as the key in LIST (an alist)."
  `(setq ,list
         (delq ,(if fetcher
                    `(funcall ,fetcher ,elt ,list)
                  elt)
               ,list)))

;;;###autoload
(defmacro pushnew! (place &rest values)
  "Push VALUES sequentially into PLACE, if they aren't already present.
This is a variadic `cl-pushnew'."
  (let ((var (make-symbol "result")))
    `(dolist (,var (list ,@values) (with-no-warnings ,place))
       (cl-pushnew ,var ,place :test #'equal))))

;;;###autoload
(defmacro prependq! (sym &rest lists)
  "Prepend LISTS to SYM in place."
  `(setq ,sym (append ,@lists ,sym)))

;;;###autoload
(defmacro defadvice! (symbol arglist &optional docstring &rest body)
  "From Doom Emacs.

Define an advice called SYMBOL and add it to PLACES.

ARGLIST is as in `defun'. WHERE is a keyword as passed to `advice-add', and
PLACE is the function to which to add the advice, like in `advice-add'.
DOCSTRING and BODY are as in `defun'.

\(fn SYMBOL ARGLIST &optional DOCSTRING &rest [WHERE PLACES...] BODY\)"
  (declare (doc-string 3) (indent defun))
  (unless (stringp docstring)
    (push docstring body)
    (setq docstring nil))
  (let (where-alist)
    (while (keywordp (car body))
      (push `(cons ,(pop body) (enlist ,(pop body)))
            where-alist))
    `(progn
       (defun ,symbol ,arglist ,docstring ,@body)
       (dolist (targets (list ,@(nreverse where-alist)))
         (dolist (target (cdr targets))
           (advice-add target (car targets) #',symbol))))))

;;;###autoload
(defun lookup-key! (keys &rest keymaps)
  "From Doom Emacs: Like `lookup-key', but search active keymaps if KEYMAP is omitted."
  (if keymaps
      (cl-some (rpartial #'lookup-key keys) keymaps)
    (cl-loop for keymap
             in (append (cl-loop for alist in emulation-mode-map-alists
                                 append (mapcar #'cdr
                                                (if (symbolp alist)
                                                    (if (boundp alist) (symbol-value alist))
                                                  alist)))
                        (list (current-local-map))
                        (mapcar #'cdr minor-mode-overriding-map-alist)
                        (mapcar #'cdr minor-mode-map-alist)
                        (list (current-global-map)))
             if (keymapp keymap)
             if (lookup-key keymap keys)
             return it)))

;;;###autoload
(defmacro cmd! (&rest body)
  "From Doom Emacs: Returns (lambda () (interactive) ,@body)
A factory for quickly producing interaction commands, particularly
for keybinds or aliases."
  (declare (doc-string 1) (pure t) (side-effect-free t))
  `(lambda (&rest _) (interactive) ,@body))

;;;###autoload
(defmacro defer-feature! (feature &rest fns)
  "From Doom Emacs.

Pretend FEATURE hasn't been loaded yet, until FEATURE-hook or FN runs.
Some packages (like `elisp-mode' and `lisp-mode') are loaded immediately at
startup, which will prematurely trigger `after!' (and `with-eval-after-load')
blocks. To get around this we make Emacs believe FEATURE hasn't been loaded yet,
then wait until FEATURE-hook (or MODE-hook, if FN is provided) is triggered to
reverse this and trigger `after!' blocks at a more reasonable time."
  (let ((advice-fn (intern (format "emacs--defer-feature-%s-a" feature)))
        (fns (or fns (list feature))))
    `(progn
       (delq! ',feature features)
       (defadvice! ,advice-fn (&rest _)
         :before ',fns
         ;; Some plugins (like yasnippet) will invoke a fn early to parse
         ;; code, which would prematurely trigger this. In those cases, well
         ;; behaved plugins will use `delay-mode-hooks', which we can check for:
         (unless delay-mode-hooks
           ;; ...Otherwise, announce to the world this package has been loaded,
           ;; so `after!' handlers can react.
           (provide ',feature)
           (dolist (fn ',fns)
             (advice-remove fn #',advice-fn)))))))

;;;###autoload
(defmacro shut-up! (&rest forms)
  "From Doom Emacs’s ‘quiet!’.

Runs FORMS without generating any output.

Emacs likes to tell you _everything_. Tell it not to sometimes.
This won’t prevent the *Messages* buffer from being wrote to, just in the echo area."
  `(unless noninteractive
     (let ((inhibit-message t)
           (save-silently t))
       (prog1 ,@forms (message "")))
     (letf! ((standard-output (lambda (&rest _)))
             (defun message (&rest _))
             (defun load (file &optional noerror nomessage nosuffix must-suffix)
               (funcall load file noerror t nosuffix must-suffix))
             (defun write-region (start end filename &optional append visit lockname mustbenew)
               (unless visit (setq visit 'no-message))
               (funcall write-region start end filename append visit lockname mustbenew)))
       ,@forms)))

(provide 'lib)
;;; lib.el ends here
