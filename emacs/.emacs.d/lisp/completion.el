;;; completion.el --- Completion in Emacs with Vertico, Consult, Marginalia, and Company with Orderless -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;  While I myself am not a minialist, I do appreciate the simplicity yet also featureful-ness of
;;
;;  - Vertico
;;  - Consult
;;  - Marginalia
;;  - Corfu
;;  - Cape
;;
;;  Interesting packages that use the built-in functionalities of Emacs, as opposed to
;;  creating a whole API.
;;
;;; Code:

(require 'lib)
(require 'mode-local)
(require 'thingatpt)
(require 'general)

;;; Packages
;;;

;;;; Orderless
;;;;

(use-package orderless
  :ensure (:wait t)
  :preface
  (defun orderless:fast-dispatch (word index total)
    "Dispatch orderless on a WORD where INDEX is 0 and TOTAL is at least 1 character.
Potentially speeds up completion a little bit."
    (and (= index 0) (= total 1) (length< word 1)
         `(orderless-regexp . ,(concat "^" (regexp-quote word)))))
  :config
  (orderless-define-completion-style orderless-fast
                                     (orderless-style-dispatchers '(orderless:fast-dispatch))
                                     (orderless-matching-styles '(orderless-literal orderless-initialism orderless-prefixes)))
  (setq orderless-component-separator #'orderless-escapable-split-on-space
        completion-styles '(orderless-fast basic)
        completion-category-defaults nil
        completion-category-overrides
        '((file (styles . (orderless-fast partial-completion)))
          (buffer (styles . (orderless-fast partial-completion)))
          (info-menu (styles . (orderless-fast partial-completion)))))
  (set-face-attribute 'completions-first-difference nil :inherit nil))

;;;; Minibuffer Completion
;;;;

(use-package minibuffer
  :ensure nil
  :config
  (unless (package-installed-p 'orderless)
    (setq completion-styles '(basic substring flex partial-completion)))
  (file-name-shadow-mode 1)
  (minibuffer-depth-indicate-mode 1)
  (minibuffer-electric-default-mode 1)
  :custom
  (tab-always-indent 'complete)
  (completions-format 'vertical)
  (completion-cycle-threshold 3)
  (completion-flex-nospace nil)
  (completion-ignore-case t)
  (completions-detailed t)
  (completion-pcm-complete-word-inserts-delimiters t)
  (read-buffer-completion-ignore-case t)
  (read-file-name-completion-ignore-case t)
  (resize-mini-windows 'grow-only))

;;;;; Vertico
;;;;;

;;;###autoload
(defun vertico:crm-select ()
  "Enter candidate in `consult-completing-read-multiple'"
  (interactive)
  (let ((idx vertico--index))
    (unless (get-text-property 0 'consult--crm-selected (nth vertico--index vertico--candidates))
      (setq idx (1+ idx)))
    (run-at-time 0 nil (lambda ()
                         (interactive)
                         (vertico--goto idx) (vertico--exhibit))))
  (vertico-exit))

;;;###autoload
(defun vertico:crm-exit ()
  "Enter candidate in `consult-completing-read-multiple'"
  (interactive)
  (run-at-time 0 nil #'vertico-exit)
  (vertico-exit))

;;;###autoload
(defun vertico:search-symbol-at-point ()
  "Consult the lines that have the symbol at point."
  (interactive)
  (consult-line (thing-at-point 'symbol)))

(defvar vertico:find-file-in--history nil)
;;;###autoload
(defun vertico:find-file-in (&optional dir initial)
  "Jump to file under DIR (recursive).
If INITIAL is non-nil, use as initial input."
  (interactive)
  (require 'consult)
  (let* ((default-directory (or dir default-directory))
         (prompt-dir (consult--directory-prompt "Find" default-directory))
         (cmd (split-string-and-unquote consult-find-args " ")))
    (find-file
     (consult--read
      (split-string (cdr (apply #'simple-call-process cmd)) "\n" t)
      :prompt default-directory
      :sort nil
      :initial (if initial (shell-quote-argument initial))
      :add-history (thing-at-point 'filename)
      :category 'file
      :history '(:input vertico:find-file-in--history)))))

(defun vertico:find-file-in-emacsd ()
  "Find a file in ‘user-emacs-directory’."
  (interactive)
  (vertico:find-file-in my-emacs-dir))

(defun vertico:find-file-in-org-dir ()
  "Find a file in ‘org-directory’."
  (interactive)
  (vertico:find-file-in org-directory))

(defun vertico:find-file-emacs-finance ()
  "Find a file in ‘~/Documents/Finance’"
  (interactive)
  (vertico:find-file-in (expand-file-name "~/Documents/Finance")))

(defun vertico:find-file-in-project-root ()
  "Find a file in the current project’s root."
  (interactive)
  (vertico:find-file-in (project-root (project-current))))

;;;###autoload
(defun vertico:embark-magit-status (file)
  "From Doom Emacs.
Run ‘magit-status’ on repo containing the embark target."
  (interactive "GFile: ")
  (magit-status (locate-dominating-file file ".git")))

(use-package vertico
  :hook ((elpaca-after-init . vertico-mode)
         (vertico-mode . vertico-indexed-mode))
  :functions vertico--exhibit
  :init
  ;; these can be annoying so we disable the help at startup
  (advice-add #'vertico--setup :after
    (lambda (&rest _)
      (setq-local completion-auto-help nil
                  completion-show-inline-help nil)))
  :custom
  (vertico-resize nil)
  (vertico-count 16)
  (vertico-cycle t)
  :config
  ;; Hooks
  (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
  (advice-add #'tmm-add-prompt :after #'minibuffer-hide-completions)
  (defadvice! vertico:suppress-completion-help (fn &rest args)
    :around #'ffap-menu-ask
    (letf! ((#'minibuffer-completion-help #'ignore))
      (apply fn args)))
  ;; Keys
  (define-key vertico-map (kbd "C-n") #'vertico-next)
  (define-key vertico-map (kbd "C-M-n") #'vertico-next-group)
  (define-key vertico-map (kbd "C-p") #'vertico-previous)
  (define-key vertico-map (kbd "C-M-p") #'vertico-previous-group)
  (define-key vertico-map (kbd "DEL") #'vertico-directory-delete-char)
  (define-key vertico-map (kbd "<backspace>") #'vertico-directory-delete-char)
  (define-key vertico-map (kbd "RET") #'vertico-directory-enter)
  (define-key vertico-map (kbd "<return>") #'vertico-directory-enter)
  (define-key vertico-map (kbd "?") #'minibuffer-completion-help)
  (define-key vertico-map (kbd "M-RET") #'vertico-exit-input)
  (define-key vertico-map (kbd "M-TAB") #'minibuffer-complete)
  (define-key vertico-map (kbd "<M-return>") #'minibuffer-force-complete-and-exist)
  (define-key vertico-map (kbd "<M-tab>") #'minibuffer-complete))

;;;;; Consult
;;;;;

(use-package consult
  :ensure (:wait t)
  :general
  ;; Remappings
  ([remap switch-to-buffer]              #'consult-buffer)
  ([remap switch-to-buffer-other-window] #'consult-buffer-other-window)
  ([remap switch-to-buffer-other-frame]  #'consult-buffer-other-frame)
  ([remap project-switch-to-buffer]      #'consult-project-buffer)
  ([remap bookmark-jump]                 #'consult-bookmark)
  ([remap recentf-open-files]            #'consult-recent-file)
  ([remap yank-pop]                      #'consult-yank-pop)
  ([remap locate]                        #'consult-locate)
  ([remap repeat-complex-command]        #'consult-complex-command)
  ([remap man]                           #'consult-man)
  ([remap imenu]                         #'consult-imenu)
  ([remap goto-line]                     #'consult-goto-line)
  ;; Editing
  ("C-x C-k C-r" #'consult-kmacro)
  ;; Register
  ("C-x r C-r" #'consult-register)
  ("C-x r C-s" #'consult-register-store)
  ;; Histories
  ("C-c h" #'consult-history)
  ;; Isearch
  (:keymaps 'consult-crm-map
   "TAB"                       #'vertico:crm-select
   "RET"                       #'vertico:crm-exit)
  ;; Misc.
  ([remap apropos] #'consult-apropos)
  :preface
  (defvar consult:find-program (cl-find-if #'executable-find (list "fdfind"
                                                                   "fd")))
  :init
  (with-eval-after-load 'project
    (autoload 'project-root "project")
    (autoload 'project-current "project"))

  ;; (advice-add #'register-preview :override #'consult-register-window)
  ;; (advice-add #'multi-occur :override #'consult-multi-occur)

  (general-setq prefix-help-command #'embark-prefix-help-command
                register-preview-delay 0
                register-preview-function #'consult-register-format
                consult-preview-key '(:debounce 0.2 any)
                consult-narrow-key "<"
                consult-line-numbers-widen t
                consult-async-min-input 2
                consult-async-refresh-delay 0.15
                consult-async-input-throttle 0.2
                consult-async-input-debounce 0.1
                consult-find-args (concat
                                   (format "%s -i -H -E .git --regex %s"
                                           consult:find-program
                                           (if windows-nt-p "--path-separator=/" ""))))
  :config
  (defadvice! consult:recent-file-fix (&rest _args)
    "`consult-recent-file' needs to have `recentf-mode' on to work correctly. A
Also need to have consult loaded as well, as it’s deferred by use-package."
    :before #'consult-recent-file
    (require 'consult)
    (recentf-mode +1))

  ;; Sources
  ;;

  (with-eval-after-load 'org
    (defvar consult-org-source
      `(:name     "Org"
        :narrow   ?o
        :hidden   t
        :category buffer
        :state    ,#'consult--buffer-state
        :items    ,(lambda () (mapcar #'buffer-name (org-buffer-list)))))
    (add-to-list 'consult-buffer-sources 'consult-org-source 'append))

  ;; Customizations
  ;;

  (consult-customize
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-recent-file consult--source-project-recent-file
   consult--source-bookmark
   :preview-key (list "C-SPC" "M-."))

  (consult-customize
   consult-buffer consult-buffer-other-window consult-buffer-other-frame
   consult--source-buffer consult--source-project-buffer consult--source-hidden-buffer
   :preview-key (list :debounce 0.25 "<up>" "<down>" "C-p" "C-n"
                      :debounce 0.3 'any))

  (consult-customize
   consult-theme
   :preview-key (list "C-SPC" "M-."
                      :debounce 1 'any)))

;;;;;; Keybindings
;;;;;;

;;;;;;; Files
;;;;;;;

(emacs:leader-def
  "f f" #'find-file
  "f r" #'consult-recent-file
  "f c" #'vertico:find-file-in-emacsd
  "f o" #'vertico:find-file-in-org-dir
  "f F" #'vertico:find-file-in-finance
  "f p" #'vertico:find-file-in-project-root)

;;;;;;; Buffers
;;;;;;;

(emacs:leader-def
  "b b" #'switch-to-buffer
  "b w" #'switch-to-buffer-other-window
  "b f" #'switch-to-buffer-other-frame
  "b i" #'ibuffer)

;;;;;;; Goto
;;;;;;;

(emacs:leader-def
  "g m" #'consult-mark
  "g k" #'consult-global-mark
  "g o" #'consult-outline
  "g i" #'consult-imenu
  "g I" #'consult-imenu-multi
  "g e" #'consult-error
  "g E" #'consult-compile-error)

;;;;;;; Searching
;;;;;;;

(emacs:leader-def
  "s l" #'consult-line
  "s s" #'vertico:search-symbol-at-point
  "s k" #'consult-keep-lines
  "s u" #'consult-focus-lines
  "s m" #'consult-multi-occur
  "s g" #'consult-ripgrep
  "s f" #'consult-find)

;;;;;; Switch Directories in the Minibuffer
;;;;;;

(use-package consult-dir
  :after (consult)
  :bind (([remap list-directory] . consult-dir)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

;;;;; Marginalia
;;;;;

(use-package marginalia
  :bind (:map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode)
  :config
  (advice-add #'marginalia--project-root :override
    (lambda () (project-root (project-current))))
  (pushnew! marginalia-command-categories
            '(flycheck-error-list-set-filter . builtin)))

;;;;; Embark
;;;;;

;;; TODO define some Embark maps here
(use-package embark
  :defines vertico-map embark-quit-after-action
  :bind (("C-;" . embark-act)
         ("C-." . embark-dwim)
         ([remap describe-bindings] . embark-bindings)
         :map minibuffer-local-map
         ("C-;" . embark-act)
         ("M-e" . embark-export)
         ("M-c" . embark-collect-snapshot)
         :map vertico-map
         ("C-;" . embark-act)
         ("M-e" . embark-export)
         ("M-c" . embark-collect-snapshot))
  :init
  (defun embark:resize-collect-window ()
    (when (memq embark-collect--kind '(:live :completions))
      (fit-window-to-buffer (get-buffer-window)
                            (floor (frame-height) 2) 1)))
  :config
  (setq embark-indicator '(embark-highlight-indicator
                           embark-isearch-highlight-indicator
                           embark-verbose-indicator)
        embark-verbose-indicator-display-action
        '(display-buffer-at-bottom (window-height . fit-window-to-buffer)))
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))

  (add-hook 'embark-post-action-hook #'embark-collect--update-linked)
  (add-hook 'embark-collect-post-revert-hook #'embark:resize-collect-window)

  (define-key embark-file-map "g" #'vertico:embark-magit-staus)
  (define-key embark-file-map "S" #'sudo-find-file))

(use-package embark-consult
  :after (embark consult))

;;;; Abbrev

(use-package abbrev
  :ensure nil
  :bind ("<C-tab>" . expand-abbrev))

;;;; Dabbrev
;;;;

(use-package dabbrev
  :ensure nil
  :bind (("C-M-/" . dabbrev-completion)
         ;;("C-M-/" . dabbrev-expand)
         )
  :preface
  (defun dabbrev:friend-buffer-p (other-buffer)
    (< (buffer-size other-buffer) (* 1 1024 1024)))
  :custom
  (dabbrev-ignored-buffer-regexps
   '("\\` "
     "\\.\\(?:pdf\\|jpe?g\\|png\\)\\'"
     "\\(?:\\(?:[EG]?\\|GR\\)TAGS\\|e?tags\\|GPATH\\)\\(<[0-9]+>\\)?"))
  (dabbrev-upcase-means-case-search t)
  (dabbrev-friend-buffer-function #'dabbrev:friend-buffer-p)
  :config
  (add-to-list 'dabbrev-ignored-buffer-modes 'pdf-view-mode)
  (add-to-list 'dabbrev-ignored-buffer-modes 'doc-view-mode)
  (add-to-list 'dabbrev-ignored-buffer-modes 'tags-table-mode))

;;;; Hippie Expand
;;;;
;;;; TODO need to adjust this

(use-package hippie-exp
  :ensure nil
  :bind ("M-/" . hippie-expand)
  :custom
  (hippie-expand-try-functions-list
   '(try-expand-all-abbrevs
     try-expand-dabbrev
     try-expand-dabbrev-all-buffers
     try-expand-line
     try-complete-file-name-partially
     try-complete-file-name
     try-expand-list)))

;;;; Corfu
;;;;

(use-package corfu
  :ensure (:wait t)
  :hook (elpaca-after-init . global-corfu-mode)
  :general (:keymaps 'corfu-map
            "TAB"       #'corfu-next
            "<tab>"     #'corfu-next
            [tab]       #'corfu-next
            "C-n"       #'corfu-next
            "S-TAB"     #'corfu-previous
            "<S-tab>"   #'corfu-previous
            "<backtab>" #'corfu-previous
            [backtab]   #'corfu-previous
            "C-p"       #'corfu-previous
            ;; "RET"       #'ignore
            ;; "<return>"  #'ignore
            ;; [return]    #'ignore
            ;; "SPC"       #'corfu-insert-separator
            ;; "<space>"   #'corfu-insert-separator
            ;; [space]     #'corfu-insert-separator
            "M-d"       #'corfu-show-documentation
            "M-l"       #'corfu-show-location)
  :preface
  (defun corfu:in-minibuffer ()
    "Allow for Corfu in the minibuffer."
    (unless (or (bound-and-true-p mct--active)
                (bound-and-true-p vertico--input))
      (setq-local corfu-auto nil)
      (corfu-mode +1)))

  (defun corfu:send-shell (&rest _)
    "Send completion candidates when inside comint/shell."
    (cond
     ((and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
      (eshell-send-input))
     ((and (derived-mode-p 'comint-mode)  (fboundp 'comint-send-input))
      (comint-send-input))))

  (defun corfu:insert-shell-filter (&optional _)
    "Insert completion candidate and send when inside comint/eshell."
    (when (derived-mode-p 'eshell-mode 'comint-mode)
      (lambda ()
        (interactive)
        (corfu-insert)
        (corfu:send-shell))))

  (defun corfu:alt-sort-function (candidates)
    "Sort CANDIDATES using both ‘display-sort-function’ and ‘corfu-sort-function’."
    (let ((candidates
           (let ((display-sort-func (corfu--metadata-get 'display-sort-function)))
             (if display-sort-func
                 (funcall display-sort-func candidates)
               candidates))))
      (if corfu-sort-function
          (funcall corfu-sort-function candidates)
        candidates)))
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-auto-delay 0.33)
  (corfu-auto-prefix 2)
  (corfu-preselect 'prompt)
  (corfu-quit-no-match t)
  (corfu-scroll-margin 6)
  (corfu-echo-documentation nil)
  (corfu-popupinfo-delay (cons 0.5 0.5))
  (corfu-sort-override-function #'corfu:alt-sort-function)
  :config
  (add-hook 'minibuffer-setup-hook #'corfu:in-minibuffer 1)

  ;; ;; SPC as separator
  ;; (setq corfu-separator 32)

  ;; ;; highly recommended to use corfu-separator with "32" (space)
  ;; (define-key corfu-map (kbd "SPC")
  ;;   (lambda ()
  ;;     (interactive)
  ;;     (if current-prefix-arg
  ;;         ;;we suppose that we want leave the word like that, so do a space
  ;;         (progn
  ;;           (corfu-quit)
  ;;           (insert " "))
  ;;       (if (and (= (char-before) corfu-separator)
  ;;                (or
  ;;                 ;; check if space, return or nothing after
  ;;                 (not (char-after))
  ;;                 (= (char-after) ?\s)
  ;;                 (= (char-after) ?\n)))
  ;;           (progn
  ;;             (corfu-insert)
  ;;             (insert " "))
  ;;         (corfu-insert-separator)))))

  (dolist (c (list (cons "SPC" " ")
                   (cons "." ".")
                   (cons "," ",")
                   (cons ")" ")")
                   (cons "}" "}")
                   (cons "]" "]")))
    (define-key corfu-map (kbd (car c)) `(lambda ()
                                           (interactive)
                                           (corfu-insert)
                                           (insert ,(cdr c)))))
  (corfu-echo-mode +1)
  (corfu-history-mode +1)
  (corfu-popupinfo-mode +1)

  (with-eval-after-load 'savehist
    (add-to-list 'savehist-additional-variables 'corfu-history)))

(use-package corfu-terminal
  :after corfu
  :config
  (unless (display-graphic-p)
    (corfu-terminal-mode +1)))

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-use-icons t)
  (kind-icon-default-face 'corfu-default)
  (kind-icon-blend-background nil)
  (kind-icon-blend-frac 0.1)
  (svg-lib-icons-dir (concat my-cache-dir "svg-lib/"))
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

;;;; Cape
;;;;
;;;; As a note, you cannot abuse ‘cape-capf-buster’ too much, otherwise
;;;; you will not get any completion candidates as you type with Corfu.
;;;; If you see the following "super capfs" we don’t bust capfs that cache
;;;; a lot like ‘cape-dabbrev’ and ‘cape-dict’.
;;;; If we do this *inside* of a super capf, we won’t get any results.
;;;; I’m not entirely sure why that is, but I imagine it has something to do with
;;;; how ‘cape-capf-super’ transforms a series of capfs.

(use-package cape
  :ensure (:wait t)
  :after corfu
  :custom
  (cape-dict-file (expand-file-name "~/.local/share/dict/words"))
  (cape-dabbrev-min-length 2)
  (cape-dabbrev-check-other-buffers t)
  :config
  (defalias 'cape:elisp-cap
    (cape-capf-buster #'elisp-completion-at-point)
    "Bust the ‘elisp-completion-at-point’.")

  (defalias 'cape:elisp-capf
    (cape-capf-super
     #'cape:elisp-cap
     #'cape-abbrev
     #'cape-keyword
     #'cape-elisp-symbol
     #'cape-dabbrev)
    "A ‘cape-capf-super’ that’s similar to most simpler Company configurations.
The defacto cape to use for Emacs Lisp.")

  (defalias 'cape:prog-capf
    (cape-capf-super
     #'cape-abbrev
     #'cape-keyword
     #'cape-dabbrev)
    "A ‘cape-capf-super’ for when a ‘prog-mode’-esque mode doesn’t utilitize ‘lsp-mode’.")

  (defalias 'cape:mega-writing-capf
    (cape-capf-super
     #'cape-dict
     #'cape-abbrev
     #'cape-dabbrev
     #'cape-elisp-block)
    "A ‘cape-capf-super’ for modes for writing.
The defacto cape to use for markup languages for writing.")

  (advice-add #'comint-completion-at-point :around #'cape-wrap-nonexclusive)

  (setq-mode-local conf-mode
                   completion-at-point-functions (list (cape-capf-buster #'cape:prog-capf)
                                                       #'cape-file)))

(provide 'completion)
;;; completion.el ends here
