;;; abbrev-x.el --- Extensions for abbrevs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs abbrev

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'cl-macs)
(require 'abbrev)

;;;###autoload
(defun abbrev-x::inside-comment-or-string-p ()
  "Check if the point in inside a comment or string"
  (let* ((pos (point))
         (faces (get-text-property pos 'face)))
    (when (not (listp faces))
      (setf faces (list faces)))
    (save-excursion
      (if (derived-mode-p 'org-mode)
          (save-match-data
            (beginning-of-line)
            (looking-at "^[ \t]*#"))
        (or
         (nth 4 (syntax-ppss))
         (nth 8 (syntax-ppss))
         (numberp (cl-position 'font-lock-comment-face faces))
         (numberp (cl-position 'font-lock-comment-delimiter-face faces))
         (numberp (cl-position 'font-lock-doc-face faces))
         (numberp (cl-position 'font-lock-string-face faces)))))))

;;;###autoload
(defun abbrev-x::table-p (symbol)
  "Check if a SYMBOL is bound to an abbrev table.

The ‘abbrev-table-p’ function has the misfortune of throwing
and error if a symbol is not bound to anything, which, while accurate,
isn’t helpful in the case if you’re trying to check if an abbrev table even
exists."
  (if (boundp symbol)
      (ignore-errors
        (abbrev-table-p (unquote symbol)))
    nil))

(defun abbrev-x:abbrev-add-props (abbrev props)
  "Add one or more PROPS to an existing ABBREV.
ABBREV has to be the string representation of the abbrev.
Eample: \"if\" \"cif\" etc.

PROPS should be a plist of (PROP VALUE).
Example:

’(:enable-function (lambda (&rest _) (do-something)))

NOTE: This effects a _singular abbrev_ and not an abbrev table."
  (unless (null props)
    (let ((sym (abbrev-symbol abbrev)))
      (if (cddr props)
          (cl-loop for (k v) on props by #'cddr
                   unless (and (not sym) (abbrev-get sym k))
                   do (abbrev-put sym k v))
        (unless (and (not sym)
                     (abbrev-get sym (car props)))
          (abbrev-put sym (car props) (cadr props)))))))

(defun abbrev-x:abbrev-table-add-props (abbrev-table props)
  "Add one or more PROPS to an existing ABBREV-TABLE.
PROPS should be a plist of (PROP VALUE).
Example:

’(:enable-function (lambda (&rest _) (do-something)))

NOTE: This effects the _abbrev table_ and not a singular abbrev."
  (unless (null props)
    (if (cddr props)
        (cl-loop for (k v) on props by #'cddr
                 unless (not (boundp abbrev-table))
                   unless (abbrev-table-get (unquote abbrev-table) k)
                     do (abbrev-table-put (unquote abbrev-table) k v))
      (unless (and (not (boundp abbrev-table))
                   (abbrev-table-get (unquote abbrev-table) (car props)))
        (abbrev-table-put (unquote abbrev-table) (car props) (cadr props))))))

;;;###autoload
(defun abbrev-x:expand-abbrev-maybe ()
  "Function to expand an abbrev if:
1. The \"word\" emacs recognizes if it’s not in a comment, comment block, string or docstring.
2. If it matches the regex for a \"word\" or a \"word\" that ends with an asterisk
(for ‘let*’ ‘if-let*’, etc).

This function simply for returning a truthy or falsey value."
  (and (not (abbrev-x::inside-comment-or-string-p))
       (looking-back (rx (| (group word "*") word)) (line-beginning-position) t)))

(defmacro define-abbrev-x (table abbrev expansion &optional hook &rest props)
  "Define new ABBREV in TABLE with EXPANSION, optionally with HOOK and PROPS."
  (let* ((tbl-str      (symbol-name `,table))
         (split        (string-split tbl-str "-"))
         (mode-pos     (cl-position-if (lambda (str) (string= str "mode")) split))
         (tbl-name-lst (cl-subseq split 0 (1+ mode-pos)))
         (mode         (string-join tbl-name-lst "-"))
         (tbl-name     (format "%s-abbrev-table" mode)))
    `,(if (boundp `,table)
          `(define-abbrev ,table ,abbrev ,expansion ,(or hook t) ,@props)
        `(define-abbrev-table ',(intern tbl-name)
           ,`'((,abbrev ,expansion ,(or hook t) ,@props))
           ,(format "An abbrev table for %s" mode)
           :system t))))

(defun abbrev-x::compose-table-abbrevs (abbrevs expansion hook &rest props)
  "Create a list of ABBREVS that can be passed to ‘define-abbrev-table’.
This is for the purposes of correcting common spelling errors with abbrevs.

The resulting form will be something like

    ((\"acknowlege\" \"acknowledge\")
     (\"aknowledge\" \"acknowledge\"))

Which we can use like so with ‘define-abbrev-table’

    (let ((abbrev-list (abbrev-x::compose-table-abbrevs ’(\"acknowlege\" \"aknowledge\") \"acknowledge\")))
      (define-abbrev-table org-mode-abbrev-table
        abbrev-list
        \"Some description here\"
        :system t))"
  (cl-loop for abbrev in abbrevs
           collect (list abbrev expansion hook props)))

;; ;;;###autoload
;; (defmacro abbrev-x::add-multiple-abbrevs (table abbrevs expansion &optional hook &rest props)
;;   "Add mulitple ABBREVS to TABLE for the same EXPANSION.
;; NOTE: HOOK and PROPS will be the same for all of them."
;;   `,(if (abbrev-x::table-p ,table)
;;         (cl-loop for abbrev in abbrevs
;;                  do `(define-abbrev-x ,table ,abbrev ,expansion ,hook ,@props))
;;       (let (())
;;       ))

;; ;;;###autoload
;; (defmacro abbrev-x::add-abbrev-multiple-tables (tables abbrev expansion &optional hook &rest props)
;;   "Add a common ABBREV to multiple TABLES for the same EXPANSION.
;; NOTE: HOOK and PROPS will be the same of all of them."
;;   `,(cl-loop for table in tables
;;              do `(define-abbrev-x ,table ,abbrev ,expansion ,hook ,@props)))

;; ;;;###autoload
;; (defmacro abbrev-x::multiple-abbrevs-in-tables (tables abbrevs expansion &optional hook &rest props)
;;   "Add multiple ABBREVS to multiple TABLES for the same EXPANSION.
;; NOTE: HOOK and PROPS will be the same of all of them."
;;   `,(cl-loop for table in tables
;;              for abbrev in abbrevs
;;              do `(define-abbrev-x ,table ,abbrev ,expansion ,hook ,props)))

;; (defmacro define-multi-abbrev (tables abbrevs expansion &optional hook &rest props)
;;   "Define multiple ABBREVS in a table, define an abbrev in multiple TABLES, or both for the same EXPANSION.
;; NOTE: HOOK and PROPS will be the same for all of the abbrevs."
;;   `,(cond ((and
;;             (listp ,tables)
;;             (not (listp ,abbrevs)))
;;            ,(abbrev-x::add-abbrev-multiple-tables tables abbrevs expansion hook props))
;;           ((and
;;             (listp ,abbrevs)
;;             (not (listp ,tables)))
;;            ,(abbrev-x::add-multiple-abbrevs tables abbrevs expansion hook props))
;;           (t
;;            ,(abbrev-x::multiple-abbrevs-in-tables tables abbrevs expansion hook props))))

(provide 'abbrev-x)
;;; abbrev-x.el ends here
