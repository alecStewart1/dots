;;; blorg.el --- Blorg: Blogging with Org -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;  My configuration for blogging with ‘ox-publish’.
;;
;;  The idea is to limit 3rd-party packaging, and use
;;  existing functionality in Emacs and Org-Mode.
;;  Except ‘jack’ because I didn’t feel like writing
;;  out HTML in a string for ‘:html-preamble’ and
;;  ‘:html-postamble’.
;;
;;; Code:

(require 'lib)
(require 'ox)
(require 'ox-publish)
(require 'jack)

;;;###autoload
(defconst blorg:nav-links
  '(home    ("Home" "/")
    posts   ("Posts" "/posts")
    about   ("About" "/about")
    contact ("Contact" "/contact"))
  "A plist of items to be in the navbar.")

;;;###autoload
(defconst blorg:html-preamble ;; TODO
  (jack-html
   `(:nav.navbar
     (:ul.navlist
      ,(cl-loop for (_prop val) on blorg:nav-links by #'cddr
                do `(:li.nav-item
                     (:a.nav-link
                      (@ :href (cdr val))
                      (car val)))))))
  "‘:html-premable’ for blog site using ‘jack’.")

;;;###autoload
(defconst blog:html-postamble ;; TODO
  (jack-html
   `(:footer.footer
     ))
  "‘:html-postamble’ for blog site using ‘jack’.")

(setq org-publish-project-alist
      `(("alec-stewart-pages"
         :base-directory ,(concat my-personal-blog-dir "pages/")
         :base-extension "org"
         :publishing-directory "public/"
         :recursive t
         :with-title t
         :with-author nil
         :with-creator nil
         :with-date nil
         :with-statistics-cookies nil
         :with-todo-keywords nil
         :html-link-home "/"
         :html-link-up "/"
         :html-head-include-scripts nil
         :html-head-include-default-style nil
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-title "Pages Sitemap"
         :sitemap-filename "sitemap.org"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically)
        ("alec-stewart-posts"
         :base-directory ,(concat my-personal-blog-dir "posts/")
         :base-extension "org"
         :publishing-directory "public/posts"
         :recursive t
         :with-title t
         :with-author t
         :with-creator nil
         :with-date t
         :with-statistics-cookies nil
         :with-todo-keywords nil
         :html-link-home "/"
         :html-link-up "/posts"
         :html-head-include-scripts nil
         :html-head-include-default-style nil
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-title "Post Sitemap"
         :sitemap-filename "sitemap.org"
         :sitemap-style list
         :sitemap-sort-files anti-chronologically)
        ("alec-stewart" :components ("alec-stewart-pages" "alec-stewart-posts"))))
