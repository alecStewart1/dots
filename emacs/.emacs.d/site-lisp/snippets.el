;;; snippets.el --- My snippet system -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Alec Stewart

;; Author: Alec Stewart <alec-stewart@protonmail.com>
;; URL: https://codeberg.org/alecStewart1/dots/emacs/.emacs.d
;; Keywords: emacs .emacs.d dotemacs snippets skeleton abbrev yasnippet

;; This file is not part of GNU Emacs.

;; This is free and unencumbered software released into the public domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a compiled
;; binary, for any purpose, commercial or non-commercial, and by any
;; means.

;; In jurisdictions that recognize copyright laws, the author or authors
;; of this software dedicate any and all copyright interest in the
;; software to the public domain. We make this dedication for the benefit
;; of the public at large and to the detriment of our heirs and
;; successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to this
;; software under copyright law.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;; IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;; For more information, please refer to <http://unlicense.org/>

;;; Commentary:
;;
;;
;;
;;; Code:

(require 'skeleton)
(require 'abbrev)
(require 'abbrev-x)

(defvar snippet::marks nil
  "A list of markers that represent skeleton positions.")

(add-hook 'skeleton-end-hook #'snippet::make-markers)

(defmacro def-global-snippet (name
                              &rest prompt-and-or-skeleton)
  "Create a global \"snippet\" with NAME and PROMPT-AND-OR-SKELETON
NAME must be valid in the Emacs Lisp naming convention.

DOCSTRING is used in the abbrev rather than the skeleton.

PROMPT-AND-OR-SKELETON can be any of the following:
1. A valid Skeleton that uses the internal `Skeleton' langauge
2. A key/value \"pair\" that’s :prompt STRING, followed by
   a valid Skeleton that uses the internal `Skeleton' language.
The prompt given is used by the Skeleton to prompt the user for an input.

This macro makes use of `define-skeleton' and `define-abbrev' in order to
create something similar to a code/writing snippet system, like that of
`YASnippet'.

Keep in mind that all abbreviations created are put in the
`global-abbrev-table' under the named passed to this macro.

That may or may not be something you want, depending on your uses.
If you're looking to only define an abbrev for a specific mode, see
`def-mode-snippet’."
  (declare (debug t)
           (indent defun))
  (let* ((snip-name (symbol-name `,name))
         (func-name (intern (concat "global-" snip-name "-skel")))
         (has-prompt (keywordp (car prompt-and-or-skeleton)))
         (prompt (if has-prompt (cadr prompt-and-or-skeleton) ""))
         (skeleton (if (not has-prompt) prompt-and-or-skeleton (cddr prompt-and-or-skeleton))))
    (macroexp-progn
     `((define-skeleton ,func-name
         ,(concat snip-name " skeleton")
         ,prompt
         ,@skeleton)
       (define-abbrev global-abbrev-table ,snip-name
         "" ,func-name
         :system t
         :case-fixed t
         :enable-function #'abbrev-x:expand-abbrev-maybe)))))

(defmacro def-mode-snippet (name
                            mode
                            &rest prompt-and-or-skeleton)
  "Create a MODE specific \"snippet\" with PROMPT-AND-OR-SKELETON.
NAME must be valid in the Emacs Lisp naming convention.

MODE must be a valid major or minor mode that is known to Emacs.

Example: ‘org-mode’, ‘emacs-lisp-mode’, etc.

PROMPT-AND-OR-SKELETON can be any of the following:
1. A valid Skeleton that uses the internal `Skeleton' langauge
2. A key/value \"pair\" that’s :prompt STRING, followed by
   a valid Skeleton that uses the internal `Skeleton' language.
The prompt given is used by the Skeleton to prompt the user for an input.

This macro makes use of `define-skeleton' and `define-abbrev' in order to
create something similar to a code/writing snippet system, like that of
`YASnippet'. Keep in mind that all abbreviations created are put in the abbrev
table of MODE you passed to this macro.

Example: passing ‘org-mode’ will add the abbrev to the ‘org-mode-abbrev-table’.

That may or may not be something you want depending on your uses.
If you're looking to only define an abbrev globally, see `def-global-snippet'."
  (declare (debug t)
           (indent defun))
  (let* ((snip-name (symbol-name `,name))
         (func-name (intern (concat (symbol-name mode) "-" snip-name "-skel")))
         (var-str (concat (symbol-name mode) "-abbrev-table"))
         (abbrev-table (intern var-str))
         (has-prompt (keywordp (car prompt-and-or-skeleton)))
         (prompt (if has-prompt (cadr prompt-and-or-skeleton) ""))
         (skeleton (if (not has-prompt) prompt-and-or-skeleton (cddr prompt-and-or-skeleton))))
    (macroexp-progn
     `((define-skeleton ,func-name
         ,(format "%s %s %s %s." snip-name "skeleton. Defined in" var-str "abbreviaton table")
         ,prompt
         ,@skeleton)
       (define-abbrev-x ,abbrev-table ,snip-name
         "" #',func-name
         :system t
         :case-fixed t
         :enable-function #'abbrev-x:expand-abbrev-maybe)))))

(defun snippet::make-markers ()
  "Create markers from points inside of ‘skeleton-positions’."
  (setf snippet::marks (mapcar #'copy-marker (reverse skeleton-positions))))

;;;###autoload
(defun snippet:next-position ()
  "Goto the next skeleton position in ‘snippet:marks’.
These positions are denoted with ’@ _’ in Skeleton’s template language."
  (interactive)
  (let* ((positions (mapcar #'marker-position snippet::marks))
         (pos (pop positions)))
    (if positions
        (if (catch 'break
              (while (setf pos (pop positions))
                (when (< (point) pos)
                  (throw 'break t))))
            (goto-char pos))
      (goto-char (marker-position (car snippet::marks))))))

;;;###autoload
(defun snippet:prev-position ()
  "Goto the previous skeleton position in ‘snippet:marks’.
These positions are denoted with ’@ _’ in Skeleton’s template language."
  (interactive)
  (let* ((positions (mapcar #'marker-position snippet::marks))
         (pos (pop positions)))
    (if positions
        (if (catch 'break
              (while (setf pos (pop positions))
                (when (> (point) pos)
                  (throw 'break t))))
            (goto-char pos))
      (goto-char (marker-position (car snippet::marks))))))

(define-key prog-mode-map (kbd "C-{") #'snippet:prev-position)
(define-key prog-mode-map (kbd "C-}") #'snippet:next-position)

(provide 'snippets)
;;; snippets.el ends here
